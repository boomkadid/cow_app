package com.kasidid.mrbboomm.cowlog;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 1/4/2017.
 */

public class Sync_DB extends AsyncTask<String, Void, String> {
    private Cow cow;

    public Sync_DB(Cow c) {
        this.cow = c;
    }

    @Override
    protected String doInBackground(String... strings) {
        String link;
        String data;
        try {
            data = "?id=" + URLEncoder.encode(String.valueOf(cow.getId()), "UTF-8");
            data += "&username=" + URLEncoder.encode(cow.getUsername(), "UTF-8");
            data += "&cowname=" + URLEncoder.encode(cow.getCowname(), "UTF-8");
            data += "&nickname=" + URLEncoder.encode(cow.getNickname(), "UTF-8");
            data += "&fraction=" + URLEncoder.encode(cow.getFraction(), "UTF-8");
            data += "&birth=" + URLEncoder.encode(cow.getBirth(), "UTF-8");
            data += "&daystart=" + URLEncoder.encode(cow.getDaystart(), "UTF-8");
            data += "&status=" + URLEncoder.encode(cow.getStatus(), "UTF-8");
            data += "&dayend=" + URLEncoder.encode(cow.getEndDate(), "UTF-8");
            data += "&daymilk=" + URLEncoder.encode(cow.getCalvdate(), "UTF-8");
            data += "&state=" + URLEncoder.encode(cow.getState(), "UTF-8");
            data += "&papa=" + URLEncoder.encode(cow.getPapa(), "UTF-8");
            data += "&papaf=" + URLEncoder.encode(cow.getPapaf(), "UTF-8");
            data += "&mama=" + URLEncoder.encode(cow.getMama(), "UTF-8");
            data += "&mamaf=" + URLEncoder.encode(cow.getMamaf(), "UTF-8");
            data += "&type=" + URLEncoder.encode(cow.getType(), "UTF-8");
            data += "&parity=" + URLEncoder.encode(cow.getParity(), "UTF-8");
            link = "https://info.rdi.ku.ac.th/cowapp/manager/update.php" + data;
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(link).build();
            Response response = client.newCall(request).execute();
            Log.d("link", link);
            return response.body().string();


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("link", "fail");
            return new String("Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        String jsonStr = result;
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String query_result = jsonObj.getString("query_result");
                if (query_result.equals("SUCCESS")) {
                    Log.d("result_sync", "SUCESS");
                } else if (query_result.equals("FAILURE")) {
                    Log.d("result_sync", "FAILURE");
                } else {

                    Log.d("result_sync", "Couldn't connect");
                }
            } catch (JSONException e) {
                Log.d("result_sync", result);
            }
        } else {

            Log.d("result_sync", "couldn't get JSON data");
        }
    }

}