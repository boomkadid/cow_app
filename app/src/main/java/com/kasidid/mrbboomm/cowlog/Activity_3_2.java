package com.kasidid.mrbboomm.cowlog;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by mrbboomm on 8/23/2016.
 */
public class Activity_3_2 extends Fragment {
    TextView remain,Header;
    Button mConfirm;
    Calendar mCalendar;
    COWHelper mHelper;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_3_2, container, false);
        Bundle bundle = this.getArguments();
        final String  id = bundle.getString("ID");
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String username =  prefs.getString("username","not found");
        mHelper = new COWHelper(getActivity());
        final Cow c = mHelper.getCowByID(id);
        mCalendar = Calendar.getInstance();
        Header = (TextView)view.findViewById(R.id.textheader3_2);
        remain = (TextView)view.findViewById(R.id.text3_2);
        Header.setText("กำลังตั้งท้อง");
        remain.setText("กำหนดคลอดประมาณวันที่ "+c.getDayTH(c.getDayend()));
        Button deliver = (Button)view.findViewById(R.id.deliver);
        Button abort = (Button)view.findViewById(R.id.abort);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun.ttf");
        Header.setTypeface(tf);
        remain.setTypeface(tf);
        deliver.setTypeface(tf);
        abort.setTypeface(tf);
        deliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage(c.getNickname()+" พร้อมคลอด?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {
                        Activity_3_3 fragment = new Activity_3_3();
                        Bundle args = new Bundle();
                        args.putString("ID",id);
                        fragment.setArguments(args);
                        android.support.v4.app.FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.replace(R.id.root_frame,fragment);
                        trans.commit();

                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

            }
        });
        mCalendar = Calendar.getInstance();
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage("แท้ง?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {
                        Cow cow = mHelper.getCowByID(id);
                        cow.setDaystart(mCalendar.get(Calendar.YEAR)+"-"+(mCalendar.get(Calendar.MONTH)+1)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH));
                        cow.setStatus("0");
                        mHelper.updateCow(cow);
                        Activity_0_1 fragment = new Activity_0_1();
                        Bundle args = new Bundle();
                        args.putString("ID",id);
                        fragment.setArguments(args);
                        android.support.v4.app.FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.replace(R.id.root_frame,fragment);
                        trans.commit();
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

            }
        });
        return view;
    }


}

