package com.kasidid.mrbboomm.cowlog;

import android.provider.BaseColumns;

/**
 * Created by Kasidid on 3/6/2017.
 */

public class Calving {

    private int id;
    private String username;
    private String cowname;
    private String parity;
    private String calvingdate;
    private String restday;
    private String kid;
    public static final String TABLE  = "calving";
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String USERNAME = "username";
        public static final String COWNAME = "number";
        public static final String PARITY = "parity";
        public static final String CALVINGDATE = "calvingdate";
        public static final String RESTDAY = "restday";
        public static final String KID = "kid";

    }

    public String getKid() {
        return kid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setKid(String kid) {
        this.kid = kid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCowname() {
        return cowname;
    }

    public void setCowname(String cowname) {
        this.cowname = cowname;
    }

    public String getParity() {
        return parity;
    }

    public void setParity(String parity) {
        this.parity = parity;
    }

    public String getCalvingdate() {
        return calvingdate;
    }

    public void setCalvingdate(String calvingdate) {
        this.calvingdate = calvingdate;
    }

    public String getRestday() {
        return restday;
    }

    public void setRestday(String restday) {
        this.restday = restday;
    }
}
