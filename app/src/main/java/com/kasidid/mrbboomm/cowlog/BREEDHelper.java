package com.kasidid.mrbboomm.cowlog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kasidid on 2/27/2017.
 */

public class BREEDHelper extends SQLiteOpenHelper {
    public BREEDHelper(Context context) {
        super(context, "breed.db", null, 5);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BREED_TABLE = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT)",
                Breed.TABLE,
                Breed.Column.ID,
                Breed.Column.USERNAME,
                Breed.Column.COWNAME,
                Breed.Column.PARITY,
                Breed.Column.PAPA,
                Breed.Column.BREEDDATE);

        db.execSQL(CREATE_BREED_TABLE);
    }
    private ContentValues getValues(Breed breed){
        ContentValues values = new ContentValues();
        values.put(Breed.Column.USERNAME, breed.getUsername());
        values.put(Breed.Column.COWNAME, breed.getCowname());
        values.put(Breed.Column.PARITY, breed.getParity());
        values.put(Breed.Column.PAPA, breed.getPapa());
        values.put(Breed.Column.BREEDDATE,breed.getBreeddate());
        return values;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_BREED_TABLE = "DROP TABLE IF EXISTS breed";
        db.execSQL(DROP_BREED_TABLE);
        onCreate(db);
    }
    public void addBreed(Breed breed){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getValues(breed);
        db.insert(Breed.TABLE, null, values);
        db.close();
    }
    public void deleteBreed(String cowname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Breed.TABLE, Breed.Column.COWNAME + "='" + cowname+"'", null);
        db.close();
    }
    public void clearBreed(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Breed.TABLE, null, null);
        db.close();
    }
    public List<String> getLastBreed(String cowname,String parity){
        List<String> breeds = new ArrayList<String>();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String papa="-",breeddate="-";
        Cursor cursor = sqLiteDatabase.query
                (Breed.TABLE, null, Breed.Column.COWNAME+"=?",
                        new String[]{cowname}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        int count = 0;
        while(!cursor.isAfterLast()) {
            if (cursor.getString(3).equals(parity)){
                count += 1;
                papa = cursor.getString(4);
                breeddate = cursor.getString(5);
            }

            cursor.moveToNext();
        }
        cursor.close();
        breeds.add(String.valueOf(count));
        breeds.add(papa);
        breeds.add(breeddate);
        return breeds;
    }
    public List<String> getIntervalBreed(String cowname,String parity,Calendar st, Calendar ed){
        List<String> breeds = new ArrayList<String>();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String papa="-",breeddate="-";
        Cursor cursor = sqLiteDatabase.query
                (Breed.TABLE, null, Breed.Column.COWNAME+"=?",
                        new String[]{cowname}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        int count = 0;
        Calendar bd = Calendar.getInstance();
        while(!cursor.isAfterLast()) {
            if (cursor.getString(3).equals(parity) ){
                papa = cursor.getString(4);
                breeddate = cursor.getString(5);
                try {
                    bd.setTime(df.parse(breeddate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (bd.after(st) && bd.before(ed)) count += 1;
            }

            cursor.moveToNext();
        }
        cursor.close();
        breeds.add(String.valueOf(count));
        breeds.add(papa);
        breeds.add(breeddate);
        return breeds;
    }
    public List<String> getBreedTimes(String cowname,String parity) {
        List<String> breeds = new ArrayList<String>();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        Cursor cursor = sqLiteDatabase.query
                (Breed.TABLE, null, Breed.Column.COWNAME+"=?",
                        new String[]{cowname}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        int count = 0;
        String papa = "";
        while(!cursor.isAfterLast()) {
            if (cursor.getString(3).equals(parity)){
                count += 1;
                papa = cursor.getString(4);
            }

            cursor.moveToNext();
        }
        breeds.add(String.valueOf(count));
        breeds.add(papa);
        sqLiteDatabase.close();
        cursor.close();
        return breeds;
    }


}
