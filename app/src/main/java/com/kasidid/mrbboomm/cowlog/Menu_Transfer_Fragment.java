package com.kasidid.mrbboomm.cowlog;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 11/17/2017.
 */

public class Menu_Transfer_Fragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_transfer_f, container, false);
        Button from_user_button = (Button)view.findViewById(R.id.from_user_button);
        from_user_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                new Sync_GetTransfer(getActivity()).execute();
            }
        });
        return view;
    }
    private ProgressDialog mProgressDialog;
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
    public class Sync_GetTransfer extends AsyncTask<String,Void,String> {
        Context mContext;
        String  data;
        ArrayList<String> usernames = new ArrayList<>();
        ArrayList<String> cowname = new ArrayList<>();
        ArrayList<String> usernames2 = new ArrayList<>();
        public Sync_GetTransfer(Context context){
            this.mContext = context;
        }
        @Override
        protected String doInBackground(String... params) {
            SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(mContext);
            data= "?username=" + prefs.getString("username","not found");

            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/transfer/getfromuser.php" + data).build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("result(fromuser)","https://info.rdi.ku.ac.th/cowapp/manager/transfer/getfromuser.php" + data);
                Log.d("result(fromuser)",result);

                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            usernames.add(x.getString("username"));
                            cowname.add(x.getString("cowname"));
                            usernames2.add(x.getString("username2"));
                            Log.d("get(fromuser)", "done");

                        } catch (Exception e) {

                            Log.d("get(fromuser)","fail");
                            continue;
                        }

                    }
                }
                else{
                    return "none";
                }
                return "success";
            }catch (Exception e){
                Log.d("get(fromuser)","fail");
                return "fail";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            hideProgressDialog();
            if (s.equals("success")){

                Intent intent = new Intent(getActivity(),Transfer_fromUser.class);
                intent.putExtra("usernames", usernames);
                intent.putExtra("cownames",  cowname);
                intent.putExtra("usernames2",  usernames2);
                startActivity(intent);

            }
            else{
                Toast.makeText(mContext, "ไม่พบรายการโค", Toast.LENGTH_LONG).show();

            }
        }
    }

}
