package com.kasidid.mrbboomm.cowlog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by mrbboomm on 10/4/2016.
 */
public class Activity_Image extends Fragment {
    String id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_image_f, container, false);
        Bundle bundle = this.getArguments();
        id = bundle.getString("ID");
        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        gridview.setAdapter(new CustomAdapter(getActivity()));
        return view;
    }
    public class CustomAdapter extends BaseAdapter {
        private Context mContext;

        public CustomAdapter(Context context) {
            mContext = context;
        }

        public int getCount() {
            return 4;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            COWHelper mHelper = new COWHelper(getActivity());
            Cow c = mHelper.getCowByID(id);
            if (convertView == null) {
                imageView = new ImageView(getActivity());
                imageView.getScaleType();
                imageView.setLayoutParams(new GridView.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, 300));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(4, 4, 4, 4);
            } else {
                imageView = (ImageView) convertView;
            }
            Picasso.with(mContext).load("https://info.rdi.ku.ac.th/cowapp/manager/pictures/"+c.getCowname()+"/"+(position+1)+".jpg").into(imageView);
            return imageView;
        }

    }
}
