package com.kasidid.mrbboomm.cowlog;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 1/4/2017.
 */

public class Sync_Breed extends AsyncTask<String, Void, String> {
    private Breed breed;

    public Sync_Breed(Breed breed) {
        this.breed = breed;
    }

    @Override
    protected String doInBackground(String... strings) {
        String link;
        String data;
        try {
            data = "?id=" + URLEncoder.encode(String.valueOf(breed.getId()), "UTF-8");
            data += "&username=" + URLEncoder.encode(breed.getUsername(), "UTF-8");
            data += "&cowname=" + URLEncoder.encode(breed.getCowname(), "UTF-8");
            data += "&parity=" + URLEncoder.encode(breed.getParity(), "UTF-8");
            data += "&papa=" + URLEncoder.encode(breed.getPapa(), "UTF-8");
            data += "&breeddate=" + URLEncoder.encode(breed.getBreeddate(), "UTF-8");
            link = "https://info.rdi.ku.ac.th/cowapp/manager/updatebreed.php" + data;

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(link).build();
            Response response = client.newCall(request).execute();
            Log.d("link_breed", link);
            return response.body().string();

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("link", "FAIL sync");
            return new String("Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        String jsonStr = result;
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String query_result = jsonObj.getString("query_result");
                if (query_result.equals("SUCCESS")) {
                    Log.d("result_sync", "SUCESS");
                } else if (query_result.equals("FAILURE")) {
                    Log.d("result_sync", "FAILURE");
                } else {

                    Log.d("result_sync", "Couldn't connect");
                }
            } catch (JSONException e) {
                Log.d("result_sync", result);
            }
        } else {
            Log.d("result_sync", "couldn't get JSON data");
        }
    }

}