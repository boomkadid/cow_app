package com.kasidid.mrbboomm.cowlog;

import android.provider.BaseColumns;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mrbboomm on 7/11/2016.
 */
public class Cow {
    private int id;
    private String cowname;
    private String nickname;
    private String fraction;
    private String username;
    private String birth;
    private String daystart;
    private String status;
    private String dayend;
    private String calvdate;
    private String state;
    private String papa;
    private String papaf;
    private String mama;
    private String mamaf;
    private String type;
    private String parity;
    public static final String TABLE  = "cow";
    public Cow(){

    }
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String COWNAME = "number";
        public static final String NICKNAME = "nickname";
        public static final String USERNAME = "username";
        public static final String FRACTION = "fraction";
        public static final String CALVDATE = "calvdate";
        public static final String BIRTH = "birth";
        public static final String DAYSTART = "dayend";
        public static final String DAYEND = "daystart";
        public static final String STATUS = "status";
        public static final String STATE = "state";
        public static final String PAPA = "papa";
        public static final String PAPAF = "papaf";
        public static final String MAMA = "mama";
        public static final String MAMAF = "mamaf";
        public static final String TYPE = "type";
        public static final String PARITY = "parity";

    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCowname() {
        return cowname;
    }

    public void setCowname(String cowname) {
        this.cowname = cowname;
    }

    public String getPapaf() {
        return papaf;
    }

    public void setPapaf(String papaf) {
        this.papaf = papaf;
    }

    public String getMamaf() {
        return mamaf;
    }

    public void setMamaf(String mamaf) {
        this.mamaf = mamaf;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFraction() {
        return fraction;
    }

    public void setFraction(String fraction) {
        this.fraction = fraction;
    }

    public String getDaystart() {
        return daystart;
    }

    public void setDaystart(String daystart) {
        this.daystart = daystart;
    }

    public String getDayend() {
        return dayend;
    }

    public void setDayend(String dayend) {
        this.dayend = dayend;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPapa() {
        return papa;
    }

    public void setPapa(String papa) {
        this.papa = papa;
    }

    public String getMama() {
        return mama;
    }

    public void setMama(String mama) {
        this.mama = mama;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCalvdate() {
        return calvdate;
    }

    public void setCalvdate(String calvdate) {
        this.calvdate = calvdate;
    }

    public String getParity() {
        return parity;
    }

    public void setParity(String parity) {
        this.parity = parity;
    }

    public String showStatus(){
        String msg;
        switch (this.status){
            case "0":
                msg = "รอวันตรวจสอบการเป็นสัด";
                break;
            case "1":
                msg = "อยู่ในช่วงเป็นสัด";
                break;
            case "2":
                msg =  "อยู่ในช่วงหลังผสมพันธุ์";
                break;
            case "3":
                msg =  "กำลังตั้งท้อง";
                break;
            case "4":
                msg =  "ช่วงหลังคลอด";
                break;
            case "5":
                msg = "รอช่วงการเป็นสัด";
                break;
            case "6":
                msg = "กำลังตั้งท้อง";
                break;
            default:
                msg = "N/A";
                break;
        }
        return msg;
    }

    public String getEndDate() {
        Calendar daystart = Calendar.getInstance();
        Calendar dayend = Calendar.getInstance();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH");
        try {
            daystart.setTime(df.parse(this.getDaystart()));
            dayend.setTime(df.parse(this.getDaystart()));
        } catch (ParseException e) {
//            e.printStackTrace();
        }
        switch (status) {
            case "0":
                dayend.add(Calendar.DATE,+365);
            case "1":
                if (this.getState().equals("0"))
                    dayend.add(Calendar.DATE, +5);
                else{
                    try {
                        dayend.setTime(df2.parse(this.getState()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case "2":
                dayend.add(Calendar.DATE, +67);
                break;
            case "3":
                dayend.add(Calendar.DATE, 279 - 67);
                break;
            case "4":
                dayend.add(Calendar.DATE, +39);
                break;
            case "5":
                dayend.add(Calendar.DATE, +18);
                break;
            case "6": //wait for confirm calving
                dayend.add(Calendar.DATE, +365);
                break;
        }
        return dayend.get(Calendar.YEAR)+"-"+(dayend.get(Calendar.MONTH)+1)+"-"+dayend.get(Calendar.DAY_OF_MONTH);
    }
    public String getDayTH(String dateEn){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar date =  Calendar.getInstance();
        try {
            date.setTime(df.parse(dateEn));
        } catch (ParseException e) {
            e.printStackTrace();
            return "-";
        }
        switch(date.get(Calendar.MONTH)+1){
            case 1:
                return date.get(Calendar.DAY_OF_MONTH) + " ม.ค " +(date.get(Calendar.YEAR)+543);
            case 2:
                return date.get(Calendar.DAY_OF_MONTH) + " ก.พ. " +(date.get(Calendar.YEAR)+543);
            case 3:
                return date.get(Calendar.DAY_OF_MONTH) + " มี.ค. " +(date.get(Calendar.YEAR)+543);
            case 4:
                return date.get(Calendar.DAY_OF_MONTH) + " เม.ย. " +(date.get(Calendar.YEAR)+543);
            case 5:
                return date.get(Calendar.DAY_OF_MONTH) + " พ.ค. " +(date.get(Calendar.YEAR)+543);
            case 6:
                return date.get(Calendar.DAY_OF_MONTH) + " มิ.ย. " +(date.get(Calendar.YEAR)+543);
            case 7:
                return date.get(Calendar.DAY_OF_MONTH) + " ก.ค. " +(date.get(Calendar.YEAR)+543);
            case 8:
                return date.get(Calendar.DAY_OF_MONTH) + " ส.ค. " +(date.get(Calendar.YEAR)+543);
            case 9:
                return date.get(Calendar.DAY_OF_MONTH) + " ก.ย. " +(date.get(Calendar.YEAR)+543);
            case 10:
                return date.get(Calendar.DAY_OF_MONTH) + " ต.ค. " +(date.get(Calendar.YEAR)+543);
            case 11:
                return date.get(Calendar.DAY_OF_MONTH) + " พ.ย. " +(date.get(Calendar.YEAR)+543);
            case 12:
                return date.get(Calendar.DAY_OF_MONTH) + " ธ.ค. " +(date.get(Calendar.YEAR)+543);
            default:
                return "-";
        }
    }

}

