package com.kasidid.mrbboomm.cowlog;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mrbboomm on 7/11/2016.
 */
public class Menu_CowDB_dad_Fragment extends Fragment{
    COWHelper mHelper;
    List<String> cows_id,cows_nickname;
    RecycleAdapter mAdapter;
    static String username;
    RecyclerView mRecyclerView;
    @Override
    public void onResume() {
        super.onResume();
        getActivity().findViewById(R.id.search_list).setVisibility(View.GONE);
        cows_id = mHelper.getCowArray(username,0,2,true);
        String[] ids = cows_id.toArray(new String[0]);
        mAdapter = new RecycleAdapter(getActivity(),ids);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        getActivity().getMenuInflater().inflate(R.menu.menu_search,menu);
        final MenuItem item = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                query = null;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String[] ids;
                if (newText.length() == 0){
                    cows_id = mHelper.getCowArray(username,0,2,true);
                    ids = cows_id.toArray(new String[0]);
                }
                else{
                    List<String> cows_filter = new ArrayList<String>();
                    cows_id = mHelper.getCowArray(username,0,2,true);
                    cows_nickname = mHelper.getCowArray(username,15,2,true);
                    for(int i=0;i<cows_nickname.size();i++)
                    {
                        if(cows_nickname.get(i).toLowerCase().startsWith(newText.toLowerCase()))
                        {
                            cows_filter.add(cows_id.get(i));
                        }
                    }
                    ids = cows_filter.toArray(new String[0]);
                }

                mAdapter = new RecycleAdapter(getActivity(),ids);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mAdapter);
                return false;
            }

        });
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        username =  prefs.getString("username","not found");
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.userprofile_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("รายการโคเพศผู้");
        mHelper = new COWHelper(getActivity());

        View view = inflater.inflate(R.layout.menu_cowdb2_f, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.listView1);

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        Button mAdd = (Button)view.findViewById(R.id.adding);
        mAdd.setText("+เพิ่มข้อมูลโคเพศผู้");
        mAdd.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/THSarabun.ttf"));
        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Edit_Add.class);
                intent.putExtra("type",String.valueOf(2)); //huffer
                getActivity().startActivity(intent);
            }

        });
        return view;
    }

    private static class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ViewHolder>{
        Context mContext;
        String[] ID;
        public RecycleAdapter(Context context, String[] ids){
            mContext = context;
            ID = ids;
        }
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            TextView Cow,avg,total;
            ImageView imageView;

            public ViewHolder(View view) {
                super(view);
                Cow = (TextView)view.findViewById(R.id.text_cowname);
                avg = (TextView)view.findViewById(R.id.avg);
                total = (TextView)view.findViewById(R.id.total);
                imageView = (ImageView)view.findViewById(R.id.imageView1);

                view.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext,Cow_Profile2.class);
                intent.putExtra("ID",ID[getAdapterPosition()]);
                mContext.startActivity(intent);

            }
        }
        @Override
        public RecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.menu_cowdb_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecycleAdapter.ViewHolder holder, int position) {
            COWHelper mHelper = new COWHelper(mContext);
            Cow c = mHelper.getCowByID(String.valueOf(ID[position]));
            MilkHelper milkHelper = new MilkHelper(mContext);
            if (c.getNickname().equals("")) holder.Cow.setText(c.getCowname());
            else holder.Cow.setText(c.getNickname());
            holder.avg.setText("สายพันธุ์ "+c.getFraction());
            holder.total.setText("เกิดเมื่อวันที่ "+c.getDayTH(c.getBirth()));
            Picasso.with(mContext).load("https://info.rdi.ku.ac.th/cowapp/manager/images/"+c.getUsername()+"/"+c.getCowname()+"/1.png").error(R.drawable.cow_acc).resize(300,300).centerInside().into(holder.imageView);
        }

        @Override
        public int getItemCount() {
            return ID.length;
        }
    }



}


