package com.kasidid.mrbboomm.cowlog;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/19/2016.
 */
public class Edit_Edit extends AppCompatActivity implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{
    private int ID = -1;
    COWHelper mHelper;
    CALVINGHelper cHelper = new CALVINGHelper(this);
    String id;
    int birthdate_request=564564,request_datetime=54564646,calvingdate_request=434856345;

    String username,real_birth,real_calvingdate;
    EditText etNickname,etDetail,etBirth,etcalvingdate,etpapa,etpapaf,etmama,etmamaf;
    Cow c;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");
        setContentView(R.layout.edit_edit);
        mHelper = new COWHelper(this);
        Intent intent = getIntent();
        id = intent.getStringExtra("ID");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_edit);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        etNickname = (EditText) findViewById(R.id.cow_nickname_edit);
        etDetail = (EditText) findViewById(R.id.cow_detail_edit);
        etBirth = (EditText) findViewById(R.id.txtdate_edit);
        etcalvingdate = (EditText) findViewById(R.id.calvingdate_edit);
        etpapa = (EditText) findViewById(R.id.cow_papa_edit);
        etpapaf = (EditText) findViewById(R.id.cow_papaf_edit);
        etmama = (EditText) findViewById(R.id.cow_mama_edit);
        etmamaf = (EditText) findViewById(R.id.cow_mamaf_edit);

        c = mHelper.getCowByID(id);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("ประวัติโค");

        etNickname.setText(c.getNickname());
        etDetail.setText(c.getFraction());
        etBirth.setText(c.getDayTH(c.getBirth()));
        real_birth = c.getBirth();
        etpapa.setText(c.getPapa());
        etpapaf.setText(c.getPapaf());
        etmama.setText(c.getMama());
        etmamaf.setText(c.getMamaf());
        Calving calving = cHelper.getCalving(c.getCowname(),c.getParity());
        try {
            etcalvingdate.setText(c.getDayTH(calving.getCalvingdate()));
            real_calvingdate = calving.getCalvingdate();
        }catch (Exception e){
            etcalvingdate.setText(c.getDayTH(c.getCalvdate()));
            real_calvingdate = c.getCalvdate();
        }
        if (c.getType().equals("0") || c.getType().equals("2")){
            findViewById(R.id.calvinglayout).setVisibility(View.GONE);
        }
        etBirth.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = birthdate_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Edit.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        etcalvingdate.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = calvingdate_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Edit.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        ImageView check = (ImageView)findViewById(R.id.check);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editclick();
            }
        });
    }


    @Override
    public void onDateSet(com.layernet.thaidatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        if (request_datetime == birthdate_request){
            real_birth = date;
            etBirth.setText(c.getDayTH(date));
        }
        if (request_datetime == calvingdate_request){
            real_calvingdate = date;
            etcalvingdate.setText(c.getDayTH(date));
        }
    }
    public void editclick(){
        c.setNickname(etNickname.getText().toString());
        c.setFraction(etDetail.getText().toString());
        c.setBirth(real_birth);
        c.setPapa(etpapa.getText().toString());
        c.setPapaf(etpapaf.getText().toString());
        c.setMama(etmama.getText().toString());
        c.setMamaf(etmamaf.getText().toString());
        c.setCalvdate(real_calvingdate);

        if (ID == -1) {
            mHelper.updateCow(mHelper.StateUpdate(c));
            if (c.getType().equals("1")){
                Calving calving = new Calving();
                calving.setCowname(c.getCowname());
                calving.setParity(c.getParity());
                calving.setCalvingdate(real_calvingdate);
                calving.setUsername(c.getUsername());
                calving.setRestday("-");
                calving.setKid("-");
                cHelper.addCalving(calving);
                new Sync_Calving(calving).execute();
            }
//

        } else {
            c.setId(ID);
        }
        new Sync_DB(c).execute();
        finish();

    }
//    public void deleteclick(View view){
//        AlertDialog.Builder builder =
//                new AlertDialog.Builder(Edit_Edit.this);
//        builder.setMessage("ต้องการลบโค "+c.getCowname()+" ?");
//        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int idd) {
//                mHelper.deleteCow(String.valueOf(c.getId()));
//                new Sync_DeleteCow(Edit_Edit.this, c).execute();
//                new Sync_DeleteCalving(Edit_Edit.this, c).execute();
//                cHelper.deleteCalving(c.getCowname());
//
//            }
//        });
//        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        builder.show();
//    }

}


