package com.kasidid.mrbboomm.cowlog;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by mrbboomm on 7/4/2016.
 */

public class Menu_Main extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private ActionBarDrawerToggle mDrawerToggle;
    private String[] mDrawerTitle = {"สถานะโค","ข้อมูลโครีด","ข้อมูลโคทดแทน","ข้อมูลโคเพศผู้","รายงาน","ซิงค์ข้อมูล","ออกจากระบบ"};
    private DrawerLayout mDrawerLayout;
    private ListView mListView;
    String username;
    private GoogleApiClient mGoogleApiClient;
    private MyAdapter myAdapter;
    ArrayAdapter<String> adapter,adapter2;
    SearchView searchView;
    COWHelper mHelper;

    @Override
    protected void onResume() {
        super.onResume();

        try {
            new Sync_DB_cow().PushDB();
//            new Sync_DB_calving().PushDB();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new COWHelper(this).dailyUpdate();
        findViewById(R.id.search_list).setVisibility(View.GONE);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(final String hostname, final SSLSession session) {
                if (hostname.equals("info.rdi.ku.ac.th")){
                    Log.d("sslhostname",hostname);
                    return true;
                }
                else return false;
            }
        });
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");
        if (!new FARMHelper(this).checkFarm(username)){
            startActivity(new Intent(this,Edit_Add_Farm.class));
            
        }
        else{
            Farm farm = new FARMHelper(this).getFarm(username);
            new Sync_Farm(farm).execute();
        }
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String version =  prefs.getString("version","not found");
        if (version.isEmpty() || !version.equals(pInfo.versionName) ){
            prefs.edit().putString("version", pInfo.versionName).commit();
            Log.d("first","firsttime");
//            firsttimeupdatenotification
        }

        setContentView(R.layout.menu_main);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        mHelper = new COWHelper(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.userprofile_toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mListView = (ListView) findViewById(R.id.drawer);
        myAdapter = new MyAdapter(this,mDrawerTitle);
        mListView.setAdapter(myAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectFragment(position,username);
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close ) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
                syncState();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();
        selectFragment(0,username);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.getItemId()==R.id.Refresh)
        {


        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        return super.onCreateOptionsMenu(menu);
    }


    private void selectFragment(int position, String username) {
        Fragment fragment = null;

        switch (position){
            case 0: {
                fragment = new Menu_Activity_Fragment();
                break;
            }
            case 1: {
                fragment = new Menu_CowDB_cow_Fragment();
                break;
            }
            case 2: {
                fragment = new Menu_CowDB_huffer_Fragment();
                break;
            } case 3: {
                fragment = new Menu_CowDB_dad_Fragment();
                break;
            }case 4: {
                fragment = new Menu_Farm_Fragment();
                break;
            }

            case 5:{
                fragment = new Menu_Activity_Fragment();
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(Menu_Main.this);
                builder.setMessage("ขั้นตอนนี้ใช้เวลาสักครู่");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                new Sync_DB_cow().PushDB();
                                new Sync_DB_calving().PushDB();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            if (checkDatabase("data/data/com.kasidid.mrbboomm.cowlog/databases/cbreed.db")){

                                try {
                                    new Sync_DB_cbreed().PushDB();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                            if (checkDatabase("data/data/com.kasidid.mrbboomm.cowlog/databases/milk.db")){
                                try {
                                    new Sync_DB_milk().PushDB();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (checkDatabase("data/data/com.kasidid.mrbboomm.cowlog/databases/breed.db")){
                                try {
                                    new Sync_DB_breed().PushDB();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                break;
            }
            case 6:{
                fragment = new Menu_Activity_Fragment();
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(Menu_Main.this);
                builder.setMessage("ต้องการออกจากระบบ");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {
                            new Sync_DB_cow().PushDB();
                            new Sync_DB_calving().PushDB();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (checkDatabase("data/data/com.kasidid.mrbboomm.cowlog/databases/cbreed.db")){

                            try {
                                new Sync_DB_cbreed().PushDB();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        if (checkDatabase("data/data/com.kasidid.mrbboomm.cowlog/databases/milk.db")){
                            try {
                                new Sync_DB_milk().PushDB();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        // [START_EXCLUDE]
                                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Menu_Main.this);
                                        prefs.edit().clear().commit();
                                        Intent intent = new Intent(Menu_Main.this, Login_Google.class);
                                        startActivity(intent);
                                        // [END_EXCLUDE]
                                    }
                                });

                        finish();

                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                break;
            }

        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
        mDrawerLayout.closeDrawers();
        }
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public class Sync_DB_cow {
        public  void PushDB() throws JSONException {
            String id="",username="",cowname="",fraction="",birth="",daystart="",status="",dayend="",daymilk="",state="",papa="",papaf="",mama="",mamaf="",type="",nickname="",parity ="";
            String myPath = "data/data/com.kasidid.mrbboomm.cowlog/databases/cow.db";// Set path to your database

            String myTable = Cow.TABLE;//Set name of your table


            SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            String searchQuery = "SELECT  * FROM " + myTable + " WHERE 1";
            Cursor cursor = myDataBase.rawQuery(searchQuery, new String[]{});

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {

                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {
                            if (cursor.getString(i) != null) {

                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                rowObject.put(cursor.getColumnName(i), "xxx");
                            }
                        } catch (Exception e) {
                            //Log.d("TAG_NAME", e.getMessage());
                        }
                    }
                    else{
                        rowObject.put(cursor.getColumnName(i), "xxx");
                    }

                }
                id += cursor.getString(0)+"cxcd";
                username += cursor.getString(1)+"cxcd";
                cowname += cursor.getString(2)+"cxcd";
                fraction += cursor.getString(3)+"cxcd";
                birth += cursor.getString(4)+"cxcd";
                daystart += cursor.getString(5)+"cxcd";
                status += cursor.getString(6)+"cxcd";
                dayend += cursor.getString(7)+"cxcd";
                daymilk += cursor.getString(8)+"cxcd";
                state += cursor.getString(9)+"cxcd";
                papa += cursor.getString(10)+"cxcd";
                papaf += cursor.getString(11)+"cxcd";
                mama += cursor.getString(12)+"cxcd";
                mamaf += cursor.getString(13)+"cxcd";
                type += cursor.getString(14)+"cxcd";
                nickname += cursor.getString(15)+"cxcd";
                parity += cursor.getString(16)+"cxcd";

                cursor.moveToNext();
            }
            HashMap<String,String> postData = new HashMap<String,String>();
            postData.put("username",username );
            postData.put("cowname",cowname );
            postData.put("fraction", fraction);
            postData.put("birth",birth );
            postData.put("daystart", daystart);
            postData.put("status",status );
            postData.put("dayend",dayend );
            postData.put("daymilk",daymilk );
            postData.put("state",state );
            postData.put("papa",papa );
            postData.put("papaf",papaf );
            postData.put("mama", mama);
            postData.put("mamaf",mamaf );
            postData.put("type",type );
            postData.put("nickname", nickname);
            postData.put("parity",parity );

            new pushbyPost(Menu_Main.this,"https://info.rdi.ku.ac.th/cowapp/manager/updateallcows.php", postData).execute();
            cursor.close();
            myDataBase.close();
        }
    }
    public class Sync_DB_calving {
        public  void PushDB() throws JSONException {
            String id="",username="",cowname="",parity ="",calvingdate="",restday="",kid="";
            String myPath = "data/data/com.kasidid.mrbboomm.cowlog/databases/calving.db";// Set path to your database

            String myTable = Calving.TABLE;//Set name of your table


            SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            String searchQuery = "SELECT  * FROM " + myTable + " WHERE 1";
            Cursor cursor = myDataBase.rawQuery(searchQuery, new String[]{});

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {

                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {
                            if (cursor.getString(i) != null) {

                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                rowObject.put(cursor.getColumnName(i), "xxx");
                            }
                        } catch (Exception e) {
                            //Log.d("TAG_NAME", e.getMessage());
                        }
                    }
                    else{
                        rowObject.put(cursor.getColumnName(i), "xxx");
                    }

                }
                id += cursor.getString(0)+"cxcd";
                username += cursor.getString(1)+"cxcd";
                cowname += cursor.getString(2)+"cxcd";
                parity += cursor.getString(3)+"cxcd";
                calvingdate += cursor.getString(4)+"cxcd";
                restday += cursor.getString(5)+"cxcd";
                kid += cursor.getString(6)+"cxcd";
                cursor.moveToNext();

            }
            HashMap<String,String> postData = new HashMap<String,String>();
            postData.put("username",username );
            postData.put("cowname",cowname );
            postData.put("parity",parity );
            postData.put("calvingdate",calvingdate );
            postData.put("restday",restday );
            postData.put("kid",kid );
            new pushbyPost(Menu_Main.this, "https://info.rdi.ku.ac.th/cowapp/manager/updateallcalving.php", postData).execute();
            cursor.close();
        myDataBase.close();
        }

    }

    public class Sync_DB_cbreed {
        public  void PushDB() throws JSONException {
            String id="",username="",cowname="",parity ="",papa="",breeddate="",times="";
            String myPath = null;

            myPath = "data/data/com.kasidid.mrbboomm.cowlog/databases/cbreed.db";// Set path to your database

            String myTable = CBreed.TABLE;//Set name of your table

            SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

            String searchQuery = "SELECT  * FROM " + myTable + " WHERE 1";
            Cursor cursor = myDataBase.rawQuery(searchQuery, new String[]{});

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {

                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {
                            if (cursor.getString(i) != null) {

                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                rowObject.put(cursor.getColumnName(i), "xxx");
                            }
                        } catch (Exception e) {
                            //Log.d("TAG_NAME", e.getMessage());
                        }
                    }
                    else{
                        rowObject.put(cursor.getColumnName(i), "xxx");
                    }

                }
                id += cursor.getString(0)+"cxcd";
                username += cursor.getString(1)+"cxcd";
                cowname += cursor.getString(2)+"cxcd";
                parity += cursor.getString(3)+"cxcd";
                papa += cursor.getString(4)+"cxcd";
                breeddate += cursor.getString(5)+"cxcd";
                times += cursor.getString(6)+"cxcd";
                cursor.moveToNext();

            }
            HashMap<String,String> postData = new HashMap<String,String>();
            postData.put("username",username );
            postData.put("cowname",cowname );
            postData.put("parity",parity );
            postData.put("papa",papa );
            postData.put("breeddate",breeddate );
            postData.put("times",times );
            new pushbyPost(Menu_Main.this, "https://info.rdi.ku.ac.th/cowapp/manager/updateallcbreeds.php", postData).execute();
            cursor.close();
            myDataBase.close();
        }

    }
    public class Sync_DB_breed {
        public  void PushDB() throws JSONException {
            String id="",username="",cowname="",parity ="",papa="",breeddate="";
            String myPath = null;

            myPath = "data/data/com.kasidid.mrbboomm.cowlog/databases/breed.db";// Set path to your database

            String myTable = Breed.TABLE;//Set name of your table
            SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            String searchQuery = "SELECT  * FROM " + myTable + " WHERE 1";
            Cursor cursor = myDataBase.rawQuery(searchQuery, new String[]{});

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {

                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {
                            if (cursor.getString(i) != null) {

                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                rowObject.put(cursor.getColumnName(i), "xxx");
                            }
                        } catch (Exception e) {
                            //Log.d("TAG_NAME", e.getMessage());
                        }
                    }
                    else{
                        rowObject.put(cursor.getColumnName(i), "xxx");
                    }

                }
                id += cursor.getString(0)+"cxcd";
                username += cursor.getString(1)+"cxcd";
                cowname += cursor.getString(2)+"cxcd";
                parity += cursor.getString(3)+"cxcd";
                papa += cursor.getString(4)+"cxcd";
                breeddate += cursor.getString(5)+"cxcd";
                cursor.moveToNext();

            }
            HashMap<String,String> postData = new HashMap<String,String>();
            postData.put("username",username );
            postData.put("cowname",cowname );
            postData.put("parity",parity );
            postData.put("papa",papa );
            postData.put("breeddate",breeddate );
            new pushbyPost(Menu_Main.this, "https://info.rdi.ku.ac.th/cowapp/manager/updateallbreeds.php", postData).execute();
            cursor.close();
            myDataBase.close();
        }

    }
    public class Sync_DB_milk {
        public  void PushDB() throws JSONException {
            String id="",username="",cowname="",parity ="",volume="",fat="",protein="",lactos="",solid="",solidmilk="",milkdate="",calvingdate="";
            String myPath = null;

            myPath = "data/data/com.kasidid.mrbboomm.cowlog/databases/milk.db";// Set path to your database

            String myTable = Milk.TABLE;//Set name of your table


            SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
            String searchQuery = "SELECT  * FROM " + myTable + " WHERE 1";
            Cursor cursor = myDataBase.rawQuery(searchQuery, new String[]{});

            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {

                int totalColumn = cursor.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {
                            if (cursor.getString(i) != null) {

                                rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                rowObject.put(cursor.getColumnName(i), "xxx");
                            }
                        } catch (Exception e) {
                            //Log.d("TAG_NAME", e.getMessage());
                        }
                    }
                    else{
                        rowObject.put(cursor.getColumnName(i), "xxx");
                    }

                }
                id += cursor.getString(0)+"cxcd";
                username += cursor.getString(1)+"cxcd";
                cowname += cursor.getString(2)+"cxcd";
                milkdate += cursor.getString(3)+"cxcd";
                volume += cursor.getString(4)+"cxcd";
                fat += cursor.getString(5)+"cxcd";
                protein += cursor.getString(6)+"cxcd";
                lactos += cursor.getString(7)+"cxcd";
                solid += cursor.getString(8)+"cxcd";
                solidmilk += cursor.getString(9)+"cxcd";
                calvingdate += cursor.getString(10)+"cxcd";
                parity += cursor.getString(11)+"cxcd";
                cursor.moveToNext();

            }
            HashMap<String,String> postData = new HashMap<String,String>();
            postData.put("username",username );
            postData.put("cowname",cowname );
            postData.put("milkdate",milkdate );
            postData.put("volume",volume );
            postData.put("fat",fat );
            postData.put("protein",protein);
            postData.put("lactos",lactos );
            postData.put("solid",solid );
            postData.put("solidmilk",solidmilk);
            postData.put("calvingdate",calvingdate);
            postData.put("parity",parity);
            new pushbyPost(Menu_Main.this, "https://info.rdi.ku.ac.th/cowapp/manager/updateallmilk.php", postData).execute();
            cursor.close();
            myDataBase.close();
        }

    }

    class MyAdapter extends BaseAdapter{
        String [] menu;
        int[] images = {R.drawable.ic_action_name,R.drawable.ic_action_cows1,R.drawable.ic_action_huffer1,R.drawable.ic_action_huffer1,R.drawable.ic_action_farm,R.drawable.ic_action_sync,R.drawable.ic_action_signout};

        private Context context;
        public MyAdapter(Context context,String[] menu){
            this.context = context;
            this.menu = menu;
        }
        @Override
        public int getCount() {
            return this.menu.length;
        }

        @Override
        public Object getItem(int i) {
            return this.menu[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View row = null;
            if (view == null){
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.drawer_item,viewGroup,false);
            }
            else{
                row = view;
            }
            TextView text = (TextView) row.findViewById(R.id.drawer_text);
            ImageView img = (ImageView) row.findViewById(R.id.drawer_img);
            String fontPath = "fonts/THSarabun.ttf";
            Typeface tf = Typeface.createFromAsset(getAssets(),fontPath);
            text.setText(menu[i]);
            text.setTypeface(tf);
            img.setImageResource(images[i]);
            return row;
        }
    }
    private boolean checkDatabase(String DB_PATH){
        File dbFile = new File(DB_PATH);
        if(dbFile.exists()){
            return true;
        }
        else{
            //This'll create the directories you wanna write to, so you
            //can put the DB in the right spot.
            dbFile.getParentFile().mkdirs();
            return false;
        }
    }


}

