package com.kasidid.mrbboomm.cowlog;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by mrbboomm on 9/5/2016.
 */
public class MilkHelper extends SQLiteOpenHelper {
    public MilkHelper(Context context) {
        super(context, "milk.db", null, 10);
    }
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TASK_TABLE = String.format("CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                "%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,UNIQUE(%s,%s, %s) ON CONFLICT REPLACE)",
                Milk.TABLE,
                Milk.Column.ID,
                Milk.Column.USERNAME,
                Milk.Column.COWNAME,
                Milk.Column.MILKDATE,
                Milk.Column.VOLUME,
                Milk.Column.FAT,
                Milk.Column.PROTEIN,
                Milk.Column.LACTOS,
                Milk.Column.SOLID,
                Milk.Column.SOLIDMILK,
                Milk.Column.CALVDATE,
                Milk.Column.PARITY,
                Milk.Column.COWNAME,Milk.Column.MILKDATE,Milk.Column.PARITY);

        db.execSQL(CREATE_TASK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String DROP_Milk_TABLE = "DROP TABLE IF EXISTS milk";
        db.execSQL(DROP_Milk_TABLE);
        onCreate(db);
    }
    public void addMilk(Milk milk){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values =getContent(milk);
        db.insert(Milk.TABLE, null, values);
        db.close();
    }
    public void clearMilk(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Milk.TABLE, null, null);
        db.close();
    }
    public void deleteMilkbyName(String cowname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Milk.TABLE, Milk.Column.COWNAME + "='" + cowname+"'", null);
        db.close();
    }
    public ContentValues getContent(Milk milk){
        ContentValues values = new ContentValues();
        values.put(Milk.Column.USERNAME, milk.getUsername());
        values.put(Milk.Column.COWNAME, milk.getCowname());
        values.put(Milk.Column.MILKDATE, milk.getMilkdate());
        values.put(Milk.Column.VOLUME, milk.getVolume());
        values.put(Milk.Column.FAT,milk.getFat());
        values.put(Milk.Column.PROTEIN,milk.getProtien());
        values.put(Milk.Column.LACTOS,milk.getLactos());
        values.put(Milk.Column.SOLID,milk.getSolid());
        values.put(Milk.Column.SOLIDMILK,milk.getSolidmilk());
        values.put(Milk.Column.CALVDATE,milk.getCalvdate());
        values.put(Milk.Column.PARITY,milk.getParity());
        return  values;
    }
    public float[] getMilkVolume(Context mcontext,String cowname,String parity) {
        float total = 0; //0
        int count = 0; //1
        final List<Integer> x = new ArrayList<>();
        final List<Integer> x_sorted = new ArrayList<>();
//        getcalvingdate from calvinghelper
//        ==================================
        CALVINGHelper cHelper = new CALVINGHelper(mcontext);
        Calving calving = cHelper.getCalving(cowname,parity);
        String calving_date = calving.getCalvingdate();
        try{
            Log.d("calvingdate",calving_date+" "+cowname+" "+parity);
        }catch (Exception e){

            Log.d("calvingdate","not found"+" "+cowname+" "+parity);
        }
//        try it!!!! with db change!!
//        ==================================
        List<Float> y = new ArrayList<>();
        List<Float> y_sorted = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar daystart = Calendar.getInstance();
        Calendar dayend = Calendar.getInstance();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query
                (Milk.TABLE, null, Milk.Column.COWNAME + "=?",
                        new String[]{cowname}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        String tmp_calvingday= "";
        int found = 0;
        while (!cursor.isAfterLast()) {
            try {

                try{
                     daystart.setTime(df.parse(calving_date));
                    Log.d("Found",calving_date + "and" + cursor.getString(10));
                    tmp_calvingday = calving_date;

                }
                catch (Exception e){
                    daystart.setTime(df.parse(cursor.getString(10)));
                    tmp_calvingday = cursor.getString(10);
                }
                dayend.setTime(df.parse(cursor.getString(3)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long diff = (dayend.getTimeInMillis() - daystart.getTimeInMillis())/ (24 * 60 * 60 * 1000);
            if (!cursor.getString(11).equals(parity) || dayend.getTimeInMillis() - daystart.getTimeInMillis() < 0  ){
                cursor.moveToNext();
                continue;
            }
            if (x.contains((int)diff)){ //more than 1 record in one day
                y.set(x.indexOf((int)diff), y.get(x.indexOf((int)diff))  +  Float.parseFloat(cursor.getString(4))) ;
            }
            else{
                x.add((int) (diff));
                x_sorted.add((int)(diff));
                y.add(Float.valueOf(cursor.getString(4)));
            }
            found = 1;
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.close();
        if (found == 1){
            Collections.sort(x_sorted);
            for (int i=0;i<x.size();i++){
                y_sorted.add(y.get(x.indexOf(x_sorted.get(i))));
//                Log.d("sort",x_sorted.get(i).toString() + " "+y_sorted.get(i));
                if(i==0){
                    if(x_sorted.get(0)==0) x_sorted.set(0,1);
                    total += y_sorted.get(0)*(x_sorted.get(0));
                    count = x_sorted.get(0);
                }
                else{ //y=milkyield x=time
                    total += (x_sorted.get(i)-x_sorted.get(i-1))* (y_sorted.get(i)+y_sorted.get(i-1))/2;
                    count = x_sorted.get(i);
                }
            }
            CALVINGHelper calvingHelper = new CALVINGHelper(mcontext);
//aow ook w
//            if (!calvingHelper.getReport(cowname,parity).get(2).equals("-")){
//                try {
//                    daystart.setTime(df.parse(tmp_calvingday));
//                    dayend.setTime(df.parse(calvingHelper.getReport(cowname,parity).get(2))); //restday
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                long diff = (dayend.getTimeInMillis() - daystart.getTimeInMillis())/ (24 * 60 * 60 * 1000);
//                total += Math.abs(Math.abs(diff)-x_sorted.get(x_sorted.size()-1)) * y_sorted.get(y_sorted.size()-1);
//                count = Math.abs((int)diff);
//            }
        }



        if (count == 0)
            return new float[]{0, 0, 0};
        else
            return new float[]{total/count,total,count};

    }
    public void deleteMilk(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Milk.TABLE, Milk.Column.ID + " = " + id, null);
        db.close();
    }
    public List<String> getMilkArray(String cowname,int column,String parity) {
        List<String> milks = new ArrayList<String>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();
        cursor = db.query
                (Milk.TABLE, null, Milk.Column.COWNAME + "=?", new String[]{cowname}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            if(cursor.getString(11).equals(parity))
                milks.add(cursor.getString(column));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return milks;
    }
    public Milk getMilkByID(String id){
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();
        Milk milk = new Milk();
        cursor =  db.rawQuery("select * from " +Milk.TABLE + " where " + Milk.Column.ID + "=" + id, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        if (!cursor.isAfterLast()) {
            milk.setId(cursor.getInt(0));
            milk.setUsername(cursor.getString(1));
            milk.setCowname(cursor.getString(2));
            milk.setMilkdate(cursor.getString(3));
            milk.setVolume(cursor.getString(4));
            milk.setFat(cursor.getString(5));
            milk.setProtien(cursor.getString(6));
            milk.setLactos(cursor.getString(7));
            milk.setSolid(cursor.getString(8));
            milk.setSolidmilk(cursor.getString(9));
            milk.setCalvdate(cursor.getString(10));
            milk.setParity(cursor.getString(11));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return milk;
    }
    public Milk plusMilk(Milk milk){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        Cursor cursor = sqLiteDatabase.query
                (Milk.TABLE, null, Milk.Column.COWNAME+"=? and "+Milk.Column.PARITY+"=? and "+Milk.Column.MILKDATE+"=?",
                        new String[]{milk.getCowname(),milk.getParity(),milk.getMilkdate()}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        double volume = Double.parseDouble(milk.getVolume());
        while(!cursor.isAfterLast()) {
            milk.setVolume(String.valueOf(Double.parseDouble(cursor.getString(4))+volume));
            cursor.moveToNext();
        }
        cursor.close();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values =getContent(milk);
        db.insert(Milk.TABLE, null, values);
        db.close();
        return  milk;
    }


}