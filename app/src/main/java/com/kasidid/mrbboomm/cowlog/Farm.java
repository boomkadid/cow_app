package com.kasidid.mrbboomm.cowlog;

import android.provider.BaseColumns;

/**
 * Created by Kasidid on 2/27/2017.
 */

public class Farm {
    private int id;
    private String username;
    private String farmname;
    private String farmid;
    private String owner;
    private String address;
    public static final String TABLE  = "farm";
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String USERNAME = "username";
        public static final String FARMID = "farmid";
        public static final String FARMNAME = "farmname";
        public static final String OWNER = "owner";
        public static final String ADDRESS = "address";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFarmname() {
        return farmname;
    }

    public void setFarmname(String farmname) {
        this.farmname = farmname;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFarmid() {
        return farmid;
    }

    public void setFarmid(String farmid) {
        this.farmid = farmid;
    }
}
