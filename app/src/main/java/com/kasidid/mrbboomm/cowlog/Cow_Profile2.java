package com.kasidid.mrbboomm.cowlog;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
// other imports and package statement are omitted

public class Cow_Profile2 extends AppCompatActivity {
    Toolbar toolbar;
    String username;
    COWHelper mHelper;
    ImageView image_profile;
    TextView addmilk_text,breed_text,milk_text,edit_status_text,milk_record_text;
    LinearLayout edit,report,milk,addmilk,milk_record,breed,edit_status,back,edit_image_profile,delete;
    Cow cow;
    String id;
    CollapsingToolbarLayout ctoolbar;
    //reimage handle
    final int MY_PERMISSIONS_REQUEST_CAMERA = 44444;
    final int MY_PERMISSIONS_REQUEST_EXTERNAL = 55555;
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;
    final int CAMERA_REQUEST = 13322;
    final int GALLERY_REQUEST = 22132;
    final int GALLERY_REQUEST2 = 13722;
    Bitmap bitbit;
    String selectedPhoto;
    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (cow.getType().equals("0")) {
            milk_record.setEnabled(false);
            milk_record_text.setTextColor(Color.parseColor("#DCDCDC"));
            milk.setEnabled(false);
            milk_text.setTextColor(Color.parseColor("#DCDCDC"));
            addmilk.setEnabled(false);
            addmilk_text.setTextColor(Color.parseColor("#DCDCDC"));

        }else if(cow.getType().equals("2")) {
            milk_record.setEnabled(false);
            milk_record_text.setTextColor(Color.parseColor("#DCDCDC"));
            milk.setEnabled(false);
            milk_text.setTextColor(Color.parseColor("#DCDCDC"));
            addmilk.setEnabled(false);
            addmilk_text.setTextColor(Color.parseColor("#DCDCDC"));
            breed.setEnabled(false);
            breed_text.setTextColor(Color.parseColor("#DCDCDC"));
            edit_status.setEnabled(false);
            edit_status_text.setTextColor(Color.parseColor("#DCDCDC"));

        }
        else if(cow.getType().equals("1") && !(new CALVINGHelper(this).getReport(cow.getCowname(),cow.getParity()).get(2).equals("-"))){
                addmilk.setEnabled(false);
                addmilk_text.setTextColor(Color.parseColor("#DCDCDC"));

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cow_profile2);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        ImageView photo_image = (ImageView) findViewById(R.id.photo);
        photo_image.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.camera,200,200));
        photo_image.setAlpha(0.7f);
        username =  prefs.getString("username","not found");
        mHelper = new COWHelper(this);
        final Intent intent = getIntent();
        id = intent.getStringExtra("ID");
        cow = mHelper.getCowByID(id);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ctoolbar = (CollapsingToolbarLayout)findViewById(R.id.collapsingtoolbar);
        ctoolbar.setTitle("โคหมายเลข "+cow.getCowname());
        ctoolbar.setCollapsedTitleTypeface( Typeface.createFromAsset(getAssets(),"fonts/THSarabun.ttf"));
        ctoolbar.setCollapsedTitleGravity(Gravity.LEFT);
        ctoolbar.setExpandedTitleTypeface(Typeface.createFromAsset(getAssets(),"fonts/THSarabun.ttf"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        image_profile = (ImageView)findViewById(R.id.image_profile);
        Picasso.with(this).load("https://info.rdi.ku.ac.th/cowapp/manager/images/"+cow.getUsername()+"/"+cow.getCowname()+"/1.png").error(R.drawable.cow_acc).resize(350,350).centerInside().into(image_profile);
        //handle image
        ImageView info_cow = (ImageView)findViewById(R.id.info_cow);
        ImageView gender_icon = (ImageView)findViewById(R.id.gender_icon);
        ImageView info_milk = (ImageView)findViewById(R.id.info_milk);
        ImageView add_milk_icon = (ImageView)findViewById(R.id.add_milk_icon);
        ImageView records_icon = (ImageView)findViewById(R.id.records_icon);
        ImageView report_icon = (ImageView)findViewById(R.id.report_icon);
        ImageView edit_icon = (ImageView)findViewById(R.id.edit_icon);
        ImageView remove_icon = (ImageView)findViewById(R.id.remove_icon);
        ImageView ic_action_signout = (ImageView)findViewById(R.id.ic_action_signout);

        info_cow.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.info_cow,50,50));
        gender_icon.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.gender_icon,50,50));
        info_milk.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.info_milk,50,50));
        add_milk_icon.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.add_milk_icon,50,50));
        records_icon.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.records_icon,50,50));
        report_icon.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.report_icon,50,50));
        edit_icon.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.edit_icon,50,50));
        remove_icon.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.remove_icon,50,50));
        ic_action_signout.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(this.getResources(),R.drawable.ic_action_signout,50,50));



        edit = (LinearLayout) findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,Edit_Edit.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        report = (LinearLayout) findViewById(R.id.report);
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,report_Cow.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        addmilk_text = (TextView) findViewById(R.id.add_milk_text);
        addmilk = (LinearLayout) findViewById(R.id.add_milk);
        addmilk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,Cow_Profile_Milk_Add.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        milk_text = (TextView) findViewById(R.id.milk_text);
        milk = (LinearLayout) findViewById(R.id.milk);
        milk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,Cow_Profile_Milking.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        milk_record_text = (TextView) findViewById(R.id.milk_record_text);
        milk_record = (LinearLayout) findViewById(R.id.milk_record);
        milk_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,Log_Milk.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        breed_text = (TextView) findViewById(R.id.breed_text);
        breed = (LinearLayout) findViewById(R.id.breed);
        breed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,Activity_Main.class);
                intent.putExtra("ID",id);
                startActivity(intent);
                finish();
            }
        });
        edit_status_text = (TextView) findViewById(R.id.edit_status_text);
        edit_status = (LinearLayout) findViewById(R.id.edit_status);
        edit_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,Edit_Edit_status.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        delete = (LinearLayout) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteclick();
            }
        });
        edit_status = (LinearLayout) findViewById(R.id.edit_status);
        edit_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Cow_Profile2.this,Edit_Edit_status.class);
                intent.putExtra("ID",id);
                startActivity(intent);
            }
        });
        back = (LinearLayout) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        final String[] Photo_cholice = {"ถ่ายรูปใหม่","เลือกรูปจากอัลบั้ม"};
        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());
        edit_image_profile = (LinearLayout) findViewById(R.id.edit_image_profile);
        edit_image_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(Cow_Profile2.this);
                builder.setItems(Photo_cholice, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                ActivityCompat.requestPermissions(Cow_Profile2.this,
                                        new String[]{android.Manifest.permission.CAMERA},
                                        MY_PERMISSIONS_REQUEST_CAMERA);
                                break;
                            case 1:
                                ActivityCompat.requestPermissions(Cow_Profile2.this,
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                        MY_PERMISSIONS_REQUEST_EXTERNAL);
                                break;
                        }
                    }
                });
                builder.create();
                builder.show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                        showAlert(new String[]{Manifest.permission.CAMERA});
                    } else {
                    }
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_EXTERNAL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST2);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE )) {
                        showAlert(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE });
                    } else {
                    }
                }
                return;
            }

        }
    }
    private void showAlert(final String[] string) {
        AlertDialog alertDialog = new AlertDialog.Builder(Cow_Profile2.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(Cow_Profile2.this,
                                string,
                                MY_PERMISSIONS_REQUEST_CAMERA);

                    }
                });
        alertDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            switch (requestCode){
                case CAMERA_REQUEST:
                    bitmap = (Bitmap) data.getExtras().get("data");
                    bitbit = bitmap;
                    image_profile.setImageBitmap(bitmap);
                    break;
                case GALLERY_REQUEST:
                    Uri uri = data.getData();
                    galleryPhoto.setPhotoUri(uri);
                    String photoPath = galleryPhoto.getPath();
                    selectedPhoto = photoPath;
                    bitmap = null;
                    try {
                        bitmap = ImageLoader.init().from(selectedPhoto).requestSize(512, 512).getBitmap();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    bitbit = bitmap;
                    image_profile.setImageBitmap(bitmap);
                    break;
                case GALLERY_REQUEST2:
                    final Uri imageUri = data.getData();
                    final InputStream imageStream;
                    try {
                        imageStream = getContentResolver().openInputStream(imageUri);
                        bitmap = BitmapFactory.decodeStream(imageStream);
                        bitbit = bitmap;
                        image_profile.setImageBitmap(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
            }
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Cow_Profile2.this);
            LayoutInflater inflater = getLayoutInflater();
            dialogBuilder.setCancelable(false);
            final View dialogView = inflater.inflate(R.layout.dialog_image, null);
            dialogBuilder.setView(dialogView);

            ImageView dialog_image = (ImageView) dialogView.findViewById(R.id.dialog_imageview);
            ImageView cancel_image = (ImageView) dialogView.findViewById(R.id.cancel_image);
            dialog_image.setImageBitmap(bitmap);
            final AlertDialog b = dialogBuilder.create();
            TextView cancel = (TextView) dialogView.findViewById(R.id.cancel);
            cancel_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();
                }
            });
            TextView ok = (TextView) dialogView.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                            image_profile.setImageBitmap(bitbit);
                    String encodedImage = ImageBase64.encode(bitbit);
                    HashMap<String, String> postData = new HashMap<String, String>();
                    postData.put("image", encodedImage);
                    postData.put("name", cow.getCowname());
                    postData.put("username", username);
                    Log.d("image",encodedImage);
                    new pushbyPost(Cow_Profile2.this,"https://info.rdi.ku.ac.th/cowapp/manager/uploadImage2.php",postData).execute();
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar.make(parentLayout, "รูปถูกเพิ่มแล้ว", Snackbar.LENGTH_LONG)
                            .setAction("ปิด", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                }
                            })
                            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                            .show();
                    b.dismiss();
                }
            });
            b.show();

        }
    }
    public class Sync_DeleteCow extends AsyncTask<String, Void, String> {
        Context mContext;
        COWHelper mHelper;
        Cow mcow;

        public Sync_DeleteCow(Context context, Cow cow) {
            mContext = context;
            mcow = cow;
        }

        protected String doInBackground(String... params) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(mcow.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(mcow.getCowname(), "UTF-8");
                link = "https://info.rdi.ku.ac.th/cowapp/manager/delete.php" + data;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                return "success";


            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {
            if (result == "success") {
                mHelper = new COWHelper(Cow_Profile2.this);
                mHelper.deleteCow(id);
                //                Intent intent = new Intent(Edit_Edit.this, Menu_Main.class);
                //                startActivity(intent);
                //                finish();
            }
            else{
                Toast.makeText(Cow_Profile2.this,"Deleted Failed.",Toast.LENGTH_SHORT).show();
            }
        }


    }
    public class Sync_DeleteCalving extends AsyncTask<String, Void, String> {
        Context mContext;
        COWHelper mHelper;
        Cow mcow;

        public Sync_DeleteCalving(Context context, Cow cow) {
            mContext = context;
            mcow = cow;
        }

        protected String doInBackground(String... params) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(mcow.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(mcow.getCowname(), "UTF-8");
                link = "https://info.rdi.ku.ac.th/cowapp/manager/deletecalving.php" + data;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                return "success";


            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {
            if (result == "success") {
                new CALVINGHelper(Cow_Profile2.this).deleteCalving(cow.getCowname());
                Intent intent = new Intent(Cow_Profile2.this, Menu_Main.class);
                startActivity(intent);
                finish();
            }
            else{
                Toast.makeText(Cow_Profile2.this,"Deleted Failed.",Toast.LENGTH_SHORT).show();
            }
        }


    }

    public void deleteclick(){
        AlertDialog.Builder builder =
                new AlertDialog.Builder(Cow_Profile2.this);
        builder.setMessage("ต้องการลบโค "+cow.getCowname()+" ?");
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int idd) {
                mHelper.deleteCow(String.valueOf(cow.getId()));
                new Sync_DeleteCow(Cow_Profile2.this, cow).execute();
                new Sync_DeleteCalving(Cow_Profile2.this, cow).execute();
                new CALVINGHelper(Cow_Profile2.this).deleteCalving(cow.getCowname());

            }
        });
        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}