package com.kasidid.mrbboomm.cowlog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.daimajia.swipe.util.Attributes;

import java.net.URLEncoder;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 11/22/2016.
 */

public class Log_Milk  extends ActionBarActivity {
    COWHelper mHelper;
    private MyAdapter myAdapter;
    Cow c;
    TextView alert;
    ListView mListView;
    MilkHelper lHelper;
    ImageView add;
    List<String> ids;

    @Override
    protected void onResume() {
        super.onResume();
        ids = lHelper.getMilkArray(c.getCowname(),0,c.getParity());
        if (ids.size() != 0){
            alert.setVisibility(View.GONE);
        }
        else{
            mListView.setVisibility(View.GONE);
            alert.setText("ไม่มีข้อมูลผลผลิตน้ำนม");
            alert.setVisibility(View.VISIBLE);
            alert.setGravity(Gravity.CENTER);
            alert.setTextColor(Color.parseColor("#C8C8C8"));
        }
        TextView calvingdate = (TextView)findViewById(R.id.calvingdate);
        try {
            calvingdate.setText(c.getDayTH(new CALVINGHelper(this).getReport(c.getCowname(),c.getParity()).get(0)));
        }catch (Exception e){
            calvingdate.setText(c.getCalvdate());
        }
        myAdapter = new MyAdapter(this,ids);
        mListView.setAdapter(myAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_milk);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("ประวัติการให้นม");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent intent = getIntent();
        final String id = intent.getStringExtra("ID");
        mHelper = new COWHelper(this);
        c = mHelper.getCowByID(id);
        alert = (TextView) findViewById(R.id.warn);
        mListView= (ListView)findViewById(R.id.listmilk);


        add = (ImageView) findViewById(R.id.add);

        if (c.getType().equals("1")) { //cow
            if (!(new CALVINGHelper(this).getReport(c.getCowname(), c.getParity()).get(2).equals("-"))) { //พักรีด
                add.setEnabled(false);
                add.setVisibility(View.GONE);
            }
        }
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Log_Milk.this,Cow_Profile_Milk_Add.class);
                intent.putExtra("ID",id);
                startActivity(intent);
                finish();
            }
        });
        lHelper = new MilkHelper(this);




    }
    class MyAdapter extends BaseAdapter {
        List<String> menu;


        private Context context;
        public MyAdapter(Context context,List<String> menu){
            this.context = context;
            this.menu = menu;
        }
        @Override
        public int getCount() {
            return this.menu.size();
        }

        @Override
        public Object getItem(int i) {
            return this.menu.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            View row = null;
            if (view == null){
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.milk_item,viewGroup,false);
            }
            else{
                row = view;
            }
            final Milk milk = lHelper.getMilkByID(ids.get(position));
            TextView text = (TextView) row.findViewById(R.id.milk);
            ImageView delete = (ImageView)row.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(Log_Milk.this)
                            .setMessage("คุณต้องการลบข้อมูลของวันที่ "+c.getDayTH(new MilkHelper(Log_Milk.this).getMilkByID(ids.get(position)).getMilkdate())+"?")
                            .setNegativeButton("ยกเลิก", null)
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    lHelper.deleteMilk(ids.get(position));
                                    new Sync_DeleteMilk(Log_Milk.this, milk).execute();
                                    startActivity(getIntent());
                                    finish();
                                }
                            }).create().show();
                }
            });
            text.setText("วันที่ "+c.getDayTH(milk.getMilkdate())+"\n"+
            "ปริมาณน้ำนม "+milk.getVolume()+" กิโลกรัม");
            return row;
        }
    }
    public class Sync_DeleteMilk extends AsyncTask<String, Void, String> {
        Context mContext;
        COWHelper mHelper;
        Milk milk;

        public Sync_DeleteMilk(Context context, Milk milk) {
            this.mContext = context;
            this.milk = milk;
        }

        protected String doInBackground(String... params) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(this.milk.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(this.milk.getCowname(), "UTF-8");
                data += "&milkdate=" + URLEncoder.encode(this.milk.getMilkdate(), "UTF-8");
                link = "https://info.rdi.ku.ac.th/cowapp/manager/deletemilk.php" + data;
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                return "success";


            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }
        }

        protected void onPostExecute(String result) {

        }


    }

}