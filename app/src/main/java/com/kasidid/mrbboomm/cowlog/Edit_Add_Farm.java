package com.kasidid.mrbboomm.cowlog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Kasidid on 10/25/2017.
 */

public class Edit_Add_Farm extends AppCompatActivity{
    EditText et_farmId,et_farmName,et_farmOwner,et_farmAddress;
    Button confirm;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_add_farm);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        final String username =  prefs.getString("username","not found");
        final FARMHelper farmHelper =  new FARMHelper(this);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_edit);
        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("ข้อมูลฟาร์ม");
        Farm tmp = farmHelper.getFarm(username);
        et_farmId = (EditText)findViewById(R.id.farmid);
        et_farmName = (EditText)findViewById(R.id.farmname);
        et_farmOwner = (EditText)findViewById(R.id.farmowner);
        et_farmAddress = (EditText)findViewById(R.id.farmaddress);
        et_farmId.setText(tmp.getFarmid());
        et_farmName.setText(tmp.getFarmname());
        et_farmOwner.setText(tmp.getOwner());
        et_farmAddress.setText(tmp.getAddress());
        ImageView check = (ImageView)findViewById(R.id.check);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_farmId.getText().toString().equals("") ||et_farmName.getText().toString().equals("") || et_farmOwner.getText().toString().equals("") || et_farmAddress.getText().toString().equals("")){
                    new AlertDialog.Builder(Edit_Add_Farm.this)
                            .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                Farm f = new Farm();
                f.setUsername(username);
                f.setFarmid(et_farmId.getText().toString());
                f.setFarmname(et_farmName.getText().toString());
                f.setOwner(et_farmOwner.getText().toString());
                f.setAddress(et_farmAddress.getText().toString());
                farmHelper.addFarm(f);
                new Sync_Farm(f).execute();
                finish();

            }
        });

    }
}
