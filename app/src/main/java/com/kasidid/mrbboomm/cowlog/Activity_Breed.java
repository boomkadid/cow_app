package com.kasidid.mrbboomm.cowlog;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Kasidid on 3/29/2017.
 */

public class Activity_Breed extends android.support.v4.app.Fragment{
    String id;
    TextView times,couple,breeddate;
    COWHelper cowHelper;
    Cow cow;
    @Override
    public void onResume() {
        super.onResume();
        try{
            CBreed cBreed = new CBREEDHelper(getActivity()).getCBreed(cow.getCowname(), String.valueOf(Integer.parseInt(cow.getParity())+1));
            times.setText("ผสมล่าสุดครั้งที่: "+cBreed.getTimes());
            couple.setText("พ่อพันธุ์หมายเลข: "+cBreed.getPapa());
            breeddate.setText("วันที่ผสม: "+cBreed.getBreeddate());
//            List<String> last_breed = new BREEDHelper(getActivity()).getLastBreed(cow.getCowname(), String.valueOf(Integer.parseInt(cow.getParity())+1));
//            times.setText("ผสมล่าสุดครั้งที่: "+last_breed.get(0));
//            couple.setText("พ่อพันธุ์หมายเลข: "+last_breed.get(1));
//            breeddate.setText("วันที่ผสม: "+cow.getDayTH(last_breed.get(2)));
        }
        catch (Exception e){
            times.setText("");
            couple.setText("ยังไม่ได้รับการผสม");
            breeddate.setText("");
        }
    }

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_breed, container, false);
        Bundle bundle = this.getArguments();
        id = bundle.getString("ID");
        times = (TextView)view.findViewById(R.id.times);
        couple = (TextView)view.findViewById(R.id.couple);
        breeddate = (TextView)view.findViewById(R.id.breeddate);
        cowHelper = new COWHelper(getActivity());
        cow = cowHelper.getCowByID(id);

        return view;
    }
}
