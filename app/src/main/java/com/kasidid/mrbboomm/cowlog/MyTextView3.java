package com.kasidid.mrbboomm.cowlog;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Kasidid on 3/1/2017.
 */

public class MyTextView3 extends TextView {

    public MyTextView3(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextView3(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView3(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun.ttf");
        setTypeface(tf);
    }


}