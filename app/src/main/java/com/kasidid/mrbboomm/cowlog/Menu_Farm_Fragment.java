package com.kasidid.mrbboomm.cowlog;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flyco.labelview.LabelView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.R.attr.bitmap;

/**
 * Created by Kasidid on 10/14/2016.
 */

public class Menu_Farm_Fragment extends Fragment{
    COWHelper mHelper;
    FrameLayout exchange;
    LinearLayout reportstatus,reportcows,editFarm;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().findViewById(R.id.search_list).setVisibility(View.GONE);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.userprofile_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("สถานะฟาร์ม");
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        View view = inflater.inflate(R.layout.menu_farm_f, container, false);

        ImageView farm_status = (ImageView) view.findViewById(R.id.farm_status);
        ImageView cow_status = (ImageView )view.findViewById(R.id.cow_status);
        ImageView setting = (ImageView )view.findViewById(R.id.setting);
        farm_status.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(getActivity().getResources(),R.drawable.farm_status,50,50));
        cow_status.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(getActivity().getResources(),R.drawable.cow_status,50,50));
        setting.setImageBitmap(ImageNicer.decodeSampledBitmapFromResource(getActivity().getResources(),R.drawable.setting,50,50));

        reportstatus = (LinearLayout)view.findViewById(R.id.reportfarm);
        reportstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),report_Status.class);
                startActivity(intent);
            }
        });
        reportcows = (LinearLayout) view.findViewById(R.id.reportcows);
        reportcows.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),report_Farm.class);
                startActivity(intent);
            }
        });
        editFarm = (LinearLayout)view.findViewById(R.id.editfarm);
        editFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Edit_Add_Farm.class);
                startActivity(intent);
            }
        });
        exchange = (FrameLayout) view.findViewById(R.id.exchange);
        exchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                new Sync_GetAds(getActivity()).execute();
            }
        });
        exchange.setVisibility(View.GONE);
        return view;
    }
    private ProgressDialog mProgressDialog;
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
    public class Sync_GetAds extends AsyncTask<String,Void,String> {
        Context mContext;
        String  data;
        ArrayList<String> usernames = new ArrayList<>();
        ArrayList<String> cowname = new ArrayList<>();
        ArrayList<String> usernames2 = new ArrayList<>();
        public Sync_GetAds(Context context){
            this.mContext = context;
        }
        @Override
        protected String doInBackground(String... params) {
            SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(mContext);
            data= "?username=" + prefs.getString("username","not found");

            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/transfer/getfromuser.php" + data).build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("result(fromuser)","https://info.rdi.ku.ac.th/cowapp/manager/transfer/getfromuser.php" + data);
                Log.d("result(fromuser)",result);

                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            usernames.add(x.getString("username"));
                            cowname.add(x.getString("cowname"));
                            usernames2.add(x.getString("username2"));
                            Log.d("get(fromuser)", "done");

                        } catch (Exception e) {

                            Log.d("get(fromuser)","fail");
                            continue;
                        }

                    }
                }
                else{
                    return "none";
                }
                return "success";
            }catch (Exception e){
                Log.d("get(fromuser)","fail");
                return "fail";
            }

        }

        @Override
        protected void onPostExecute(String s) {
            hideProgressDialog();
            if (s.equals("success")){

                Intent intent = new Intent(getActivity(),Transfer_fromUser.class);
                intent.putExtra("usernames", usernames);
                intent.putExtra("cownames",  cowname);
                intent.putExtra("usernames2",  usernames2);
                startActivity(intent);

            }
            else{
                Toast.makeText(mContext, "ไม่พบรายการ", Toast.LENGTH_LONG).show();

            }
        }
    }

}
