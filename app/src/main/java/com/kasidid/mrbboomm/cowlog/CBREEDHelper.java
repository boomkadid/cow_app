package com.kasidid.mrbboomm.cowlog;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kasidid on 2/27/2017.
 */

public class CBREEDHelper extends SQLiteOpenHelper {
    public CBREEDHelper(Context context) {
        super(context, "cbreed.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CBREED_TABLE = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT, UNIQUE(%s,%s) ON CONFLICT REPLACE)",
                CBreed.TABLE,
                CBreed.Column.ID,
                CBreed.Column.USERNAME,
                CBreed.Column.COWNAME,
                CBreed.Column.PARITY,
                CBreed.Column.PAPA,
                CBreed.Column.BREEDDATE,
                CBreed.Column.TIMES,
                CBreed.Column.COWNAME,CBreed.Column.PARITY);

        db.execSQL(CREATE_CBREED_TABLE);
    }
    private ContentValues getValues(CBreed cbreed){
        ContentValues values = new ContentValues();
        values.put(CBreed.Column.USERNAME, cbreed.getUsername());
        values.put(CBreed.Column.COWNAME, cbreed.getCowname());
        values.put(CBreed.Column.PARITY, cbreed.getParity());
        values.put(CBreed.Column.PAPA, cbreed.getPapa());
        values.put(CBreed.Column.BREEDDATE,cbreed.getBreeddate());
        values.put(CBreed.Column.TIMES,cbreed.getTimes());
        return values;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_BREED_TABLE = "DROP TABLE IF EXISTS cbreed";
        db.execSQL(DROP_BREED_TABLE);
        onCreate(db);
    }
    public void addCBreed(CBreed cbreed){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getValues(cbreed);
        db.insert(CBreed.TABLE, null, values);
        db.close();
    }
    public CBreed plusCBreed(CBreed cBreed){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        Cursor cursor = sqLiteDatabase.query
                (CBreed.TABLE, null, CBreed.Column.COWNAME+"=? and "+CBreed.Column.PARITY+"=?",
                        new String[]{cBreed.getCowname(),cBreed.getParity()}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        while(!cursor.isAfterLast()) {
            cBreed.setId(cursor.getInt(0));
            cBreed.setUsername(cursor.getString(1));
            cBreed.setCowname(cursor.getString(2));
            cBreed.setParity(cursor.getString(3));
//                cBreed.setPapa(cursor.getString(4));
//                cBreed.setBreeddate(cursor.getString(5));
            cBreed.setTimes(String.valueOf(Integer.parseInt(cursor.getString(6))+1));
            cursor.moveToNext();
        }
        cursor.close();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getValues(cBreed);
        db.insert(CBreed.TABLE, null, values);
        db.close();
        return  cBreed;
    }
    public void deleteCBreed(String cowname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CBreed.TABLE, CBreed.Column.COWNAME + "='" + cowname+"'", null);
        db.close();
    }
    public void clearCBreed(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CBreed.TABLE, null, null);
        db.close();
    }
    public CBreed getCBreed(String cowname,String parity){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        CBreed cBreed = new CBreed();
        cBreed.setId(999);
        cBreed.setUsername("-");
        cBreed.setCowname(cowname);
        cBreed.setParity(parity);
        cBreed.setPapa("-");
        cBreed.setBreeddate("-");
        cBreed.setTimes("0");
        Cursor cursor = sqLiteDatabase.query
                (CBreed.TABLE, null, CBreed.Column.COWNAME+"=? and "+CBreed.Column.PARITY+"=?",
                        new String[]{cowname,parity}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }


        while(!cursor.isAfterLast()) {
            cBreed.setId(cursor.getInt(0));
            cBreed.setUsername(cursor.getString(1));
            cBreed.setCowname(cursor.getString(2));
            cBreed.setParity(cursor.getString(3));
            cBreed.setPapa(cursor.getString(4));
            cBreed.setBreeddate(cursor.getString(5));
            cBreed.setTimes(cursor.getString(6));
            cursor.moveToNext();
        }
        cursor.close();

        return cBreed;
    }
    public CBreed getIntervalCBreed(String cowname,String parity,Calendar st, Calendar ed){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query
                (CBreed.TABLE, null, CBreed.Column.COWNAME+"=? and "+CBreed.Column.PARITY+"=?",
                        new String[]{cowname,parity}, null, null, null, null);
        CBreed cBreed = new CBreed();
        cBreed.setId(999);
        cBreed.setUsername("-");
        cBreed.setCowname(cowname);
        cBreed.setParity(parity);
        cBreed.setPapa("-");
        cBreed.setBreeddate("-");
        cBreed.setTimes("0");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar bd = Calendar.getInstance();
        int count = 0;
        while(!cursor.isAfterLast()) {
            cBreed.setId(cursor.getInt(0));
            cBreed.setUsername(cursor.getString(1));
            cBreed.setCowname(cursor.getString(2));
            cBreed.setParity(cursor.getString(3));
            cBreed.setPapa(cursor.getString(4));
            cBreed.setBreeddate(cursor.getString(5));
            cBreed.setTimes(cursor.getString(6));
            try {
                bd.setTime(df.parse(cursor.getString(5)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (bd.after(st) && bd.before(ed)) count += 1;
            cursor.moveToNext();
        }
        cBreed.setTimes(String.valueOf(count));
        cursor.close();
        return cBreed;
    }





}
