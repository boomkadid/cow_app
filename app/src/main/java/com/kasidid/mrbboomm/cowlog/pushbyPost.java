package com.kasidid.mrbboomm.cowlog;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.EachExceptionsHandler;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.HashMap;

/**
 * Created by Kasidid on 12/8/2017.
 */

public class pushbyPost {
    Context mcontext;
    HashMap<String, String> postData;
    String Url;
    public pushbyPost(Context mcontext, String url, HashMap<String,String> postdata) {
        this.mcontext = mcontext;
        this.postData = postdata;
        this.Url = url;
    }
    public void execute(){
        PostResponseAsyncTask task = new PostResponseAsyncTask(mcontext, postData, new AsyncResponse() {
            @Override
            public void processFinish(String s) {
                Log.d("pushbypost",s);
            }
        });
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,Url);
        task.setEachExceptionsHandler(new EachExceptionsHandler() {
            @Override
            public void handleIOException(IOException e) {
                Log.d("pushbypost error",Url+" "+e);
                Toast.makeText(mcontext, "Cannot Connect to Server.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleMalformedURLException(MalformedURLException e) {
                Toast.makeText(mcontext, "URL Error.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleProtocolException(ProtocolException e) {
                Toast.makeText(mcontext, "Protocol Error.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void handleUnsupportedEncodingException(UnsupportedEncodingException e) {
                Toast.makeText(mcontext, "Encoding Error.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


}
