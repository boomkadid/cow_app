package com.kasidid.mrbboomm.cowlog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 11/16/2017.
 */

public class Transfer_toUser extends ActionBarActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        String username =  prefs.getString("username","not found");

        setContentView(R.layout.transfer_touser);
        Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ย้ายโคไปยัง");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        RecyclerView mRecycleView = (RecyclerView) findViewById(R.id.to_user);
        mRecycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecycleView.setLayoutManager(mLayoutManager);

        ArrayList<String> usernames2 = (ArrayList<String>) getIntent().getSerializableExtra("usernames");
        ArrayList<String> owners = (ArrayList<String>) getIntent().getSerializableExtra("owners");
        String cowname = getIntent().getStringExtra("cowname");
        RecycleAdapter mAdapter = new RecycleAdapter(this,usernames2,owners,username,cowname);
        mAdapter.notifyDataSetChanged();
        mRecycleView.setAdapter(mAdapter);
    }
    private class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ViewHolder>{
        Context mContext;
        String username,cowname;
        ArrayList<String> username2;
        ArrayList<String> owner;
        public RecycleAdapter(Context context, ArrayList<String> usernames2_, ArrayList<String> owners,String username_, String cowname_){
            mContext = context;
            username2 = usernames2_;
            owner = owners;
            username = username_;
            cowname = cowname_;
        }
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            TextView to_user, farm, no_use;
            public ViewHolder(View view) {
                super(view);
                to_user = (TextView)view.findViewById(R.id.text_cowname);
                farm = (TextView)view.findViewById(R.id.avg);
                no_use = (TextView)view.findViewById(R.id.total);
                view.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(mContext);

                builder.setMessage("ต้องการย้ายโคไปให้"+username2.get(getAdapterPosition()) +"?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Transfer transfer = new Transfer();
                        transfer.setUsername(username);
                        transfer.setCowname(cowname);
                        transfer.setUsername2(username2.get(getAdapterPosition()));
                        new Sync_Transfer(transfer).execute();
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();



            }
        }
        @Override
        public RecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.menu_cowdb_item, parent, false);
            RecycleAdapter.ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecycleAdapter.ViewHolder holder, int position) {
            holder.to_user.setText(owner.get(position));
            holder.no_use.setText("");
            holder.farm.setText("");
        }

        @Override
        public int getItemCount() {
            return owner.size();
        }
    }
    public class Sync_Transfer extends AsyncTask<String, Void, String> {
        private Transfer transfer;
        public Sync_Transfer(Transfer transfer) {
            this.transfer = transfer;
        }
        @Override
        protected String doInBackground(String... strings) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(transfer.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(transfer.getCowname(), "UTF-8");
                data += "&username2=" + URLEncoder.encode(transfer.getUsername2(), "UTF-8");
                link = "https://data" +
                        ".rdi.ku.ac.th/cowapp/manager/transfer/updatetransfer.php" + data;

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                Log.d("link_transfer", link);
                return response.body().string();

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("link", "FAIL sync");
                return new String("Exception: " + e.getMessage());
            }
        }



        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("query_result");
                    if (query_result.equals("SUCCESS")) {
                        Log.d("result_sync", "SUCESS");
                        new Transfer_out(Transfer_toUser.this,transfer).delete();
                        Toast.makeText(Transfer_toUser.this,"Pending",Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (query_result.equals("FAILURE")) {
                        Toast.makeText(Transfer_toUser.this,"Failure",Toast.LENGTH_SHORT).show();
                        Log.d("result_sync", "FAILURE");
                    } else {

                        Log.d("result_sync", "Couldn't connect");
                    }
                } catch (JSONException e) {
                    Log.d("result_sync", result);
                }
            } else {
                Log.d("result_sync", "couldn't get JSON data");
            }
        }

    }
}
