package com.kasidid.mrbboomm.cowlog;

        import android.app.AlarmManager;
        import android.app.PendingIntent;
        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.content.pm.PackageInfo;
        import android.content.pm.PackageManager;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.net.ConnectivityManager;
        import android.net.NetworkInfo;
        import android.net.Uri;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.preference.PreferenceManager;
        import android.support.v7.app.AppCompatActivity;
        import android.util.Log;
        import android.view.View;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.facebook.login.Login;
        import com.google.android.gms.auth.api.Auth;
        import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
        import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
        import com.google.android.gms.auth.api.signin.GoogleSignInResult;
        import com.google.android.gms.common.ConnectionResult;
        import com.google.android.gms.common.SignInButton;
        import com.google.android.gms.common.api.GoogleApiClient;
        import com.google.android.gms.common.api.OptionalPendingResult;
        import com.google.android.gms.common.api.ResultCallback;
        import com.google.android.gms.common.api.Status;

        import org.json.JSONArray;
        import org.json.JSONObject;

        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.security.SecureRandom;
        import java.security.cert.CertificateException;
        import java.util.Calendar;
        import java.util.HashMap;
        import java.util.List;


        import okhttp3.OkHttpClient;
        import okhttp3.Request;
        import okhttp3.Response;

        import static com.google.android.gms.common.api.CommonStatusCodes.getStatusCodeString;

/**
 * Created by mrbboomm on 8/30/2016.
 */
public class Login_Google extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient mGoogleApiClient;
    private TextView mStatusTextView,versiontext;
    private ProgressDialog mProgressDialog;
    ImageView icon;
    private void doService(){
        Intent myIntent = new Intent(this, syncservice.class);

        PendingIntent Pintent = PendingIntent.getService(this, 0, myIntent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);

        Calendar firingCal= Calendar.getInstance();
        firingCal.set(Calendar.HOUR_OF_DAY, 0); // At the hour you wanna fire
        firingCal.set(Calendar.MINUTE, 0); // Particular minute
        firingCal.set(Calendar.SECOND, 0); // particular second


        alarm.setRepeating(AlarmManager.RTC_WAKEUP, firingCal.getTimeInMillis(), 24*60*60*1000, Pintent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        doService();
        setContentView(R.layout.login_google);
        // Views
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        versiontext = (TextView) findViewById(R.id.version);
        versiontext.setText("Version "+version);
        mStatusTextView = (TextView) findViewById(R.id.status);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Log.d("FD",prefs.getString("email",""));
        FARMHelper farmHelper = new FARMHelper(this);
        if(prefs.getInt("userid", 0)!=0 && prefs.getString("email","")!=null ){
//            new Sync_Profile(acct.getId(),acct.getEmail(),acct.getGivenName()+" "+acct.getFamilyName(),acct.getDisplayName()).execute();
            Intent intent = new Intent(Login_Google.this,Menu_Main.class);
            startActivity(intent);
            finish();

        }
        // Button listeners
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.sign_out_button).setOnClickListener(this);
        findViewById(R.id.start_button).setOnClickListener(this);
        icon = (ImageView)findViewById(R.id.google_icon);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // [START build_client]
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // [END build_client]

        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
//            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
//                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    // [START onActivityResult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            int statusCode = result.getStatus().getStatusCode();
            Log.d("Status",getStatusCodeString(statusCode));
            handleSignInResult(result);
        }
    }
    // [END onActivityResult]

    // [START handleSignInResult]
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        int connected = 0;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = 1;
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();
                mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                String account = acct.getId().toString();
//                account = "pitiphong";
                if (0 == 0){
                    showProgressDialog();
                    new Sync_GetCow(Login_Google.this, account).execute();
                }
                if(prefs.getInt("userid", 0)==0){
                    if(true) {
                        COWHelper c = new COWHelper(this);
                        List<String> allcows =  c.getCowArray(account,0,0,false);
                        new Sync_Profile(acct.getId(),acct.getEmail(),acct.getGivenName()+" "+acct.getFamilyName(),acct.getDisplayName()).execute();
                        allcows.addAll(c.getCowArray(account,0,1,false));
                        Log.d("numberofcow", String.valueOf(allcows.size()));
                        if (allcows.size() == 0){
                            showProgressDialog();
                            new Sync_GetCow(Login_Google.this, account).execute();
                        }
                        prefs.edit().putString("username", account).commit();
                        prefs.edit().putString("email", acct.getEmail().toString()).commit();
//                        prefs.edit().putString("name", acct.getGivenName().toString()+" "+acct.getFamilyName().toString()).commit();
                        prefs.edit().putInt("userid", 1).commit();
                    }

                }

                Uri personPhoto = acct.getPhotoUrl();
                try {
                    new DownloadImageTask()
                            .execute(personPhoto.toString());
                }catch (Exception IO){

                }
                updateUI(true);
            } else {
                updateUI(false);
            }
        }
        else {
            connected = 0;
            mStatusTextView.setText("Please connect to the internet");
        }
        Log.d("Connection", String.valueOf(connected));

    }
    // [END handleSignInResult]

    // [START signIn]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signIn]

    // [START signOut]
    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Login_Google.this);
                        prefs.edit().clear().commit();
                        icon.setImageResource(R.drawable.cow_acc);
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END signOut]

    // [START revokeAccess]
    private void getStart() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        Intent intent = new Intent(Login_Google.this,Menu_Main.class);
                        startActivity(intent);
                        finish();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            mStatusTextView.setText(R.string.signed_out);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.sign_out_button:
                signOut();
                break;
            case R.id.start_button:
                getStart();
                break;
        }
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            icon.setImageBitmap(result);
//            hideProgressDialog();
        }
    }

    public class Sync_GetCow extends AsyncTask<String, Void, String> {
        Context mContext;
        COWHelper mHelper;
        MilkHelper milkHelper;
        CALVINGHelper calvingHelper;
        FARMHelper farmHelper;
        CBREEDHelper cbreedHelper;
        String Username;
        public Sync_GetCow(Context context,String username) {
            mContext = context;
            Username = username;
        }

        protected String doInBackground(String... params) {
            mHelper = new COWHelper(mContext);
            milkHelper = new MilkHelper(mContext);
            farmHelper = new FARMHelper(mContext);
            calvingHelper = new CALVINGHelper(mContext);
            cbreedHelper = new CBREEDHelper(mContext);
            mHelper.clearCow(Username);
            milkHelper.clearMilk(Username);
            farmHelper.clearFarm(Username);
            calvingHelper.clearCalving(Username);
            new CBREEDHelper(Login_Google.this).clearCBreed(Username);
            String data;
            String result;
            data = "?username=" + Username;

            /////////////////////////////////////////////////////
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("http://info.rdi.ku.ac.th/cowapp/manager/getCow.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("result(cow)",result);
                Cow c = new Cow();
                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            c.setUsername(x.getString("username"));
                            c.setCowname(x.getString("cowname"));
                            c.setFraction(x.getString("fraction"));
                            c.setBirth(x.getString("birth"));
                            c.setDaystart(x.getString("daystart"));
                            c.setStatus(x.getString("status"));
                            c.setDayend(x.getString("dayend"));
                            c.setCalvdate(x.getString("daymilk"));
                            c.setState(x.getString("state"));
                            c.setPapa(x.getString("papa"));
                            c.setPapaf(x.getString("papaf"));
                            c.setMama(x.getString("mama"));
                            c.setMamaf(x.getString("mamaf"));
                            c.setType(x.getString("type"));
                            c.setNickname(x.getString("nickname"));
                            c.setParity(x.getString("parity"));
                            mHelper.addCow(c);
                            Log.d("get(cow)", "done");

                        } catch (Exception e) {

                            Log.d("get(cow)",e.toString());
                            continue;
                        }

                    }
                }
            }catch (Exception e){
                Log.d("get(cow)",e.toString());
            }
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/getfarm.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("result(farm)",result);

                Farm f = new Farm();
                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            f.setUsername(x.getString("username"));
                            f.setFarmid(x.getString("farmid"));
                            f.setFarmname(x.getString("farmname"));
                            f.setOwner(x.getString("owner"));
                            f.setAddress(x.getString("address"));
                            farmHelper.addFarm(f);
                            Log.d("get(farm)", "done");

                        } catch (Exception e) {

                            Log.d("get(farm)", e.toString());
                            continue;
                        }

                    }
                }
            }catch (Exception e){

                Log.d("get(farm)",e.toString());
            }
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/getcbreed.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("link(cbreed)","http://info.rdi.ku.ac.th/cowapp/manager/getcbreed.php" + data);
                Log.d("result(cbreed)",result);

                CBreed cBreed = new CBreed();
                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            cBreed.setUsername(x.getString("username"));
                            cBreed.setCowname(x.getString("cowname"));
                            cBreed.setParity(x.getString("parity"));
                            cBreed.setPapa(x.getString("papa"));
                            cBreed.setBreeddate(x.getString("breeddate"));
                            cBreed.setTimes(x.getString("times"));
                            cbreedHelper.addCBreed(cBreed);
                            Log.d("get(cbreed)", "done"+x.getString("cowname")+','+x.getString("parity")+','+x.getString("papa")+','+x.getString("breeddate")+','+x.getString("times"));

                        } catch (Exception e) {

                            Log.d("get(cbreed)",e.toString());
                            continue;
                        }

                    }
                }
            }catch (Exception e){

                Log.d("get(cbreed)",e.toString());
            }
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/getcalving.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("result(calving)",result);

                Calving calving = new Calving();
                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            calving.setUsername(x.getString("username"));
                            calving.setCowname(x.getString("cowname"));
                            calving.setParity(x.getString("parity"));
                            calving.setCalvingdate(x.getString("calvingdate"));
                            calving.setKid(x.getString("kid"));
                            calving.setRestday(x.getString("restday"));
                            new CALVINGHelper(Login_Google.this).addCalving(calving);
                            Log.d("get(calving)", "done");

                        } catch (Exception e) {

                            Log.d("get(calving)","fail");
                            continue;
                        }

                    }
                }
            }catch (Exception e){

                Log.d("get(calving)","fail");
            }

            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/getMilk.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("result(milk)",result);
                Milk m = new Milk();
                if (result != null) {

                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            m.setUsername(x.getString("username"));
                            m.setCowname(x.getString("cowname"));
                            m.setMilkdate(x.getString("milkdate"));
                            m.setVolume(x.getString("volume"));
                            m.setFat(x.getString("fat"));
                            m.setProtien(x.getString("protein"));
                            m.setLactos(x.getString("lactos"));
                            m.setSolid(x.getString("solid"));
                            m.setSolidmilk(x.getString("solidmilk"));
                            m.setCalvdate(x.getString("calvdate"));
                            m.setParity(x.getString("parity"));
                            milkHelper.addMilk(m);
                            Log.d("get(milk)", "done");

                        } catch (Exception e) {

                            Log.d("get(milk)","fail");
                            continue;
                        }

                    }
                }
                return "success";
            }catch (Exception e){
                Log.d("get(milk)","fail");
            }

            return null;
        }

        protected void onPostExecute(String result) {
            hideProgressDialog();
        }


    }

}
