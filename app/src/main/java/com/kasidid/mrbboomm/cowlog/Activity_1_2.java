package com.kasidid.mrbboomm.cowlog;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dd.CircularProgressButton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mrbboomm on 8/23/2016.
 */
public class Activity_1_2 extends Fragment {
    Calendar mCalendar;
    COWHelper mHelper;
    Button ready;
    CircularProgressButton mCircularButtonSimple;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final View view = inflater.inflate(R.layout.activity_1_2, container, false);
        Bundle bundle = this.getArguments();
        final String  id = bundle.getString("ID");
        TextView suggest = (TextView)view.findViewById(R.id.textheader);
        COWHelper mHelper = new COWHelper(getActivity());
        Cow cow = mHelper.getCowByID(id);
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH");
        Calendar c1 = Calendar.getInstance();
        try {
            c1.setTime(df2.parse(cow.getState()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int t = c1.get(Calendar.HOUR_OF_DAY);
        suggest.setText("ควรผสมในวันที่ "+ cow.getDayTH(cow.getState())+ "\nเวลา "+t+".00-"+(t+2)+".00 น. ");
        ready = (Button)view.findViewById(R.id.ready1_2);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun.ttf");
        ready.setTypeface(tf);
        suggest.setTypeface(tf);
        ready.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_1_3 fragment = new Activity_1_3();
                Bundle args = new Bundle();
                args.putString("ID",id);
                fragment.setArguments(args);
                android.support.v4.app.FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.root_frame,fragment);
                trans.commit();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
