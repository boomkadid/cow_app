package com.kasidid.mrbboomm.cowlog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 11/16/2017.
 */

public class Transfer_fromUser extends ActionBarActivity {
    String me;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        me =  prefs.getString("username","not found");

        setContentView(R.layout.transfer_fromuser);
        Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("รายชื่อโค");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        RecyclerView mRecycleView = (RecyclerView) findViewById(R.id.from_user);
        mRecycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecycleView.setLayoutManager(mLayoutManager);

        ArrayList<String> usernames = (ArrayList<String>) getIntent().getSerializableExtra("usernames");
        ArrayList<String> cownames = (ArrayList<String>) getIntent().getSerializableExtra("cownames");
        ArrayList<String> usernames2 = (ArrayList<String>) getIntent().getSerializableExtra("usernames2");
        RecycleAdapter mAdapter = new RecycleAdapter(this,usernames,cownames,usernames2);
        mAdapter.notifyDataSetChanged();
        mRecycleView.setAdapter(mAdapter);
    }
    private class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ViewHolder>{
        Context mContext;
        ArrayList<String> username;
        ArrayList<String> username2;
        ArrayList<String> cowname;
        public RecycleAdapter(Context context, ArrayList<String> usernames,  ArrayList<String> cownames,  ArrayList<String> usernames2){
            this.mContext = context;
            this.username = usernames;
            this.username2 = usernames2;
            this.cowname = cownames;
        }
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            TextView to_user, farm, no_use;
            public ViewHolder(View view) {
                super(view);
                to_user = (TextView)view.findViewById(R.id.text_cowname);
                farm = (TextView)view.findViewById(R.id.avg);
                no_use = (TextView)view.findViewById(R.id.total);
                view.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(mContext);
                if (username.get(getAdapterPosition()).equals(me)){
                    builder.setMessage("ต้องการยกเลิกรายการนี้?");
                    builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Transfer transfer = new Transfer();
                            transfer.setUsername(username.get(getAdapterPosition()));
                            transfer.setCowname(cowname.get(getAdapterPosition()));
                            transfer.setUsername2(username.get(getAdapterPosition()));
                            new Sync_Transfer_in(mContext,transfer).execute();
                        }
                    });
                    builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }else{
                    builder.setMessage("ต้องการเพิ่มข้อมูลของโค "+cowname.get(getAdapterPosition())+" ลงในอุปกรณ์ของคุณ?");
                    builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Transfer transfer = new Transfer();
                            transfer.setUsername(username.get(getAdapterPosition()));
                            transfer.setCowname(cowname.get(getAdapterPosition()));
                            transfer.setUsername2(username2.get(getAdapterPosition()));
                            new Sync_Transfer_in(mContext,transfer).execute();

                        }
                    });
                    builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }

            }
        }
        @Override
        public RecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.menu_cowdb_item, parent, false);
            RecycleAdapter.ViewHolder viewHolder = new RecycleAdapter.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecycleAdapter.ViewHolder holder, int position) {
            holder.to_user.setText(cowname.get(position));
            if (username.get(position).equals(me)){
                holder.no_use.setText("รอการยืนยัน.. (กดอีกครั้งเพื่อยกเลิก)");
                holder.no_use.setTextColor(Color.RED);
            }
            else{
                holder.no_use.setText("จาก "+ username.get(position));
            }
            holder.farm.setText("");
        }

        @Override
        public int getItemCount() {
            return username.size();
        }
    }
    private ProgressDialog mProgressDialog;
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("กำลังดึงข้อมูล...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
    public class Sync_Transfer_in extends AsyncTask<String, Void, String> {
        private Transfer transfer;
        private Context mContext;
        public Sync_Transfer_in(Context mContext,Transfer transfer) {
            this.transfer = transfer;
            this.mContext = mContext;

        }
        @Override
        protected String doInBackground(String... strings) {
            String link;
            String data;
            try {
                data = "?username=" + URLEncoder.encode(transfer.getUsername(), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(transfer.getCowname(), "UTF-8");
//                data += "&username2=" + URLEncoder.encode(transfer.getUsername2(), "UTF-8");
                link = "https://info.rdi.ku.ac.th/cowapp/manager/transfer/deletetransfer.php" + data;

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                Log.d("link_transfer", link);
                return response.body().string();

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("link", "FAIL sync");
                return new String("Exception: " + e.getMessage());
            }
        }



        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("query_result");
                    if (query_result.equals("SUCCESS")) {
                        Log.d("result_sync", "SUCESS");
                        Log.d("transfer_in","remove transfer db");
                        showProgressDialog();
                        new Transfer_in(mContext,transfer).execute();

                    } else if (query_result.equals("FAILURE")) {
                        Toast.makeText(mContext,"Failure",Toast.LENGTH_SHORT).show();
                        Log.d("result_sync", "FAILURE");
                    } else {

                        Log.d("result_sync", "Couldn't connect");
                    }
                } catch (JSONException e) {
                    Log.d("result_sync", result);
                }
            } else {
                Log.d("result_sync", "couldn't get JSON data");
            }
        }

    }
    public class Transfer_in extends AsyncTask<String,Void,String>{
        Transfer transfer;
        Context mContext;
        public Transfer_in(Context mContext_, Transfer transfer_){
            this.transfer = transfer_;
            this.mContext = mContext_;
        }


        @Override
        protected String doInBackground(String... params) {
            String data,result;
            COWHelper mHelper = new COWHelper(mContext);
//            BREEDHelper breedHelper = new BREEDHelper(mContext);
            CALVINGHelper calvingHelper = new CALVINGHelper(mContext);
            MilkHelper milkHelper = new MilkHelper(mContext);
            data = "?username=" + transfer.getUsername();
            data += "&cowname=" + transfer.getCowname();
            data +="&username2=" + transfer.getUsername2();
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/transfer/gettransfer_cow.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("result(cow)",result);
                Cow c = new Cow();
                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            c.setUsername(transfer.getUsername2());
                            c.setCowname(x.getString("cowname"));
                            c.setFraction(x.getString("fraction"));
                            c.setBirth(x.getString("birth"));
                            c.setDaystart(x.getString("daystart"));
                            c.setStatus(x.getString("status"));
                            c.setDayend(x.getString("dayend"));
                            c.setCalvdate(x.getString("daymilk"));
                            c.setState(x.getString("state"));
                            c.setPapa(x.getString("papa"));
                            c.setPapaf(x.getString("papaf"));
                            c.setMama(x.getString("mama"));
                            c.setMamaf(x.getString("mamaf"));
                            c.setType(x.getString("type"));
                            c.setNickname(x.getString("nickname"));
                            c.setParity(x.getString("parity"));
                            mHelper.addCow(c);
                            Log.d("get(cow)", "done");

                        } catch (Exception e) {

                            Log.d("get(cow)","fail");
                            continue;
                        }

                    }
                }
            }catch (Exception e){
                Log.d("get(cow)","fail");
            }
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/transfer/gettransfer_cbreed.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("link(cbreed)","https://info.rdi.ku.ac.th/cowapp/manager/transfer/gettransfer_cbreed.php" + data);
                Log.d("result(cbreed)",result);

                CBreed cBreed = new CBreed();
                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            cBreed.setUsername(x.getString("username"));
                            cBreed.setCowname(x.getString("cowname"));
                            cBreed.setParity(x.getString("parity"));
                            cBreed.setPapa(x.getString("papa"));
                            cBreed.setBreeddate(x.getString("breeddate"));
                            cBreed.setTimes(x.getString("times"));
                            new CBREEDHelper(Transfer_fromUser.this).addCBreed(cBreed);
                            Log.d("get(cbreed)", "done"+x.getString("cowname")+','+x.getString("parity")+','+x.getString("papa")+','+x.getString("breeddate")+','+x.getString("times"));

                        } catch (Exception e) {

                            Log.d("get(cbreed)","fail");
                            continue;
                        }

                    }
                }
            }catch (Exception e){

                Log.d("get(breed)","fail");
            }
//
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/transfer/gettransfer_calving.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("get(calving)","https://info.rdi.ku.ac.th/cowapp/manager/transfer/gettransfer_calving.php" + data);
                Log.d("result(calving)",result);

                Calving calving = new Calving();
                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            calving.setUsername(transfer.getUsername2());
                            calving.setCowname(x.getString("cowname"));
                            calving.setParity(x.getString("parity"));
                            calving.setCalvingdate(x.getString("calvingdate"));
                            calving.setKid(x.getString("kid"));
                            calving.setRestday(x.getString("restday"));
                            calvingHelper.addCalving(calving);
                            Log.d("get(calving)", "done");

                        } catch (Exception e) {

                            Log.d("get(calving)","fail");
                            continue;
                        }

                    }
                }
            }catch (Exception e){

                Log.d("get(calving)","fail");
            }
            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/transfer/gettransfer_Milk.php" + data).build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                Log.d("get(milk)","https://info.rdi.ku.ac.th/cowapp/manager/transfer/gettransfer_milk.php" + data);
                Log.d("result(milk)",result);
                Milk m = new Milk();
                if (result != null) {

                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            m.setUsername(transfer.getUsername2());
                            m.setCowname(x.getString("cowname"));
                            m.setMilkdate(x.getString("milkdate"));
                            m.setVolume(x.getString("volume"));
                            m.setFat(x.getString("fat"));
                            m.setProtien(x.getString("protein"));
                            m.setLactos(x.getString("lactos"));
                            m.setSolid(x.getString("solid"));
                            m.setSolidmilk(x.getString("solidmilk"));
                            m.setCalvdate(x.getString("calvdate"));
                            m.setParity(x.getString("parity"));
                            milkHelper.addMilk(m);
                            Log.d("get(milk)", "done");

                        } catch (Exception e) {

                            Log.d("get(milk)","fail");
                            continue;
                        }

                    }
                }
                return "success";
            }catch (Exception e){
                Log.d("get(milk)","fail");
            }

            return "fail";

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("success")){
                Log.d("transfer_in","add success");
                finish();
            }
            else{
                Log.d("transfer_in","add fail");
            }
            hideProgressDialog();
            Toast.makeText(mContext,"โคได้ถูกเพิ่มไปยังอุปกรณ์ของคุณแล้ว",Toast.LENGTH_LONG).show();
        }
    }

}
