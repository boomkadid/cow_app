package com.kasidid.mrbboomm.cowlog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.layernet.thaidatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Calendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/23/2016.
 */
public class Activity_6_1 extends Fragment implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{
    TextView remain,header;
    Button mConfirm,abort;
    Calendar mCalendar;
    COWHelper mHelper;
    int kiddo = 0;
    TextView calvingdate,kid;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_3_3, container, false);
        Bundle bundle = this.getArguments();
        final String  id = bundle.getString("ID");
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String username =  prefs.getString("username","not found");
        mHelper = new COWHelper(getActivity());

        final Cow c = mHelper.getCowByID(id);
        calvingdate = (EditText) view.findViewById(R.id.calvingdate);
        kid = (TextView) view.findViewById(R.id.kid);
        kid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setItems(new String[] {"เพศผู้", "เพศเมีย"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                kiddo = 1;
                                kid.setText("เพศผู้");
                                break;
                            case 1:
                                kiddo = 2;
                                kid.setText("เพศเมีย");
                                break;

                        }
                    }
                });
                builder.create();
                builder.show();
            }
        });
        calvingdate.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Activity_6_1.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        mConfirm = (Button) view.findViewById(R.id.confirm);
        abort = (Button)view.findViewById(R.id.abort);
        mCalendar = Calendar.getInstance();
        header = (TextView)view.findViewById(R.id.textheader3_3);
        header.setText("รอยืนยันการคลอด");
        remain = (TextView)view.findViewById(R.id.text3_3);
        remain.setText(null);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun.ttf");
        remain.setTypeface(tf);
        header.setTypeface(tf);
        mConfirm.setTypeface(tf);
        abort.setTypeface(tf);
        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calvingdate.getText().toString().equals("") || kiddo == 0){
                    new AlertDialog.Builder(getActivity())
                            .setMessage("กรุณากรอกข้อมูลให้ครบถ้วน")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage("ยืนยันการคลอด?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {
                        if(calvingdate.getText().toString().equals("")){
                            new AlertDialog.Builder(getActivity())
                                    .setMessage("กรุณากรอกวันที่คลอด")
                                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                            dialogInterface.dismiss();
                                        }
                                    }).create().show();
                            return;
                        }
                        //update cow
                        c.setStatus("4");
                        c.setState("0");
                        c.setDaystart(calvingdate.getText().toString());
                        try {c.setParity(String.valueOf((Integer.parseInt(c.getParity())+1)));}
                        catch (Exception e){c.setParity("1");}
                        c.setCalvdate(calvingdate.getText().toString());
                        c.setType("1");
                        CALVINGHelper calvingHelper = new CALVINGHelper(getActivity());
                        Calving calving = new Calving();
                        calving.setCowname(c.getCowname());
                        calving.setUsername(username);
                        calving.setParity(c.getParity());
                        calving.setCalvingdate(c.getCalvdate());
                        calving.setKid(kid.getText().toString());
                        calving.setRestday("-");
                        calvingHelper.addCalving(calving);
                        new Sync_Calving(calving).execute();
                        mHelper.updateCow(c);
                        getActivity().finish();
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        abort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage("แท้ง?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {
                        Cow cow = mHelper.getCowByID(id);
                        cow.setDaystart(mCalendar.get(Calendar.YEAR)+"-"+(mCalendar.get(Calendar.MONTH)+1)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH));
                        cow.setStatus("0");
                        mHelper.updateCow(cow);
                        Activity_0_1 fragment = new Activity_0_1();
                        Bundle args = new Bundle();
                        args.putString("ID",id);
                        fragment.setArguments(args);
                        android.support.v4.app.FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.replace(R.id.root_frame,fragment);
                        trans.commit();
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

            }
        });


        return view;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        calvingdate.setText(date);
    }

}
