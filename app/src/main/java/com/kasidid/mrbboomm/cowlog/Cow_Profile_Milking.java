package com.kasidid.mrbboomm.cowlog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.layernet.thaidatetimepicker.date.DatePickerDialog;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/19/2016.
 */
public class Cow_Profile_Milking extends ActionBarActivity implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{

    private Toolbar mToolbar;
    private SystemBarTintManager mStatusBarManager;
    COWHelper mHelper;
    float[] milk_volume;
    Cow cow;
    String username;
    CALVINGHelper calvingHelper;
    MilkHelper milkHelper;
    TextView total,avg,report,number,status,drystatus,parity,calvingdate;
    EditText drydate;
    LinearLayout dry;
    @Override
    protected void onResume() {
        super.onResume();
        milk_volume = milkHelper.getMilkVolume(this,cow.getCowname(), cow.getParity());
        calvingHelper = new CALVINGHelper(this);
        avg.setText(String.format("%,.1f", milk_volume[0])+" กก./วัน");
        total.setText(String.format("%,.1f", milk_volume[1])+" กก.");
        number.setText((int) milk_volume[2] + " วัน");
        findViewById(R.id.report).setVisibility(View.GONE);
        try {
            calvingdate.setText(cow.getDayTH(calvingHelper.getReport(cow.getCowname(),cow.getParity()).get(0)));
        }catch (Exception e){
            calvingdate.setText(cow.getCalvdate());
        }
        if (cow.getType().equals("1")){ //cow
            if (!calvingHelper.getReport(cow.getCowname(),cow.getParity()).get(2).equals("-")){ //พักรีด
                status.setText("อยู่ในช่วงพักรีด");
                status.setTextColor(Color.parseColor("#FF0000"));

                dry.setBackgroundColor(Color.parseColor("#D0D0D0"));
                dry.setEnabled(false);
                drystatus.setTextColor(Color.parseColor("#A0A0A0"));
                drystatus.setText("อยู่ในช่วงพักรีดตั้งแต่วันที่"+cow.getDayTH(calvingHelper.getReport(cow.getCowname(),cow.getParity()).get(2)));

            }
            else {
                status.setText("อยู่ในช่วงรีดนม");
            }
            //show report
            String reports = "";
            for (int i = 0; i < Integer.parseInt(cow.getParity()); i++) {
                String parity = String.valueOf(i+1);
                float[] milk_volume = milkHelper.getMilkVolume(this,cow.getCowname(), parity);
                if (milk_volume[1] == 0){continue;}
                CBreed cBreed = new CBREEDHelper(this).getCBreed(cow.getCowname(), parity);
                List<String> calving = calvingHelper.getReport(cow.getCowname(), parity);
                Log.d("calvingdate",calving.get(0));
                reports += "ท้องที่: " + parity +
                        "\nผสมครั้งที่: " + cBreed.getTimes() + " พ่อพันธุ์: " + cBreed.getPapa() +
                        "\nคลอดวันที่: " + cow.getDayTH(calving.get(0)) + " เพศลูก: " + calving.get(1) +
                        "\nน้ำนมรวมทั้งหมด: " + String.format("%,.1f", milk_volume[1])+" กก. " + " น้ำนมเฉลี่ย: " + String.format("%,.1f", milk_volume[0])+" กก./วัน" + "\n" +
                        "น้ำนมรวมที่ 305 วัน: " + String.format("%,.1f",(milk_volume[0] * 305)) +" กก."+ "\n";

            }
            report.setText(reports);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");
        setContentView(R.layout.cow_profile_milking);
        final Intent intent = getIntent();
        final String id = intent.getStringExtra("ID");
        mHelper = new COWHelper(this);

        cow = mHelper.getCowByID(id);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        calvingdate = (TextView) findViewById(R.id.calvingdate);
        status = (TextView)findViewById(R.id.status);
        parity = (TextView)findViewById(R.id.parity);
        parity.setText(cow.getParity());

        drystatus = (TextView)findViewById(R.id.dry_status);
        total = (TextView)findViewById(R.id.total);
        avg = (TextView)findViewById(R.id.avg);
        report = (TextView)findViewById(R.id.report);
        number = (TextView)findViewById(R.id.days);
        String fontPath = "fonts/THSarabun.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(),fontPath);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar birth = Calendar.getInstance();
        try {
            birth.setTime(df.parse(cow.getBirth()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("สถานะการให้นม");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mStatusBarManager = new SystemBarTintManager(this);
        mStatusBarManager.setStatusBarTintEnabled(true);

//===================================================================================
        mHelper = new COWHelper(this);
        milkHelper = new MilkHelper(this);

        avg.setTypeface(tf);
        total.setTypeface(tf);
        number.setTypeface(tf);
        report.setTypeface(tf);
        dry = (LinearLayout)findViewById(R.id.dry);
        dry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Cow_Profile_Milking.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dry_dialog, null);
                dialogBuilder.setView(dialogView);

                drydate = (EditText) dialogView.findViewById(R.id.dry_date);
                drydate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        Calendar now = Calendar.getInstance();
                        com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                                Cow_Profile_Milking.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)

                        );
                        dpd.setAccentColor("#6388ad");
                        dpd.show(getFragmentManager(), "Datepickerdialog");
                    }
                });
                dialogBuilder.setMessage("กรุณาใส่วันพักรีดนม");
                dialogBuilder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (calvingHelper.getCalving(cow.getCowname(),cow.getParity()).getCalvingdate().equals(null)){
                            new AlertDialog.Builder(Cow_Profile_Milking.this)
                                    .setMessage("ไม่สามารถดำเนินการได้เนื่องจากยังไม่ได้กำหนดวันคลอด")
                                    .setNegativeButton("ตกลง",new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    }).create().show();
                        }
                        else {
                            //do something with edt.getText().toString();
                            new AlertDialog.Builder(Cow_Profile_Milking.this)
                                    .setMessage("ต้องการพักรีดนมวันที่ "+cow.getDayTH(drydate.getText().toString()))
                                    .setNegativeButton("ยกเลิก",new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    })
                                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Calving calving = calvingHelper.getCalving(cow.getCowname(),cow.getParity());
                                            calving.setRestday(drydate.getText().toString());
                                            calving.setUsername(username);
                                            calving.setParity(cow.getParity());
                                            calving.setCowname(cow.getCowname());
                                            calvingHelper.addCalving(calving);
                                            new Sync_Calving(calving).execute();
                                            startActivity(getIntent());
                                            finish();
                                        }
                                    }).create().show();
                        }

                        dialog.dismiss();
                    }
                });
                dialogBuilder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //pass
                        dialog.dismiss();
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });


    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        drydate.setText(date);
    }
    private ProgressDialog mProgressDialog;
    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
    public class Sync_GetUser extends AsyncTask<String,Void,String>{
        Context mContext;
        String farmid, data, cowname;
        ArrayList<String> usernames = new ArrayList<>();
        ArrayList<String> owners = new ArrayList<>();
        public Sync_GetUser(Context context,String FarmID, String cowname_){
            mContext = context;
            farmid = FarmID;
            cowname = cowname_;
        }
        @Override
        protected String doInBackground(String... params) {
            data= "";

            try{
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url("https://info.rdi.ku.ac.th/cowapp/manager/transfer/getmember.php" + data).build();
                Response response = client.newCall(request).execute();
                String result = response.body().string();
                Log.d("result(farm)",result);

                if (result != null) {
                    JSONArray rec = new JSONArray(result);
                    for (int i = 0; i < rec.length(); i++) {
                        try {
                            JSONObject x = rec.getJSONObject(i);
                            usernames.add(x.getString("username"));
                            owners.add(x.getString("owner"));
                            Log.d("get(farm)", "done");

                        } catch (Exception e) {

                            Log.d("get(farm)","fail");
                            continue;
                        }

                    }
                }
                return "success";
            }catch (Exception e){

                Log.d("get(farm)","fail");
            }
            return "fail";
        }

        @Override
        protected void onPostExecute(String s) {
            hideProgressDialog();
            if (s.equals("success")){

                Intent intent = new Intent(Cow_Profile_Milking.this,Transfer_toUser.class);
                intent.putExtra("usernames", usernames);
                intent.putExtra("owners",  owners);
                intent.putExtra("cowname",  cowname);
                startActivity(intent);
                finish();
            }
            else{
                Toast.makeText(Cow_Profile_Milking.this, "กรุณาเชื่อมต่ออินเตอร์เน็ต", Toast.LENGTH_LONG).show();

            }
        }
    }

}