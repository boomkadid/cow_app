package com.kasidid.mrbboomm.cowlog;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 1/4/2017.
 */

public class Sync_Profile extends AsyncTask<String, Void, String> {
    private String username;
    private String mail;
    private String fullname;
    private String displayname;

    public Sync_Profile(String username,String mail,String fullname, String displayname) {
        this.username = username;
        this.mail = mail;
        this.fullname = fullname;
        this.displayname = displayname;
    }

    @Override
    protected String doInBackground(String... strings) {
        String link;
        String data;
        try {
            data = "?username=" + URLEncoder.encode(username, "UTF-8");
            data += "&mail=" + URLEncoder.encode(mail, "UTF-8");
            data += "&fullname=" + URLEncoder.encode(fullname, "UTF-8");
            data += "&displayname=" + URLEncoder.encode(displayname, "UTF-8");
            link = "https://info.rdi.ku.ac.th/cowapp/manager/updateprofile.php" + data;

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(link).build();
            Response response = client.newCall(request).execute();
            Log.d("link_profile", link);
            return response.body().string();

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("link", "FAIL sync");
            return new String("Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        String jsonStr = result;
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String query_result = jsonObj.getString("query_result");
                if (query_result.equals("SUCCESS")) {
                    Log.d("result_sync", "SUCESS");
                } else if (query_result.equals("FAILURE")) {
                    Log.d("result_sync", "FAILURE");
                } else {

                    Log.d("result_sync", "Couldn't connect");
                }
            } catch (JSONException e) {
                Log.d("result_sync", result);
            }
        } else {
            Log.d("result_sync", "couldn't get JSON data");
        }
    }

}