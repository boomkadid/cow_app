package com.kasidid.mrbboomm.cowlog;

import android.provider.BaseColumns;

/**
 * Created by Kasidid on 2/27/2017.
 */

public class Breed {
    private int id;
    private String username;
    private String cowname;
    private String parity;
    private String papa;
    private String breeddate;
    public static final String TABLE  = "breed";
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String USERNAME = "username";
        public static final String COWNAME = "number";
        public static final String PARITY = "parity";
        public static final String PAPA = "papa";
        public static final String BREEDDATE = "breeddate";
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCowname() {
        return cowname;
    }

    public void setCowname(String cowname) {
        this.cowname = cowname;
    }

    public String getParity() {
        return parity;
    }

    public void setParity(String parity) {
        this.parity = parity;
    }

    public String getPapa() {
        return papa;
    }

    public void setPapa(String papa) {
        this.papa = papa;
    }

    public String getBreeddate() {
        return breeddate;
    }

    public void setBreeddate(String breeddate) {
        this.breeddate = breeddate;
    }
}
