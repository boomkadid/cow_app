package com.kasidid.mrbboomm.cowlog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mrbboomm on 10/1/2016.
 */
public class Activity_Main extends AppCompatActivity {


    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    COWHelper mHelper;
    Cow c;
    String id,username;
    TextView name,birth,fraction,age;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");
        getIntent().setAction("Already created");
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        id = intent.getStringExtra("ID");
        Toolbar toolbar = (Toolbar) findViewById(R.id.id_toolbar);
        setSupportActionBar(toolbar);
        mHelper = new COWHelper(this);
        c = mHelper.getCowByID(id);
//        new Sync_DB(c).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);;
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText(c.getNickname());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new Sync_DB(c).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                finish();
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
            }
        });
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        TabLayout tabLayout = (TabLayout) findViewById(R.id.id_tabs);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);

        name = (TextView)findViewById(R.id.name);
        birth = (TextView)findViewById(R.id.birth);
        fraction = (TextView)findViewById(R.id.fraction);
        age = (TextView)findViewById(R.id.age);
        Calendar today = Calendar.getInstance();
        Calendar birthday = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            birthday.setTime(df.parse(c.getBirth()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        name.setText("หมายเลขโค: "+c.getCowname());
        int diffYear = today.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);
        int diffMonth = today.get(Calendar.MONTH) - birthday.get(Calendar.MONTH);
        if (diffMonth <0){ diffMonth += 12; diffYear -= 1;}
        if (diffYear < 1) age.setText("อายุ: "+diffMonth+" เดือน");
        else age.setText("อายุ: "+diffYear+" ปี "+diffMonth+" เดือน");
        birth.setText("วันเกิด: "+c.getDayTH(c.getBirth()));
        fraction.setText("ระดับสายเลือด: "+c.getFraction());
        String fontPath = "fonts/THSarabun.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(),fontPath);
        name.setTypeface(tf);
        birth.setTypeface(tf);
        fraction.setTypeface(tf);
        age.setTypeface(tf);
        Button showprofile = (Button)findViewById(R.id.edit);
        showprofile.setTypeface(tf);
        showprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_Main.this,Cow_Profile2.class);
                intent.putExtra("ID",id);
                startActivity(intent);
                finish();
            }
        });
        ImageView im = (ImageView)findViewById(R.id.image_profile);
        Picasso.with(this).load("https://info.rdi.ku.ac.th/cowapp/manager/images/"+c.getUsername()+"/"+c.getCowname()+"/1.png").error(R.drawable.cow_acc).resize(350,350).centerInside().into(im);
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Bundle args = new Bundle();
            args.putString("ID",id);
            switch (position){
                case 0:
                    fragment = new Root_Activity();
                    break;
                case 1:
                    fragment = new Activity_Breed();
                    break;
                case 2:
                    fragment = new Activity_Image();
                    break;
            }

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "สถานะ";
                case 1:
                    return "ประวัติการผสม";
                case 2:
                    return "รูปภาพ";
            }
            return null;
        }
    }
}