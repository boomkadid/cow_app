package com.kasidid.mrbboomm.cowlog;

import android.provider.BaseColumns;

/**
 * Created by Kasidid on 2/27/2017.
 */

public class Transfer {
    private int id;
    private String username;
    private String cowname;
    private String username2;
    public static final String TABLE  = "transfer";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCowname() {
        return cowname;
    }

    public void setCowname(String cowname) {
        this.cowname = cowname;
    }

    public String getUsername2() {
        return username2;
    }

    public void setUsername2(String username2) {
        this.username2 = username2;
    }
}
