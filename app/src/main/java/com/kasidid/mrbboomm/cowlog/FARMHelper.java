package com.kasidid.mrbboomm.cowlog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kasidid on 2/27/2017.
 */

public class FARMHelper extends SQLiteOpenHelper {
    public FARMHelper(Context context) {
        super(context, "farm.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BREED_TABLE = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT)",
                Farm.TABLE,
                Farm.Column.ID,
                Farm.Column.USERNAME,
                Farm.Column.FARMID,
                Farm.Column.FARMNAME,
                Farm.Column.OWNER,
                Farm.Column.ADDRESS);

        db.execSQL(CREATE_BREED_TABLE);
    }
    private ContentValues getValues(Farm farm){
        ContentValues values = new ContentValues();
        values.put(Farm.Column.USERNAME, farm.getUsername());
        values.put(Farm.Column.FARMID, farm.getFarmid());
        values.put(Farm.Column.FARMNAME, farm.getFarmname());
        values.put(Farm.Column.OWNER, farm.getOwner());
        values.put(Farm.Column.ADDRESS,farm.getAddress());
        return values;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_FARM_TABLE = "DROP TABLE IF EXISTS farm";
        db.execSQL(DROP_FARM_TABLE);
        onCreate(db);
    }
    public void addFarm(Farm farm){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getValues(farm);
        db.insert(Farm.TABLE, null, values);
        db.close();
    }
    public void clearFarm(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Farm.TABLE, null, null);
        db.close();
    }
    public Farm getFarm(String username){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query
                (Farm.TABLE, null, Farm.Column.USERNAME+"=?",
                        new String[]{username}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        else{
            return null;
        }
        Farm f = new Farm();
        while(!cursor.isAfterLast()) {
            f.setUsername(cursor.getString(1));
            f.setFarmid(cursor.getString(2));
            f.setFarmname(cursor.getString(3));
            f.setOwner(cursor.getString(4));
            f.setAddress(cursor.getString(5));
            cursor.moveToNext();
        }

        return f;
    }
    public boolean checkFarm(String username){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        boolean found = false;
        Cursor cursor = sqLiteDatabase.query
                (Farm.TABLE, null, Farm.Column.USERNAME+"=?",
                        new String[]{username}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        else{
            return false;
        }
        Farm f = new Farm();
        while(!cursor.isAfterLast()) {
            f.setUsername(cursor.getString(1));
            f.setFarmid(cursor.getString(2));
            f.setFarmname(cursor.getString(3));
            f.setOwner(cursor.getString(4));
            f.setAddress(cursor.getString(5));
            found = true;
            cursor.moveToNext();
        }
        cursor.close();
        return found;
    }

}
