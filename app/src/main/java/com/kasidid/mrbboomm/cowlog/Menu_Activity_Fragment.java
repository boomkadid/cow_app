package com.kasidid.mrbboomm.cowlog;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import me.leolin.shortcutbadger.ShortcutBadger;


/**
 * Created by mrbboomm on 7/18/2016.
 */
public class Menu_Activity_Fragment extends Fragment implements  SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener{
//    ExpandableListView exlist;
    List<String> cows_id,cows_nickname;
    RecycleAdapter mAdapter;
    String username;
    COWHelper mHelper;
    RecyclerView mRecyclerView;
    @Override
    public void onResume() {
        super.onResume();
        getActivity().findViewById(R.id.search_list).setVisibility(View.GONE);
        cows_id = mHelper.getCowNot(username,0);
        String[] ids = cows_id.toArray(new String[0]);
        mAdapter = new RecycleAdapter(getActivity(),ids);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        getActivity().getMenuInflater().inflate(R.menu.menu_search,menu);
        final MenuItem item = menu.findItem(R.id.menuSearch);
        final SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                query = null;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String[] ids;
                if (newText.length() == 0){
                    cows_id = mHelper.getCowNot(username,0);
                    ids = cows_id.toArray(new String[0]);
                }
                else{
                    List<String> cows_filter = new ArrayList<String>();
                    cows_id = mHelper.getCowArray(username,0,-1,true);
                    cows_nickname = mHelper.getCowArray(username,15,-1,true);
                    for(int i=0;i<cows_nickname.size();i++)
                    {
                        if(cows_nickname.get(i).toLowerCase().startsWith(newText.toLowerCase()))
                        {
                            cows_filter.add(cows_id.get(i));
                        }
                    }

                    ids = cows_filter.toArray(new String[0]);
                }

                mAdapter = new RecycleAdapter(getActivity(),ids);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mAdapter);
                return false;
            }

        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        username =  prefs.getString("username","not found");
        View view = inflater.inflate(R.layout.menu_activity_f, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.listView1);

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.userprofile_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("สถานะโค");
        mHelper = new COWHelper(getActivity());

        return view;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem menuItem) {
        return false;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
        return false;
    }

    private static class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.ViewHolder>{
        Context mContext;
        String[] ID;
        public RecycleAdapter(Context context, String[] ids){
            mContext = context;
            ID = ids;
        }
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            TextView Date,Status,priority,cowname;
            ImageView imageView;

            public ViewHolder(View view) {
                super(view);
                cowname = (TextView)view.findViewById(R.id.cowname);
                imageView = (ImageView) view.findViewById(R.id.imageView1_act);
                Date = (TextView)view.findViewById(R.id.date);
                Status = (TextView)view.findViewById(R.id.status);

                 priority = (TextView)view.findViewById(R.id.priority);

                view.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext,Activity_Main.class);
                intent.putExtra("ID",ID[getAdapterPosition()]);
                mContext.startActivity(intent);
            }
        }
        @Override
        public RecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext)
                    .inflate(R.layout.menu_activity_item, parent, false);
            RecycleAdapter.ViewHolder viewHolder = new RecycleAdapter.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecycleAdapter.ViewHolder holder, int position) {
            COWHelper mHelper = new COWHelper(mContext);
            Cow c = mHelper.getCowByID(String.valueOf(ID[position]));
            if (c.getNickname().equals("")) holder.cowname.setText(c.getCowname());
            else holder.cowname.setText(c.getNickname());
            holder.Date.setText(c.getDaystart()+" ถึง "+c.getDayend());
            holder.Status.setText(c.showStatus());
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            switch(c.getStatus()){
                case "0":{
                    holder.priority.setText("-");
                    break;
                }
                case "1":{
                    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH");
                    Calendar mark = Calendar.getInstance();
                    Calendar starta = Calendar.getInstance();
                    Calendar enda = Calendar.getInstance();
                    Calendar noon = Calendar.getInstance();
                    noon.set(Calendar.HOUR_OF_DAY, 12);
                    try {
                        mark.setTime(df2.parse(c.getState()));
                        starta.setTime(df2.parse(c.getState()));
                        enda.setTime(df2.parse(c.getState()));
                        enda.add(Calendar.HOUR,2);
                    } catch (ParseException e) {
                    }
                    if(c.getState().equals("0") || c.getState()==null){
                        holder.priority.setText("ตรวจสอบการเป็นสัด");
                        holder.priority.setTextColor(Color.RED);
                        break;
                    }
                    if (c1.before(enda) && c1.after(starta)){
                        holder.priority.setText("พร้อมผสมพันธุ์");
                        holder.priority.setTextColor(Color.RED);
                    }
                    else if(c1.after(starta)){
                        if (mark.before(noon) && c1.before(noon))
                            holder.priority.setText("ควรผสมแม่โคภายในเที่ยงนี้");
                        else
                            holder.priority.setText("ควรผสมแม่โคภายในวันนี้");
                        holder.priority.setTextColor(Color.RED);
                    }
                    else{
                        if (starta.get(Calendar.DAY_OF_YEAR) == c1.get(Calendar.DAY_OF_YEAR))
                            holder.priority.setText("ผสมแม่โควันนี้"+ " ช่วงเวลา "+starta.get(Calendar.HOUR_OF_DAY)+"-"+(enda.get(Calendar.HOUR_OF_DAY))+" นาฬิกา");
                        else
                            holder.priority.setText("ผสมแม่โควันพรุ่งนี้"+ " ช่วงเวลา "+starta.get(Calendar.HOUR_OF_DAY)+"-"+(enda.get(Calendar.HOUR_OF_DAY))+" นาฬิกา");
                        holder.priority.setTextColor(Color.RED);
                    }
                    break;
                }
                case "2":{
                    if(c.getState()==null){
                        holder.priority.setText(null);
                        holder.priority.setTextColor(Color.RED);
                        break;
                    }
                    if(c.getState().equals("1") || c.getState().equals("2") || c.getState().equals("3")){
                        holder.priority.setText("ตรวจสอบการกลับสัดครั้งที่ "+c.getState());
                        holder.priority.setTextColor(Color.RED);
                    }else{
                        try {
                            c1.setTime(df.parse(c.getState()));
                            c2.setTime(df.parse(c.getState()));
                        } catch (ParseException e) {
//                            e.printStackTrace();
                        }
                        c2.add(Calendar.DATE,6);
                        holder.priority.setText("รอตรวจสอบการกลับสัด");
                        holder.priority.setTextColor(Color.parseColor("#E69500"));
                    }
                    break;
                }
                case "3":{

                    if(c.getState().equals("0")) {
                        holder.priority.setText("เรียกหมอตรวจท้อง");
                        holder.priority.setTextColor(Color.RED);
                    }
                    else if (c.getState().equals("1")){
                        holder.priority.setText("คลอดวันที่ "+c.getDayTH(c.getDayend()));
                        holder.priority.setTextColor(Color.parseColor("#00b200"));
                    }
                    else if (c.getState().equals("2")){
                        holder.priority.setText("อยู่ในระยะใกล้คลอด");
                        holder.priority.setTextColor(Color.RED);
                    }
                    break;
                }
                case "4":{
                    try {
                        c2.setTime(df.parse(c.getDayend()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    int a = c1.get(Calendar.DAY_OF_YEAR);
                    int b = c2.get(Calendar.DAY_OF_YEAR);
                    if (c2.get(Calendar.YEAR)!=c1.get(Calendar.YEAR) ) holder.priority.setText("อีก "+Integer.toString(365-a+b)+" วัน");
                    else holder.priority.setText("อีก "+Integer.toString(b-a)+" วันตรวจสอบการเป็นสัด");
                    holder.priority.setTextColor(Color.parseColor("#00b200"));
                    break;
                }
                case "5":{
                    try {
                        c2.setTime(df.parse(c.getDayend()));
                    } catch (ParseException e) {
//                        e.printStackTrace();
                    }
                    int a = c1.get(Calendar.DAY_OF_YEAR);
                    int b = c2.get(Calendar.DAY_OF_YEAR);
                    if (c2.get(Calendar.YEAR)!=c1.get(Calendar.YEAR) ) holder.priority.setText("อีก "+Integer.toString(365-a+b)+" วัน");
                    else holder.priority.setText("อีก "+Integer.toString(b-a)+" วันตรวจสอบการเป็นสัด");
                    holder.priority.setTextColor(Color.parseColor("#E69500"));
                    break;
                }
                case "6":{
                    holder.priority.setText("รอยืนยันสถานะคลอด");
                    holder.priority.setTextColor(Color.RED);
                    break;
                }
            }
            Picasso.with(mContext).load("https://info.rdi.ku.ac.th/cowapp/manager/images/"+c.getUsername()+"/"+c.getCowname()+"/1.png").error(R.drawable.cow_acc).resize(300,300).centerInside().into(holder.imageView);
        }

        @Override
        public int getItemCount() {
            return ID.length;
        }
    }

}