package com.kasidid.mrbboomm.cowlog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;


/**
 * Created by mrbboomm on 7/11/2016.
 */
public class COWHelper extends SQLiteOpenHelper{
    public COWHelper(Context context) {
        super(context, "cow.db", null, 25);
    }
    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_COW_TABLE = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                "%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s DATETIME,%s DATETIME,%s STRING,%s DATETIME,%s TEXT,%s TEXT,%s TEXT,%s TEXT, %s TEXT,%s TEXT, %s TEXT, %s TEXT,UNIQUE(%s,%s) ON CONFLICT REPLACE)",
                Cow.TABLE,
                Cow.Column.ID,
                Cow.Column.USERNAME,
                Cow.Column.COWNAME,
                Cow.Column.FRACTION,
                Cow.Column.BIRTH,
                Cow.Column.DAYSTART,
                Cow.Column.STATUS,
                Cow.Column.DAYEND,
                Cow.Column.CALVDATE,
                Cow.Column.STATE,
                Cow.Column.PAPA,
                Cow.Column.PAPAF,
                Cow.Column.MAMA,
                Cow.Column.MAMAF,
                Cow.Column.TYPE,
                Cow.Column.NICKNAME,
                Cow.Column.PARITY,
                Cow.Column.USERNAME,Cow.Column.COWNAME);

        db.execSQL(CREATE_COW_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String DROP_COW_TABLE = "DROP TABLE IF EXISTS cow";
        db.execSQL(DROP_COW_TABLE);
        onCreate(db);
        db.execSQL(String.format("CREATE UNIQUE INDEX myindex ON %s(%s,%s)",Cow.TABLE,Cow.Column.USERNAME,Cow.Column.COWNAME));
    }
    private ContentValues getValues(Cow cow){
        ContentValues values = new ContentValues();
        values.put(Cow.Column.COWNAME, cow.getCowname());
        values.put(Cow.Column.NICKNAME, cow.getNickname());
        values.put(Cow.Column.FRACTION, cow.getFraction());
        values.put(Cow.Column.USERNAME, cow.getUsername());
        values.put(Cow.Column.CALVDATE, cow.getCalvdate());
        values.put(Cow.Column.BIRTH, cow.getBirth());
        values.put(Cow.Column.DAYSTART,cow.getDaystart());
        values.put(Cow.Column.STATUS,cow.getStatus());
        values.put(Cow.Column.STATE,cow.getState());
        values.put(Cow.Column.PAPA,cow.getPapa());
        values.put(Cow.Column.PAPAF,cow.getPapaf());
        values.put(Cow.Column.MAMA,cow.getMama());
        values.put(Cow.Column.MAMAF,cow.getMamaf());
        values.put(Cow.Column.DAYEND,cow.getEndDate());
        values.put(Cow.Column.TYPE,cow.getType());
        values.put(Cow.Column.PARITY,cow.getParity());
        return values;
    }
    public void addCow(Cow cow){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getValues(cow);
        db.insert(Cow.TABLE, null, values);
        db.close();
    }
    public  List<String> getCowNot(String username,Integer index){
        List<String> high = new ArrayList<String>();
        List<String> low = new ArrayList<String>();
        Cursor cursor;
        String status,state ;
        Calendar mCalendar = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH");
        Calendar starta = Calendar.getInstance();
        SQLiteDatabase db = this.getWritableDatabase();
        cursor = db.query
                (Cow.TABLE, null, Cow.Column.USERNAME + "=?", new String[]{username}, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            status = cursor.getString(6).toString();
            state = cursor.getString(9).toString();
            try {
                starta.setTime(df.parse(state));
            } catch (ParseException e) {
//                e.printStackTrace();
            }
            if (cursor.getString(14).equals("2")){
                cursor.moveToNext();
                continue;
            }
            if(((status.equals("1") ) || (status.equals("6")) ||
                    (status.equals("2")&&(state.equals("1")||state.equals("2")||state.equals("3"))) ) ||
                    (status.equals("3")&&(!state.equals("1"))))
                high.add(cursor.getString(index).toString());
            else
                low.add(cursor.getString(index).toString());
            cursor.moveToNext();
        }
        cursor.close();
        List<String> newList = new ArrayList<String>(high);
        newList.addAll(low);
        return  newList;

    }
    public List<String> getCowArray(String username,int column,int type, boolean sorted) {
        List<String> cows = new ArrayList<String>();
        List<String> cows_sorted = new ArrayList<String>();
        List<String> cows_nickname = new ArrayList<String>();
        List<String> cows_nickname_sorted = new ArrayList<String>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();
        cursor = db.query
                (Cow.TABLE, null, Cow.Column.USERNAME + "=?", new String[]{username}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            if (!cursor.getString(14).equals(String.valueOf(type)) && type != -1){ cursor.moveToNext(); continue;}
//            if (type==2) type = 1;
            cows.add(cursor.getString(column).toString());
            cows_nickname.add(cursor.getString(15).toString());
            cows_nickname_sorted.add(cursor.getString(15).toString());
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return cows;
    }
    public List<String> getCowSearchArray(String username,int column,int type, boolean sorted) {
        List<String> cows = new ArrayList<String>();
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();
        cursor = db.query
                (Cow.TABLE, null, Cow.Column.USERNAME + "=?", new String[]{username}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            if (!cursor.getString(14).equals(String.valueOf(type))){ cursor.moveToNext(); continue;}
            Log.d("name",cursor.getString(column).toString());
//            if(cows.contains(cursor.getString(column).toString())) { cursor.moveToNext(); continue;}
            Log.d("have","have"); cows.add(cursor.getString(column).toString());
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return cows;
    }
    public Cow getCowByID(String id){
        Cursor cursor;
        SQLiteDatabase db = this.getWritableDatabase();
        Cow cow = new Cow();
        cursor =  db.rawQuery("select * from " +Cow.TABLE + " where " + Cow.Column.ID + "=" + id, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        if (!cursor.isAfterLast()) {
            cow.setId(cursor.getInt(0));
            cow.setUsername(cursor.getString(1));
            cow.setCowname(cursor.getString(2));
            cow.setFraction(cursor.getString(3));
            cow.setBirth(cursor.getString(4));
            cow.setDaystart(cursor.getString(5));
            cow.setStatus(cursor.getString(6));
            cow.setDayend(cursor.getString(7));
            cow.setCalvdate(cursor.getString(8));
            cow.setState(cursor.getString(9));
            cow.setPapa(cursor.getString(10));
            cow.setPapaf(cursor.getString(11));
            cow.setMama(cursor.getString(12));
            cow.setMamaf(cursor.getString(13));
            cow.setType(cursor.getString(14));
            cow.setNickname(cursor.getString(15));
            cow.setParity(cursor.getString(16));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return cow;
    }
    public void updateCow(Cow cow) {

        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues values = getValues(cow);
        db.update(Cow.TABLE,
                values,
                Cow.Column.ID + " = ? ",
                new String[] { String.valueOf(cow.getId()), });

        db.close();
    }
    public void deleteCow(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Cow.TABLE, Cow.Column.ID + " = " + id, null);
        db.close();
    }
    public void deleteCowbyName(String cowname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Cow.TABLE, Cow.Column.COWNAME + "='" + cowname+"'", null);
        db.close();
    }
    public void clearCow(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Cow.TABLE, null, null);
        db.close();
    }
    public void dailyUpdate(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cow cow;
        Cursor cursor =  db.rawQuery("select * from " +Cow.TABLE + " where 1=1" , null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        Calendar daystart = Calendar.getInstance();
        Calendar dayend = Calendar.getInstance();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        while (!cursor.isAfterLast()) {

            cow = this.getCowByID(String.valueOf(cursor.getInt(0)));

            if (cow.getStatus().isEmpty()) cow.setStatus("0");

            try {
                dayend.setTime(df.parse(cow.getEndDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            Log.d("check",Calendar.getInstance().get(Calendar.YEAR)+"-"+(Calendar.getInstance().get(Calendar.MONTH)+1)+"-"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH)+dayend.get(Calendar.YEAR)+"-"+(dayend.get(Calendar.MONTH)+1)+"-"+dayend.get(Calendar.DAY_OF_MONTH));
            dayend.add(Calendar.DATE,1);
            while (Calendar.getInstance().after(dayend)){

//                Log.d("check",cow.getCowname()+"  "+cow.getStatus()+"   "+dayend.get(Calendar.YEAR)+"-"+(dayend.get(Calendar.MONTH)+1)+"-"+dayend.get(Calendar.DAY_OF_MONTH));
                if (cow.getStatus().equals("0") || Calendar.getInstance().get(Calendar.YEAR) - dayend.get(Calendar.YEAR) > 2 || dayend.get(Calendar.YEAR) > Calendar.getInstance().get(Calendar.YEAR) ){ // long time not update > 2 years
                    cow.setDaystart(Calendar.getInstance().get(Calendar.YEAR)+"-"+(Calendar.getInstance().get(Calendar.MONTH)+1)+"-"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                    cow.setStatus("0");
                    cow.setDayend(cow.getEndDate());
                    break;
                }
                try {
                    daystart.setTime(df.parse(cow.getEndDate()));
                    daystart.add(Calendar.DATE,+1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                cow.setDaystart(daystart.get(Calendar.YEAR)+"-"+(daystart.get(Calendar.MONTH)+1)+"-"+daystart.get(Calendar.DAY_OF_MONTH));
                switch(cow.getStatus()){
                    case "1":{
                        cow.setStatus("5");
                        break;
                    }
                    case "2":{
                        cow.setStatus("3");
                        break;
                    }
                    case "3":{
                        cow.setType("1");
                        cow.setStatus("6");
                        break;
                    }
                    case "4":{
                        cow.setStatus("1");
                        break;
                    }
                    case "5":{
                        cow.setStatus("1");
                        break;
                    }
                    default:{
                        cow.setStatus("0");
                        break;
                    }
                }
//                if (!cow.getStatus().equals("3")) cow.setState("0");
                cow.setState("0");
                try {
                    dayend.setTime(df.parse(cow.getEndDate()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cow.setDayend(cow.getEndDate());
            }

            cow = StateUpdate(cow);
            this.updateCow(cow);

            cursor.moveToNext();
        }
        cursor.close();
        db.close();

    }
    public Cow StateUpdate(Cow cow){
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar today = Calendar.getInstance();
        switch(cow.getStatus()){
            case "1":{
                if (cow.getState() == null || cow.getState().equals("0")) {
                    cow.setState("0");
                    break;
                }

                break;
            }
            case "2":{
                try {
                    c1.setTime(df.parse(cow.getDaystart()));
                    c2.setTime(df.parse(cow.getDaystart()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c1.add(Calendar.DATE,+18);
                c2.add(Calendar.DATE,+25);
                if (today.before(c2)){
                    if (today.before(c1)) cow.setState(c1.get(Calendar.YEAR)+"-"+(c1.get(Calendar.MONTH)+1)+"-"+c1.get(Calendar.DAY_OF_MONTH));
                    else  cow.setState("1");
                    break;
                }
                c1.add(Calendar.DATE,+21);
                c2.add(Calendar.DATE,+21);
                if (today.before(c2)){
                    if (today.before(c1)) cow.setState(c1.get(Calendar.YEAR)+"-"+(c1.get(Calendar.MONTH)+1)+"-"+c1.get(Calendar.DAY_OF_MONTH));
                    else  cow.setState("2");
                    break;
                }
                c1.add(Calendar.DATE,+21);
                c2.add(Calendar.DATE,+22);
                if (today.before(c2)){
                    if (today.before(c1)) cow.setState(c1.get(Calendar.YEAR)+"-"+(c1.get(Calendar.MONTH)+1)+"-"+c1.get(Calendar.DAY_OF_MONTH));
                    else cow.setState("3");
                    break;
                }
            }
            case "3":{
                if (!cow.getState().equals("1"))
                    cow.setState("0");
                try {
                    c1.setTime(df.parse(cow.getDaystart()));
                    c2.setTime(df.parse(cow.getEndDate()));
                } catch (ParseException e) {
                }
                c2.add(Calendar.DATE,-10-20+20);
                if (today.after(c2)) cow.setState("2");
                break;
            }
            default:{
                cow.setState("0");
                break;
            }
        }
        return cow;
    }
    public String Countcow(Context mcontext,boolean header){
        int[] cows = new int[]{0,0};
        int[] huffer = new int[]{0,0,0};
        int[] parity_cows = new int[]{0,0,0,0,0,0};
        int total = 0,huffers = 0,cowss = 0 ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor =  db.rawQuery("select * from " +Cow.TABLE + " where " +"1"+ "=" + "1", null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        else{
            String list = "1.โคสาวจำนวน "+ (huffer[0]+huffer[1]+huffer[2])+" ตัว ("+huffers*100 /total+" %)\n"+
                    "   โคสาวยังไม่ผสมพันธุ์ จำนวน "+huffer[0]+" ตัว\n"+
                    "   โคสาวกำลังผสมพันธุ์ จำนวน "+huffer[1]+" ตัว\n"+
                    "   โคสาวตั้งท้อง จำนวน "+huffer[2]+" ตัว\n"+
                    "2.แม่โคจำนวน "+ (cows[0]+cows[1])+" ตัว ("+(100-(huffers*100 /total))+" %)\n"+
                    "   โครีดนม จำนวน "+cows[0]+" ตัว\n"+
                    "   โคแห้งนม จำนวน "+cows[1]+" ตัว\n"+
                    "3.สรุปรายละเอียด\n"+
                    "   โคท้องที่ 1 จำนวน "+parity_cows[1]+parity_cows[0]+" ตัว\n"+
                    "   โคท้องที่ 2 จำนวน "+parity_cows[2]+" ตัว\n"+
                    "   โคท้องที่ 3 จำนวน "+parity_cows[3]+" ตัว\n"+
                    "   โคท้องที่ 4 จำนวน "+parity_cows[4]+" ตัว\n"+
                    "   โคท้องที่ 5 ขึ้นไป จำนวน "+parity_cows[5]+" ตัว";
            if(header)return "จำนวนโคทั้งหมด "+(huffer[0]+huffer[1]+huffer[2]+cows[0]+cows[1])+" ตัว";
            else return list;
        }
        while (!cursor.isAfterLast()) {
            if (cursor.getString(14).equals("0")){
                huffers += 1;
                if ( cursor.getString(6).equals("0")) huffer[0] += 1;
                else if(cursor.getString(6).equals("2")) huffer[1] += 1;
                else if(cursor.getString(6).equals("3")) huffer[2] += 1;
            }
            else{
                cowss += 1;
                CALVINGHelper calvingHelper = new CALVINGHelper(mcontext);
                if (!calvingHelper.getReport(cursor.getString(2),cursor.getString(16)).get(2).equals("-")) cows[1] += 1;
                else cows[0] += 1;
                if (cursor.getString(16).equals("0")) parity_cows[0]+=1;
                else if (cursor.getString(16).equals("1")) parity_cows[1]+=1;
                else if (cursor.getString(16).equals("2")) parity_cows[2]+=1;
                else if (cursor.getString(16).equals("3")) parity_cows[3]+=1;
                else if (cursor.getString(16).equals("4")) parity_cows[4]+=1;
                else parity_cows[5] += 1;

            }
            total += 1;
            cursor.moveToNext();
        }
        if (total==0) total = 1;
        String list = "1.โคสาวจำนวน "+ (huffer[0]+huffer[1]+huffer[2])+" ตัว ("+huffers*100 /total+" %)\n"+
                "   โคสาวยังไม่ผสมพันธุ์ จำนวน "+huffer[0]+" ตัว\n"+
                "   โคสาวกำลังผสมพันธุ์ จำนวน "+huffer[1]+" ตัว\n"+
                "   โคสาวตั้งท้อง จำนวน "+huffer[2]+" ตัว\n"+
                "2.แม่โคจำนวน "+ (cows[0]+cows[1])+" ตัว ("+(100-(huffers*100 /total))+" %)\n"+
                "   โครีดนม จำนวน "+cows[0]+" ตัว\n"+
                "   โคแห้งนม จำนวน "+cows[1]+" ตัว\n"+
                "3.สรุปรายละเอียด\n"+
                "   โคท้องที่ 1 จำนวน "+((cows[0]+cows[1])-parity_cows[2]-parity_cows[3]-parity_cows[4]-parity_cows[5])+" ตัว\n"+
                "   โคท้องที่ 2 จำนวน "+parity_cows[2]+" ตัว\n"+
                "   โคท้องที่ 3 จำนวน "+parity_cows[3]+" ตัว\n"+
                "   โคท้องที่ 4 จำนวน "+parity_cows[4]+" ตัว\n"+
                "   โคท้อง 5 ขึ้นไป จำนวน "+parity_cows[5]+" ตัว";
        if(header)return "จำนวนโคทั้งหมด "+(huffer[0]+huffer[1]+huffer[2]+cows[0]+cows[1])+" ตัว";
        else return list;
    }
}