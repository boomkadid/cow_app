package com.kasidid.mrbboomm.cowlog;


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by mrbboomm on 8/23/2016.
 */
public class Activity_1_1 extends Fragment {
    Button mConfirm;
    Calendar mCalendar;
    COWHelper mHelper;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_1_1, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle bundle = this.getArguments();
        final String  id = bundle.getString("ID");
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        mConfirm = (Button)getActivity().findViewById(R.id.mConfirm);
        mCalendar = Calendar.getInstance();

        TextView header = (TextView)getActivity().findViewById(R.id.textheader);
        mHelper = new COWHelper(getActivity());
        final Cow c = mHelper.getCowByID(id);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun.ttf");
        mConfirm.setTypeface(tf);
        header.setTypeface(tf);
        mConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage("พบอาการเป็นสัด?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {

                        if (mCalendar.get(Calendar.HOUR_OF_DAY) <= 12)
                            c.setState(mCalendar.get(Calendar.YEAR)+"-"+(mCalendar.get(Calendar.MONTH)+1)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH)+" 16");
                        else {
                            mCalendar.add(Calendar.DATE,1);
                            c.setState(mCalendar.get(Calendar.YEAR) + "-" + (mCalendar.get(Calendar.MONTH)+1) + "-" + mCalendar.get(Calendar.DAY_OF_MONTH) + " 08");
                        }
                        c.setDayend(mCalendar.get(Calendar.YEAR) + "-" + (mCalendar.get(Calendar.MONTH)+1) + "-" + mCalendar.get(Calendar.DAY_OF_MONTH));
                        Log.d("day",mCalendar.get(Calendar.YEAR) + "-" + (mCalendar.get(Calendar.MONTH)+1) + "-" + mCalendar.get(Calendar.DAY_OF_MONTH));
                        mHelper.updateCow(c);
                        mCalendar = Calendar.getInstance();
                        Activity_1_2 fragment = new Activity_1_2();
                        Bundle args = new Bundle();
                        args.putString("ID",id);
                        fragment.setArguments(args);
                        android.support.v4.app.FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.replace(R.id.root_frame,fragment);
                        trans.commit();

                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

            }
        });
    }
}
