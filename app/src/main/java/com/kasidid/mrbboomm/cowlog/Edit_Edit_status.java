package com.kasidid.mrbboomm.cowlog;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/19/2016.
 */
public class Edit_Edit_status extends AppCompatActivity implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{
    private int ID = -1;
    COWHelper mHelper;
    CALVINGHelper cHelper = new CALVINGHelper(this);


    private ProgressDialog mProgressDialog;
    String id;
    int daystart_request= 456464,request_datetime=54564646;
    String status="",username,real_daystart="";
    EditText daystart,times,couple;
    Cow c;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");
        setContentView(R.layout.edit_edit_status);
        mHelper = new COWHelper(this);
        Intent intent = getIntent();
        id = intent.getStringExtra("ID");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_edit);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        daystart = (EditText) findViewById(R.id.daystart);
        times = (EditText) findViewById(R.id.times);
        couple = (EditText) findViewById(R.id.couple);


        c = mHelper.getCowByID(id);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("แก้ไขสถานะปัจจุบัน");
        //initiate

        ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
        ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
        ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
        daystart.setEnabled(false);
        times.setEnabled(false);
        couple.setEnabled(false);
        status = "";
        daystart.setText("");

        final TextView status_choice = (TextView) findViewById(R.id.status);
        status_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(Edit_Edit_status.this);
                builder.setItems(new String[] {"รอผสมพันธุ์","ผสมพันธุ์แล้ว"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
                                ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
                                ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
                                daystart.setEnabled(false);
                                times.setEnabled(false);
                                couple.setEnabled(false);
                                status = "0";
                                daystart.setText("");
                                status_choice.setText("รอผสมพันธุ์");
                                break;
                            case 1:
                                daystart.setEnabled(true);
                                times.setEnabled(true);
                                couple.setEnabled(true);
                                ((TextView) findViewById(R.id.couple_)).setTextColor(Color.BLACK);
                                ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.BLACK);
                                ((TextView) findViewById(R.id.times_)).setTextColor(Color.BLACK);
                                daystart.setHint("วันที่ผสม");

                                status = "2";
                                status_choice.setText("ผสมพันธุ์แล้ว");
                                break;

                        }
                    }
                });
                builder.create();
                builder.show();
            }
        });
        daystart.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = daystart_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Edit_status.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        //======================================================================
        ImageView imageView = (ImageView)findViewById(R.id.check);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editclick();
            }
        });

    }
    @Override
    public void onDateSet(com.layernet.thaidatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        if (request_datetime == daystart_request){
            real_daystart = date;
            daystart.setText(c.getDayTH(date));
        }
    }

    public void editclick(){
        if(status.equals("")){
            new AlertDialog.Builder(Edit_Edit_status.this)
                    .setMessage("กรุณาเลือกสถานะ")
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }

        if(status.equals("2") && (couple.getText().toString().equals("") || times.getText().toString().equals("") || daystart.getText().toString().equals("") )){
            new AlertDialog.Builder(Edit_Edit_status.this)
                    .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }

        if (status.equals("2")){
            c.setStatus(status);
            c.setDaystart(real_daystart);

//                    ##############################################################################################
            CBreed cBreed = new CBreed();
            cBreed.setCowname(c.getCowname());
            cBreed.setUsername(username);
            cBreed.setTimes(times.getText().toString());
            cBreed.setPapa(couple.getText().toString());
            cBreed.setBreeddate(real_daystart);
            cBreed.setParity(String.valueOf(Integer.parseInt(c.getParity())+1));
            new CBREEDHelper(Edit_Edit_status.this).addCBreed(cBreed);
            new Sync_CBreed(cBreed).execute();
//                    ###############################################################################################
//                    ###############################################################################################
        }
        else if(status.equals("0")){
            c.setStatus(status);
            c.setDaystart(Calendar.getInstance().get(Calendar.YEAR)+"-"+(Calendar.getInstance().get(Calendar.MONTH)+1)+"-"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        }
        if (!(c.getStatus().equals("3") && c.getState().equals("1"))) c.setState("0");
//        c.getEndDate();
        if (ID == -1) {
            mHelper.updateCow(mHelper.StateUpdate(c));
        } else {
            c.setId(ID);
        }
        new Sync_DB(c).execute();
        finish();

    }


}


