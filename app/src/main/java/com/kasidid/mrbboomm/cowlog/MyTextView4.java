package com.kasidid.mrbboomm.cowlog;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Kasidid on 3/1/2017.
 */

public class MyTextView4 extends TextView {

    public MyTextView4(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextView4(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView4(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun Bold.ttf");
        setTypeface(tf);
    }


}