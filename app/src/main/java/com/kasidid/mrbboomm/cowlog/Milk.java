package com.kasidid.mrbboomm.cowlog;

import android.provider.BaseColumns;

/**
 * Created by mrbboomm on 9/5/2016.
 */
public class Milk {
    private int Id;
    private String Username;
    private String Milkdate;
    private String Cowname;
    private String Volume;
    private String Fat;
    private String Protien;
    private String Lactos;
    private String Solid;
    private String Solidmilk;
    private String Calvdate;
    private String parity;
    public static final String TABLE  = "milk";

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String COWNAME = "cowname";
        public static final String USERNAME = "username";
        public static final String MILKDATE = "milkdate";
        public static final String VOLUME = "volume";
        public static final String FAT = "fat";
        public static final String PROTEIN = "protein";
        public static final String LACTOS = "lactos";
        public static final String SOLID = "solid";
        public static final String SOLIDMILK = "solidmilk";
        public static final String CALVDATE = "calvdate";
        public static final String PARITY = "parity";
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getCowname() {
        return Cowname;
    }

    public void setCowname(String cowname) {
        Cowname = cowname;
    }

    public String getVolume() {
        return Volume;
    }

    public void setVolume(String volume) {
        Volume = volume;
    }

    public String getProtien() {
        return Protien;
    }

    public String getFat() {
        return Fat;
    }

    public void setFat(String fat) {
        Fat = fat;
    }

    public void setProtien(String protien) {
        Protien = protien;
    }

    public String getLactos() {
        return Lactos;
    }

    public void setLactos(String lactos) {
        Lactos = lactos;
    }

    public String getSolid() {
        return Solid;
    }

    public void setSolid(String solid) {
        Solid = solid;
    }

    public String getSolidmilk() {
        return Solidmilk;
    }

    public void setSolidmilk(String solidmilk) {
        Solidmilk = solidmilk;
    }

    public String getMilkdate() {
        return Milkdate;
    }

    public void setMilkdate(String milkdate) {
        Milkdate = milkdate;
    }

    public String getCalvdate() {
        return Calvdate;
    }

    public void setCalvdate(String calvdate) {
        Calvdate = calvdate;
    }

    public String getParity() {
        return parity;
    }

    public void setParity(String parity) {
        this.parity = parity;
    }
}
