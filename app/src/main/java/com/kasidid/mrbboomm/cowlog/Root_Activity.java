package com.kasidid.mrbboomm.cowlog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mrbboomm on 10/4/2016.
 */
public class Root_Activity extends Fragment {

    private static final String TAG = "RootFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		/* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.root_activity, container, false);
        Bundle bundle = this.getArguments();
        final String  id = bundle.getString("ID");
        Fragment fragment = null;
        COWHelper mHelper = new COWHelper(getActivity());
        Cow c = mHelper.getCowByID(id);
        switch (c.getStatus()){
            case "0":
                fragment = new Activity_0_1();
                break;
            case "1":
                Calendar mCalendar = Calendar.getInstance();
                DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH");

                Calendar starta = Calendar.getInstance();
                Calendar enda = Calendar.getInstance();
                try {
                    starta.setTime(df2.parse(c.getState()));
                    starta.add(Calendar.HOUR,-1);
                    enda.setTime(df2.parse(c.getState()));
                    enda.add(Calendar.HOUR,5);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(c.getState().equals("0"))
                    fragment = new Activity_1_1();
                else if(mCalendar.after(starta)){
                    fragment = new Activity_1_3();
                }
                else{
                    fragment = new Activity_1_2();                            }
                break;
            case "2":
                if(c.getState().equals("1") || c.getState().equals("2") ||c.getState().equals("3"))
                    fragment = new Activity_2_2();
                else
                    fragment = new Activity_2_1();;
                break;
            case "3":
                if (c.getState().equals("0"))
                    fragment = new Activity_3_1();
                else if(c.getState().equals("1"))
                    fragment = new Activity_3_2();
                else
                    fragment = new Activity_3_3();
                break;
            case "4":
                fragment = new Activity_4_1();
                break;
            case "5":
                fragment = new Activity_5_1();
                break;
            case "6":
                fragment = new Activity_6_1();
                break;

        }
        Bundle args = new Bundle();
        args.putString("ID",id);
        fragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
		/*
		 * When this container fragment is created, we fill it with our first
		 * "real" fragment
		 */
        transaction.replace(R.id.root_frame, fragment);

        transaction.commit();


        return view;
    }

}
