package com.kasidid.mrbboomm.cowlog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.telecom.Call;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kasidid on 3/6/2017.
 */

public class CALVINGHelper extends SQLiteOpenHelper {
    public CALVINGHelper(Context context) {
        super(context, "calving.db", null, 5);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CALVING_TABLE = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT,%s TEXT , UNIQUE(%s,%s) ON CONFLICT REPLACE)",
                Calving.TABLE,
                Calving.Column.ID,
                Calving.Column.USERNAME,
                Calving.Column.COWNAME,
                Calving.Column.PARITY,
                Calving.Column.CALVINGDATE,
                Calving.Column.RESTDAY,
                Calving.Column.KID,
                Calving.Column.COWNAME,Calving.Column.PARITY);

        db.execSQL(CREATE_CALVING_TABLE);
    }

    private ContentValues getValues(Calving calving) {
        ContentValues values = new ContentValues();
        values.put(Calving.Column.USERNAME, calving.getUsername());
        values.put(Calving.Column.COWNAME, calving.getCowname());
        values.put(Calving.Column.PARITY, calving.getParity());
        values.put(Calving.Column.CALVINGDATE, calving.getCalvingdate());
        values.put(Calving.Column.KID,calving.getKid());
        values.put(Calving.Column.RESTDAY, calving.getRestday());
        return values;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP_CALVING_TABLE = "DROP TABLE IF EXISTS calving";
        db.execSQL(DROP_CALVING_TABLE);
        onCreate(db);
    }

    public void addCalving(Calving calving) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = getValues(calving);
        db.insert(Calving.TABLE, null, values);
        db.close();
    }
    public void deleteCalving(String cowname) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Calving.TABLE, Calving.Column.COWNAME + "='" + cowname+"'", null);
        db.close();
    }
    public void clearCalving(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Calving.TABLE, null, null);
        db.close();
    }
    public List<String> getReport(String cowname,String parity){
        List<String> calving = new ArrayList<String>();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        Cursor cursor = sqLiteDatabase.query
                (Calving.TABLE, null, Calving.Column.COWNAME+"=?",
                        new String[]{cowname}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while(!cursor.isAfterLast()) {
            try {
                if (cursor.getString(3).equals(parity)) {
                    calving.add(String.valueOf(cursor.getString(4)));
                    calving.add(String.valueOf(cursor.getString(6)));
                    calving.add(String.valueOf(cursor.getString(5)));

                    return calving;
                }
            }catch (Exception e){
                cursor.moveToNext();
                continue;
            }
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.close();
        calving.add("-");
        calving.add("-");
        calving.add("-");
        return calving;
    }
    public Calving getCalving(String cowname,String parity){
        Calving calving = new Calving();
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        Cursor cursor = sqLiteDatabase.query
                (Calving.TABLE, null, Calving.Column.COWNAME+"=?",
                        new String[]{cowname}, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while(!cursor.isAfterLast()) {
            try {
                if (cursor.getString(3).equals(parity)) {
                    calving.setId(cursor.getInt(0));
                    calving.setUsername(cursor.getString(1));
                    calving.setCowname(cursor.getString(2));
                    calving.setParity(cursor.getString(3));
                    calving.setCalvingdate(cursor.getString(4));
                    calving.setRestday(cursor.getString(5));
                    calving.setKid(cursor.getString(6));

                    return calving;
                }
            }catch (Exception e){
                cursor.moveToNext();
                continue;
            }

            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.close();

        return calving;
    }
    public void updateCalving(Calving calving) {

        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues values = getValues(calving);
        db.update(Calving.TABLE,
                values,
                Calving.Column.ID + " = ? ",
                new String[] { String.valueOf(calving.getId()), });

        db.close();
    }
}