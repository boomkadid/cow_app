package com.kasidid.mrbboomm.cowlog;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 1/4/2017.
 */

public class Sync_Calving extends AsyncTask<String, Void, String> {
    private Calving calving;

    public Sync_Calving(Calving calving) {
        this.calving = calving;
    }

    @Override
    protected String doInBackground(String... strings) {
        String link;
        String data;
        try {
            data = "?id=" + URLEncoder.encode(String.valueOf(calving.getId()), "UTF-8");
            data += "&username=" + URLEncoder.encode(calving.getUsername(), "UTF-8");
            data += "&cowname=" + URLEncoder.encode(calving.getCowname(), "UTF-8");
            data += "&parity=" + URLEncoder.encode(calving.getParity(), "UTF-8");
            data += "&calvingdate=" + URLEncoder.encode(calving.getCalvingdate(), "UTF-8");
            data += "&restday=" + URLEncoder.encode(calving.getRestday(), "UTF-8");
            data += "&kid=" + URLEncoder.encode(calving.getKid(), "UTF-8");
            link = "https://info.rdi.ku.ac.th/cowapp/manager/updatecalving.php" + data;
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(link).build();
            Response response = client.newCall(request).execute();
            Log.d("link", link);
            return response.body().string();


        } catch (Exception e) {
            e.printStackTrace();
            Log.d("link", "fail");
            return new String("Exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String result) {
        String jsonStr = result;
        if (jsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                String query_result = jsonObj.getString("query_result");
                if (query_result.equals("SUCCESS")) {
                    Log.d("result_sync", "SUCESS");
                } else if (query_result.equals("FAILURE")) {
                    Log.d("result_sync", "FAILURE");
                } else {

                    Log.d("result_sync", "Couldn't connect");
                }
            } catch (JSONException e) {
                Log.d("result_sync", result);
            }
        } else {

            Log.d("result_sync", "couldn't get JSON data");
        }
    }

}