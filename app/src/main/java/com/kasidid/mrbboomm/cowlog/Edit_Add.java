package com.kasidid.mrbboomm.cowlog;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.EachExceptionsHandler;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.kosalgeek.android.photoutil.ImageLoader;

import android.Manifest;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/19/2016.
 */
public class Edit_Add extends AppCompatActivity implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{
    private final String TAG = this.getClass().getName();
    private int ID = -1;
    COWHelper mHelper;
    String username,type,status="";
    private Calendar today;
    ImageView ivCamera, ivGallery,imageView;
    EditText etcalvingdate;
    String real_birth,real_calvingdate,real_daystart="";
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;
    String selectedPhoto;
    EditText etCowName,etNickName,etFraction,etBirth,etpapaf,etmama,etmamaf,etparity,daystart,times,couple;
    Button next_page1,next_page2,previous_page1,previous_page2,previous_page3;
    final int MY_PERMISSIONS_REQUEST_CAMERA = 33333,date_request = 154564,calving_request = 78964156,GALLERY_REQUEST = 22131,CAMERA_REQUEST = 13323,daystart_request=487035;
    int request_datetime=00000;
    AutoCompleteTextView etpapa;
    Typeface tf;
    Bitmap bitbit;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private ProgressDialog mProgressDialog;

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("คุณต้องการออกจากหน้านี้?")
                .setNegativeButton("ยกเลิก", null)
                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                }).create().show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_add);
        String fontPath = "fonts/THSarabun.ttf";
        tf = Typeface.createFromAsset(getAssets(),fontPath);
        mHelper = new COWHelper(this);
                Intent intent = getIntent();
        type = intent.getStringExtra("type");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_add);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        findViewById(R.id.page2).setVisibility(View.GONE);

        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("เพิ่มข้อมูลโครีด");
        if (type.equals("0")){
            mTitle.setText("เพิ่มข้อมูลโคทดแทน");
        }
        if (type.equals("2")){
            findViewById(R.id.status).setVisibility(View.GONE);
            findViewById(R.id.status_).setVisibility(View.GONE);

            findViewById(R.id.times).setVisibility(View.GONE);
            findViewById(R.id.times_).setVisibility(View.GONE);


            findViewById(R.id.daystart).setVisibility(View.GONE);
            findViewById(R.id.daystart_).setVisibility(View.GONE);

            findViewById(R.id.couple).setVisibility(View.GONE);
            findViewById(R.id.couple_).setVisibility(View.GONE);
            mTitle.setText("เพิ่มข้อมูลโคเพศผู้");
        }

        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(this);
        username =  prefs.getString("username","not found");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(Edit_Add.this)
                        .setMessage("คุณต้องการออกจากหน้านี้?")
                        .setNegativeButton("ยกเลิก", null)
                        .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).create().show();
            }
        });

        etCowName = (EditText) findViewById(R.id.cow_name);
        etNickName = (EditText) findViewById(R.id.cow_nickname);
        etFraction = (EditText) findViewById(R.id.cow_detail);
        etBirth = (EditText) findViewById(R.id.txtdate);
        daystart = (EditText) findViewById(R.id.daystart);
        times = (EditText) findViewById(R.id.times);
        couple = (EditText) findViewById(R.id.couple);
        //////////////////////auto complete
        etpapa = (AutoCompleteTextView) findViewById(R.id.cow_papa);
        List<String> papas = mHelper.getCowSearchArray(username,10,0,false);
        papas.addAll(mHelper.getCowArray(username,10,1,false));
        Set<String> hs = new HashSet<>();
        hs.addAll(papas);
        papas.clear();
        papas.addAll(hs);
        etpapa.setTypeface(tf);
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,papas );
        etpapa.setAdapter(adapter);

        ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
        ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
        ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
        daystart.setEnabled(false);
        times.setEnabled(false);
        couple.setEnabled(false);
        status = "";
        daystart.setText("");
        final TextView status_choice = (TextView) findViewById(R.id.status);
        status_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(Edit_Add.this);
                builder.setItems(new String[] {"รอผสมพันธุ์","ผสมพันธุ์แล้ว"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                ((TextView) findViewById(R.id.couple_)).setTextColor(Color.GRAY);
                                ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.GRAY);
                                ((TextView) findViewById(R.id.times_)).setTextColor(Color.GRAY);
                                daystart.setEnabled(false);
                                times.setEnabled(false);
                                couple.setEnabled(false);
//                                question.setEnabled(false);
                                status = "0";
                                daystart.setText("");
                                status_choice.setText("รอผสมพันธุ์");
                                break;
                            case 1:
                                daystart.setEnabled(true);
                                times.setEnabled(true);
                                couple.setEnabled(true);
                                ((TextView) findViewById(R.id.couple_)).setTextColor(Color.BLACK);
                                ((TextView) findViewById(R.id.daystart_)).setTextColor(Color.BLACK);
                                ((TextView) findViewById(R.id.times_)).setTextColor(Color.BLACK);
                                daystart.setHint("วันที่ผสม");

                                status = "2";
                                status_choice.setText("ผสมพันธุ์แล้ว");
                                break;

                        }
                    }
                });
                builder.create();
                builder.show();
            }
        });
        daystart.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = daystart_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Add.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        //////////////////////////////////////////
        etpapaf = (EditText) findViewById(R.id.cow_papaf);
        etmama = (EditText) findViewById(R.id.cow_mama);
        etmamaf = (EditText) findViewById(R.id.cow_mamaf);
         etparity = (EditText) findViewById(R.id.parity);
        etcalvingdate = (EditText) findViewById(R.id.calvingdate);

        ivCamera = (ImageView)findViewById(R.id.ivCamera);
        ivGallery = (ImageView)findViewById(R.id.ivGallery);
        imageView = (ImageView) findViewById(R.id.image_cow);
        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());

        //=====================================================================
        page_handle();

        etcalvingdate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = calving_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Add.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                );
                dpd.setAccentColor("#6388ad");
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }}
        });
        etBirth.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    request_datetime = date_request;
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Edit_Add.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(Edit_Add.this,
                        new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_CAMERA);
            }
        });
        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
            }
        });
        Button bDone = (Button) findViewById(R.id.cowadd_button);
        bDone.setTypeface(tf);
        today = Calendar.getInstance();
    }

    private void page_handle() {
        //////////////////////////////////////////////////////////////////////
        next_page1 = (Button)findViewById(R.id.next_page1);
        previous_page1 = (Button)findViewById(R.id.previous_page1);
        next_page2 = (Button)findViewById(R.id.next_page2);
        previous_page2 = (Button)findViewById(R.id.previous_page2);
        previous_page3 = (Button)findViewById(R.id.previous_page3);
        next_page1.setTypeface(tf);next_page2.setTypeface(tf);
        previous_page1.setTypeface(tf);previous_page2.setTypeface(tf);previous_page3.setTypeface(tf);
        next_page1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etCowName.getText().toString().equals("") || etBirth.getText().toString().equals("")){
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                List<String> cows_id = mHelper.getCowArray(username,2,1,true);
                cows_id.addAll(mHelper.getCowArray(username,2,0,true));
                if(cows_id.contains(etCowName.getText().toString())){
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("หมายเลขโค "+etCowName.getText().toString()+" ถูกใช้งานไปแล้ว")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                if (type.equals("0") || type.equals("2")){
                    findViewById(R.id.page1).setVisibility(View.GONE);

                    findViewById(R.id.page3).setVisibility(View.VISIBLE);
                }
                else {
                    findViewById(R.id.page1).setVisibility(View.GONE);
                    findViewById(R.id.page2).setVisibility(View.VISIBLE);
                }
            }
        });
        previous_page1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        next_page2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.page2).setVisibility(View.GONE);
                findViewById(R.id.page3).setVisibility(View.VISIBLE);
            }
        });
        previous_page2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.page1).setVisibility(View.VISIBLE);
                findViewById(R.id.page2).setVisibility(View.GONE);
            }
        });
        next_page2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((etparity.getText().toString().equals("") || etcalvingdate.getText().toString().equals("")) && type.equals("1"))){
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                Calendar day_a = Calendar.getInstance();
                try {
                    day_a.setTime(df.parse(real_calvingdate));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (today.getTimeInMillis()-day_a.getTimeInMillis()<0){
                    Cow cow = new Cow();
                    new AlertDialog.Builder(Edit_Add.this)
                            .setMessage("ไม่สามารถกรอกวันคลอดหลังจากวันที่ "+ cow.getDayTH((today.get(Calendar.YEAR)+"-"+(today.get(Calendar.MONTH)+1)+"-"+today.get(Calendar.DAY_OF_MONTH))))
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                findViewById(R.id.page2).setVisibility(View.GONE);
                findViewById(R.id.page3).setVisibility(View.VISIBLE);
            }
        });
        previous_page3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type.equals("0") || type.equals("2")) {
                    findViewById(R.id.page1).setVisibility(View.VISIBLE);
                    findViewById(R.id.page3).setVisibility(View.GONE);
                }
                else{
                    findViewById(R.id.page2 ).setVisibility(View.VISIBLE);
                    findViewById(R.id.page3).setVisibility(View.GONE);
                }
            }
        });
        ///////////////////////////////////////////////////////////////////////////
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode == CAMERA_REQUEST){
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                bitbit = photo;
                imageView.setImageBitmap(photo);

            }
            else if(requestCode == GALLERY_REQUEST){
                Uri uri = data.getData();
                galleryPhoto.setPhotoUri(uri);
                String photoPath = galleryPhoto.getPath();
                selectedPhoto = photoPath;
                Bitmap bitmap = null;

                try {
                     bitmap = ImageLoader.init().from(selectedPhoto).requestSize(1024,1024).getBitmap();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitbit = bitmap;
                imageView.setImageBitmap(bitmap);
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                Log.i("Camera", "G : " + grantResults[0]);
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    openCamera();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

                        showAlert();

                    } else {
                    }
                }
                return;
            }

        }
    }
    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(Edit_Add.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(Edit_Add.this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);

                    }
                });
        alertDialog.show();
    }
    private void openCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }
    @Override
    public void onDateSet(com.layernet.thaidatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        if (request_datetime == date_request){
            real_birth = date;
            etBirth.setText(new Cow().getDayTH(date));
        }
        if (request_datetime == calving_request){
            real_calvingdate = date;
            etcalvingdate.setText(new Cow().getDayTH(date));
        }
        if (request_datetime == daystart_request){
            real_daystart = date;
            daystart.setText(new Cow().getDayTH(date));
        }
        Log.d("code", String.valueOf(request_datetime));
    }
    public void addclick(View view){
        if(!type.equals("2") && status.equals("")){
            new AlertDialog.Builder(Edit_Add.this)
                    .setMessage("กรุณาเลือกสถานะ")
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }


        if(!type.equals("2") && status.equals("2") && (daystart.getText().toString().equals("") || couple.getText().toString().equals("") || times.getText().toString().equals(""))){
            new AlertDialog.Builder(Edit_Add.this)
                    .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }
        Calendar day_a = Calendar.getInstance();
        try {
            day_a.setTime(df.parse(real_daystart));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (today.getTimeInMillis()-day_a.getTimeInMillis()<0 && status.equals("2")){
            Cow cow = new Cow();
            new AlertDialog.Builder(Edit_Add.this)
                    .setMessage("ไม่สามารถกรอกวันผสมหลังจากวันที่ "+ cow.getDayTH((today.get(Calendar.YEAR)+"-"+(today.get(Calendar.MONTH)+1)+"-"+today.get(Calendar.DAY_OF_MONTH))))
                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).create().show();
            return;
        }
        AlertDialog.Builder builder =
                new AlertDialog.Builder(Edit_Add.this);
        builder.setMessage("ต้องการเพิ่มข้อมูลของโค "+etNickName.getText().toString()+" ?");
        builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Cow cow = new Cow();
                cow.setCowname(etCowName.getText().toString());
                if (etNickName.getText().toString().equals("")) cow.setNickname(etCowName.getText().toString());
                else cow.setNickname(etNickName.getText().toString());
                cow.setFraction(etFraction.getText().toString());
                cow.setBirth(real_birth);
                cow.setUsername(username);
                cow.setStatus(status);
                cow.setPapa(etpapa.getText().toString());
                cow.setPapaf(etpapaf.getText().toString());
                cow.setMama(etmama.getText().toString());
                cow.setMamaf(etmamaf.getText().toString());
                cow.setType(type.toString());
                cow.setState("0");
                if (type.equals("0")){
                    cow.setParity("0");
                    cow.setCalvdate("-");

                }
                else if(type.equals("2")) {
                    cow.setStatus("2");
                    cow.setParity("0");
                    cow.setCalvdate("-");
                }
                else{
                    cow.setParity(etparity.getText().toString());
                    cow.setCalvdate(real_calvingdate);
                    if (!cow.getParity().equals("0")){
                        Calving calving = new Calving();
                        calving.setUsername(cow.getUsername());
                        calving.setCowname(cow.getCowname());
                        calving.setParity(cow.getParity());
                        calving.setCalvingdate(cow.getCalvdate());
                        calving.setRestday("-");
                        calving.setKid("-");
                        CALVINGHelper calvingHelper = new CALVINGHelper(Edit_Add.this);
                        calvingHelper.addCalving(calving);
                        new Sync_Calving(calving).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }

                if (bitbit != null){
                    String encodedImage = ImageBase64.encode(bitbit);

                    HashMap<String, String> postData = new HashMap<String, String>();
                    postData.put("image", encodedImage);
                    postData.put("name", etCowName.getText().toString());
                    postData.put("username", username);
                    new pushbyPost(Edit_Add.this,"https://info.rdi.ku.ac.th/cowapp/manager/uploadImage2.php",postData).execute();
                }
                if (status.equals("2")){
                    cow.setDaystart(real_daystart);
                    CBreed cBreed = new CBreed();
                    cBreed.setCowname(cow.getCowname());
                    cBreed.setUsername(username);
                    cBreed.setTimes(times.getText().toString());
                    cBreed.setPapa(couple.getText().toString());
                    cBreed.setBreeddate(real_daystart);
                    cBreed.setParity(String.valueOf(Integer.parseInt(cow.getParity())+1));
                    new CBREEDHelper(Edit_Add.this).addCBreed(cBreed);
                    new Sync_CBreed(cBreed).execute();

                }
                else{
                    cow.setDaystart(today.get(Calendar.YEAR)+"-"+(today.get(Calendar.MONTH)+1)+"-"+today.get(Calendar.DAY_OF_MONTH));
                }
                if (ID == -1) {
                    mHelper.addCow(mHelper.StateUpdate(cow));

                } else {
                    cow.setId(ID);
                }
                new Sync_DB(cow).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);;
                finish();
            }
        });
        builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}


