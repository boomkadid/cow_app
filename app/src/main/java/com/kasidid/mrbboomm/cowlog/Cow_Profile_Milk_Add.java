package com.kasidid.mrbboomm.cowlog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 2/28/2017.
 */

public class Cow_Profile_Milk_Add extends AppCompatActivity implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener {
    EditText volume,fat,protein,lactos,solid,solidmilk,milkdate;
    String real_milkdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        setContentView(R.layout.activity_milk2_f);
        final Intent intent = getIntent();
        final String id = intent.getStringExtra("ID");
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_add);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView mTitle = (TextView) findViewById(R.id.toolbar_title);
        mTitle.setTypeface(Typeface.createFromAsset(getAssets(),
                "fonts/THSarabun.ttf"));
        mTitle.setText("เพิ่มผลผลิตน้ำนม");
        final ImageView check = (ImageView) findViewById(R.id.check);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        COWHelper mHelper = new COWHelper(this);
        final Cow c = mHelper.getCowByID(id);
        TextView calvingdate = (TextView)findViewById(R.id.calvingdate);
        try {
            calvingdate.setText(c.getDayTH(new CALVINGHelper(this).getReport(c.getCowname(),c.getParity()).get(0)));
        }catch (Exception e){
            calvingdate.setText(c.getCalvdate());
        }
        volume = (EditText)findViewById(R.id.volume);
        fat = (EditText)findViewById(R.id.fat);
        protein = (EditText)findViewById(R.id.protein);
        lactos = (EditText)findViewById(R.id.lactos);
        solid = (EditText)findViewById(R.id.solid);
        solidmilk = (EditText)findViewById(R.id.solidmilk);
        milkdate = (EditText)findViewById(R.id.milkdate);
        milkdate.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    Calendar now = Calendar.getInstance();

                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Cow_Profile_Milk_Add.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getFragmentManager(), "Datepickerdialog");
                }
            }
        });

        final Calendar milkdate_cal = Calendar.getInstance();
        final Calendar today = Calendar.getInstance();
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(milkdate.getText().toString().equals("") || volume.getText().toString().equals("")){
                    new AlertDialog.Builder(Cow_Profile_Milk_Add.this)
                            .setMessage("กรุณากรอกข้อมูลที่ข้างหน้ามีเครื่องหมาย *")
                            .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                    return;
                }
                try {
                    milkdate_cal.setTime(df.parse(real_milkdate));
                    if(milkdate_cal.getTimeInMillis() > today.getTimeInMillis()){
                        new AlertDialog.Builder(Cow_Profile_Milk_Add.this)
                                .setMessage("ไม่สามารถใส่วันล่วงหน้าได้")
                                .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        dialogInterface.dismiss();
                                    }
                                }).create().show();
                        return;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(check.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);
                final AlertDialog.Builder builder =
                        new AlertDialog.Builder(Cow_Profile_Milk_Add.this);
                builder.setMessage("ยืนยันการให้นม?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {

                        Calendar cal = Calendar.getInstance();
                        Milk m = new Milk();
                        m.setUsername(c.getUsername());
                        m.setCowname(c.getCowname());
                        if (real_milkdate.equals("")) m.setMilkdate(cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DAY_OF_MONTH));
                        else m.setMilkdate(real_milkdate);
                        m.setVolume(volume.getText().toString());
                        m.setFat(fat.getText().toString());
                        m.setProtien(protein.getText().toString());
                        m.setLactos(lactos.getText().toString());
                        m.setSolid(solid.getText().toString());
                        m.setSolidmilk(solidmilk.getText().toString());
                        m.setCalvdate(c.getCalvdate());
                        m.setParity(c.getParity());
                        MilkHelper milkHelper = new MilkHelper(Cow_Profile_Milk_Add.this);
                        milkHelper.plusMilk(m);
                        new Sync_DB(m).execute();
                        finish();

                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });

    }

    @Override
    public void onDateSet(com.layernet.thaidatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        real_milkdate = date;
        milkdate.setText(new Cow().getDayTH(date));
    }

    public class Sync_DB extends AsyncTask<String, Void, String> {
        private Milk milk;

        public Sync_DB(Milk m) {
            this.milk = m;
        }

        @Override
        protected String doInBackground(String... strings) {
            String link;
            String data;
            try {
                data = "?id=" + URLEncoder.encode(String.valueOf(milk.getId()), "UTF-8");
                data += "&cowname=" + URLEncoder.encode(milk.getCowname(), "UTF-8");
                data += "&username=" + URLEncoder.encode(milk.getUsername(), "UTF-8");
                data += "&milkdate=" + URLEncoder.encode(milk.getMilkdate(), "UTF-8");
                data += "&volume=" + URLEncoder.encode(milk.getVolume(), "UTF-8");
                data += "&fat=" + URLEncoder.encode(milk.getFat(), "UTF-8");
                data += "&protien=" + URLEncoder.encode(milk.getProtien(), "UTF-8");
                data += "&lactos=" + URLEncoder.encode(milk.getLactos(), "UTF-8");
                data += "&solid=" + URLEncoder.encode(milk.getSolid(), "UTF-8");
                data += "&solidmilk=" + URLEncoder.encode(milk.getSolidmilk(), "UTF-8");

                data += "&calvdate=" + URLEncoder.encode(milk.getCalvdate(), "UTF-8");
                data += "&parity=" + URLEncoder.encode(milk.getParity(), "UTF-8");
                link = "https://info.rdi.ku.ac.th/cowapp/manager/updatemilk.php" + data;

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder().url(link).build();
                Response response = client.newCall(request).execute();
                Log.d("link_milk", link);
                return response.body().string();

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("link", "FAIL sync");
                return new String("Exception: " + e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String result) {
            String jsonStr = result;
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    String query_result = jsonObj.getString("query_result");
                    if (query_result.equals("SUCCESS")) {
                        Log.d("result_sync", "SUCESS");
                    } else if (query_result.equals("FAILURE")) {
                        Log.d("result_sync", "FAILURE");
                    } else {

                        Log.d("result_sync", "Couldn't connect");
                    }
                } catch (JSONException e) {
                    Log.d("result_sync", result);
                }
            } else {
                Log.d("result_sync", "couldn't get JSON data");
            }
        }

    }

}
