package com.kasidid.mrbboomm.cowlog;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Kasidid on 10/14/2016.
 */

public class Farm_Status extends ActionBarActivity {
    COWHelper mHelper;
    Button reportFarm,editFarm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.farm_status);
        Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("สถานะฟาร์มปัจจุบัน");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ArrayList<String> status = (ArrayList<String>) getIntent().getSerializableExtra("status");
        TextView cows_header = (TextView)findViewById(R.id.cows_header);
        TextView cows = (TextView)findViewById(R.id.cows);
        TextView milks_header =(TextView)findViewById(R.id.milks_header);
        TextView milks =(TextView)findViewById(R.id.milks);
        TextView milks305_header =(TextView)findViewById(R.id.milks305_header);
        TextView milks305 =(TextView)findViewById(R.id.milks305);
        TextView frets =(TextView)findViewById(R.id.fret);

        cows_header.setText(status.get(0));
        cows.setText(status.get(1));
        milks305_header.setText(status.get(2));
        milks305.setText(status.get(3));
        milks_header.setText(status.get(4));
        milks.setText(status.get(5));
        frets.setText(status.get(6));
    }

//        float[] fret = new float[]{0,0,0,0};
//        float[] countfret = new float[]{0,0,0,0};
//        CALVINGHelper calvingHelper = new CALVINGHelper(getActivity());
//        List<String> ids = mHelper.getCowArray(username,0,1,false);
//        ids.addAll(mHelper.getCowArray(username,0,0,false));
//        Calving calving ;
//        Cow cow;
//
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        for (int i = 0;i<ids.size();i++){
//            cow = mHelper.getCowByID(ids.get(i));
//            String last_calveday = "";
//            try{//อายุเมื่อคลอดลูกครั้งแรกเฉลี่ย
//                calving = calvingHelper.getCalving(cow.getCowname(), "1");
//                Calendar calv_1 = Calendar.getInstance();
//                Calendar birth = Calendar.getInstance();
//                calv_1.setTime(df.parse(calving.getCalvingdate()));
//                birth.setTime(df.parse(cow.getBirth()));
//                int diffYear = calv_1.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
//                int diffMonth =  calv_1.get(Calendar.MONTH) - birth.get(Calendar.MONTH);
//                if (diffMonth <0) diffMonth += 12;
//                fret[1] += Math.abs(diffYear * 12 +diffMonth);
//                countfret[1] += 1;
//            }catch (Exception e){}
//
//            for (int j = 0;j< Integer.parseInt(cow.getParity())+1;j++) {//ช่วงห่างการให้ลูกเฉลี่ย
//                String tmp_parity = String.valueOf(j+1);
//                calving = calvingHelper.getCalving(cow.getCowname(), tmp_parity);
//                try{ //ช่วงห่างการให้ลูกเฉลี่ย
//                    if (tmp_parity.equals("1")) {last_calveday = calving.getCalvingdate(); continue;}
//                    Calendar last_calvedate = Calendar.getInstance();
//                    Calendar calvedate = Calendar.getInstance();
//                    last_calvedate.setTime(df.parse(last_calveday));
//                    calvedate.setTime(df.parse(calving.getCalvingdate()));
//                    int diffYear = calvedate.get(Calendar.YEAR) - last_calvedate.get(Calendar.YEAR);
//                    int diffMonth =calvedate.get(Calendar.MONTH) - last_calvedate.get(Calendar.MONTH);
//                    if (diffMonth <0) diffMonth += 12;
//                    fret[2] += Math.abs(diffMonth+diffYear*12);
//                    last_calveday = calving.getCalvingdate();
//                    countfret[2] += 1;
//                }catch (Exception e){
//
//                }
//
//            }
//            Calendar breeddate = Calendar.getInstance();
//            Calendar calvedate = Calendar.getInstance();
//            Calendar st;
//            Calendar ed;
//            for (int j = 0;j<= Integer.parseInt(cow.getParity());j++) { //อัตราการผสมติด
//                String tmp_parity = String.valueOf(j+1);
//                st = Calendar.getInstance();
//                ed = Calendar.getInstance();
//                st.add(Calendar.DATE,-133);
//                ed.add(Calendar.DATE,-77);
//                Log.d("อัตราการผสมติด",cow.getParity());
//
//                try {
//                    Log.d("อัตราการผสมติด",cow.getNickname());
////                    List<String> b = new BREEDHelper(getActivity()).getIntervalBreed(cow.getCowname(),tmp_parity,st,ed);
//                    CBreed cBreed = new CBREEDHelper(getActivity()).getIntervalCBreed(cow.getCowname(),tmp_parity,st,ed);
//                    Log.d("อัตราการผสมติด","start"+st.get(Calendar.YEAR) + "-" + (st.get(Calendar.MONTH)+1) + "-" + st.get(Calendar.DAY_OF_MONTH));
//                    Log.d("อัตราการผสมติด","end"+ed.get(Calendar.YEAR) + "-" + (ed.get(Calendar.MONTH)+1) + "-" + ed.get(Calendar.DAY_OF_MONTH));
////                    breeddate.setTime(df.parse(b.get(2)));
//                    breeddate.setTime(df.parse(cBreed.getBreeddate()));
//                    Log.d("อัตราการผสมติด","btw"+breeddate.get(Calendar.YEAR) + "-" + (breeddate.get(Calendar.MONTH)+1) + "-" + breeddate.get(Calendar.DAY_OF_MONTH));
//
//                    if (breeddate.after(st) && breeddate.before(ed)){
//                        Log.d("อัตราการผสมติด","pass1");
////                        countfret[0] += Integer.parseInt(b.get(0));
//                        countfret[0] += Integer.parseInt(cBreed.getTimes());
//                        st.add(Calendar.DATE,+280);
//                        ed.add(Calendar.DATE,+280);
//
//                        if (cow.getStatus().equals("3")){
//                            Log.d("อัตราการผสมติด","pass2");
//                            calvedate.setTime(df.parse(cow.getDayend()));
//                            if(calvedate.after(st) && calvedate.before(ed)){
//                                Log.d("อัตราการผสมติด","pass3");
//                                fret[0] += 1;
//                            }
//                        }
//                        break;
//                    }
//
//                } catch (ParseException e) {
//                    Log.d("อัตราการผสมติด", String.valueOf(e));
//                }
//            }
//
//            for (int j = 0;j< Integer.parseInt(cow.getParity());j++) { //จำนวนวันท้องว่างเฉลี่ย
//                String tmp_parity = String.valueOf(j+1);
//                try {
////                    breeddate.setTime(df.parse(new BREEDHelper(getActivity()).getLastBreed(cow.getCowname(), String.valueOf(j+2)).get(2)));
//                    breeddate.setTime(df.parse(new CBREEDHelper(getActivity()).getCBreed(cow.getCowname(), String.valueOf(j+2)).getBreeddate()));
//                    try{
//                        calvedate.setTime(df.parse(calvingHelper.getCalving(cow.getCowname(),tmp_parity).getCalvingdate()));
//                    }catch (Exception e){
//                        continue;
//
//                    }
//                    int diffYear = breeddate.get(Calendar.YEAR) - calvedate.get(Calendar.YEAR);
//                    int diffMonth =breeddate.get(Calendar.MONTH) - calvedate.get(Calendar.MONTH);
//
//                    if (diffMonth <0) {
//                        diffMonth += 12;
//                        diffYear -= 1;
//                    }
//                    fret[3] += Math.abs(diffMonth+diffYear*12);
//                    countfret[3] += 1;
////                    Log.d("วันท้องว่าง", String.valueOf(new BREEDHelper(getActivity()).getLastBreed(cow.getCowname(), String.valueOf(j+2)).get(2)+ " - " + calvingHelper.getCalving(cow.getCowname(),tmp_parity).getCalvingdate()));
////                    Log.d("วันท้องว่าง", String.valueOf(Math.abs(diffMonth+diffYear*12)));
//                } catch (ParseException e) {
////                    Log.d("วันท้องว่าง", String.valueOf(e));
//                }
//
//
//            }
//        }
//        for (int i =0 ;i <fret.length;i++){
//            if (countfret[i] == 0) {
//                countfret[i] =1;
//                fret[i] = 0;
//            }
//        }
//        frets.setText("อัตราการผสมติด "+String.format("%.1f",(fret[0]*100/countfret[0]))+" %\n"+
//        "อายุเมื่อคลอดลูกครั้งแรกเฉลี่ย "+String.format("%.1f",(fret[1]/countfret[1]))+" เดือน\n"+
//        "ช่วงห่างการให้ลูกเฉลี่ย "+String.format("%.1f",(fret[2]/countfret[2]))+" เดือน\n"+
//        "จำนวนวันท้องว่างเฉลี่ย "+String.format("%.1f",(fret[3]/countfret[3])*30.0)+" วัน\n");
//        return view;
//    }


}
