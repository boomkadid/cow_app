package com.kasidid.mrbboomm.cowlog;


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mrbboomm on 8/23/2016.
 */
public class Activity_2_1 extends Fragment {
    TextView remain,Header;
    Button mConfirm;
    Calendar mCalendar;
    COWHelper mHelper;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_2_1, container, false);
        Bundle bundle = this.getArguments();
        final String  id = bundle.getString("ID");
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String username =  prefs.getString("username","not found");
        mHelper = new COWHelper(getActivity());
        final Cow cow = mHelper.getCowByID(id);
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            c2.setTime(df.parse(cow.getState()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int a = c1.get(Calendar.DAY_OF_YEAR);
        int b = c2.get(Calendar.DAY_OF_YEAR);
        int c;
        if (c2.get(Calendar.YEAR)!=c1.get(Calendar.YEAR) ) c = 365-a+b;
        else c = b-a;
        Header = (TextView)view.findViewById(R.id.textheader2_1);
        remain = (TextView)view.findViewById(R.id.text2_1);
        remain.setText("อีก "+c +" วันตรวจสอบการกลับสัด");
        Button submit = (Button)view.findViewById(R.id.submit_button_status2_1);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun.ttf");
        Header.setTypeface(tf);
        remain.setTypeface(tf);
        submit.setTypeface(tf);
        mCalendar = Calendar.getInstance();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage("พบอาการกลับสัด?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {
                        cow.setStatus("1");
                        cow.setDaystart(mCalendar.get(Calendar.YEAR)+"-"+(mCalendar.get(Calendar.MONTH)+1)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH));
                        if (mCalendar.get(Calendar.HOUR_OF_DAY) <= 12)
                            cow.setState(mCalendar.get(Calendar.YEAR)+"-"+(mCalendar.get(Calendar.MONTH)+1)+"-"+mCalendar.get(Calendar.DAY_OF_MONTH)+" 16");
                        else {
                            mCalendar.add(Calendar.DATE,1);
                            cow.setState(mCalendar.get(Calendar.YEAR) + "-" + (mCalendar.get(Calendar.MONTH)+1) + "-" + mCalendar.get(Calendar.DAY_OF_MONTH) + " 08");
                            mCalendar.add(Calendar.DATE,-1);
                        }
                        mHelper.updateCow(cow);
                        Activity_1_2 fragment = new Activity_1_2();
                        Bundle args = new Bundle();
                        args.putString("ID",id);
                        fragment.setArguments(args);
                        android.support.v4.app.FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.replace(R.id.root_frame,fragment);
                        trans.commit();
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

}
