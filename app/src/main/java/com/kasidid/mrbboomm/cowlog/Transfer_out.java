package com.kasidid.mrbboomm.cowlog;

import android.content.Context;
import android.util.Log;

/**
 * Created by Kasidid on 11/17/2017.
 */

public class Transfer_out {
    Transfer transfer;
    Context mContext;
    public Transfer_out(Context mContext_, Transfer transfer_) {
        this.transfer = transfer_;
        this.mContext = mContext_;
    }
    public void delete(){
        COWHelper cowHelper = new COWHelper(mContext);
        Log.d("delete for transfering out",transfer.getCowname()+"x"+transfer.getUsername());
        CBREEDHelper cbreedHelper = new CBREEDHelper(mContext);
        CALVINGHelper calvingHelper = new CALVINGHelper(mContext);
        MilkHelper milkHelper = new MilkHelper(mContext);
        cowHelper.deleteCowbyName(transfer.getCowname());
        cbreedHelper.deleteCBreed(transfer.getCowname());
        calvingHelper.deleteCalving(transfer.getCowname());
        milkHelper.deleteMilkbyName(transfer.getCowname());

    }


}
