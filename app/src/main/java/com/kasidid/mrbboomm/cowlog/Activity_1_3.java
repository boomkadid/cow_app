package com.kasidid.mrbboomm.cowlog;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.layernet.thaidatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.Calendar;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mrbboomm on 8/23/2016.
 */
public class Activity_1_3 extends Fragment implements com.layernet.thaidatetimepicker.date.DatePickerDialog.OnDateSetListener{
    COWHelper mHelper;
    EditText breeddate;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = inflater.inflate(R.layout.activity_1_3, container, false);
        Bundle bundle = this.getArguments();
        final String  id = bundle.getString("ID");
        SharedPreferences prefs =  PreferenceManager.getDefaultSharedPreferences(getActivity());
        final String username =  prefs.getString("username","not found");
        final EditText papa = (EditText)view.findViewById(R.id.couple_number_status1_3);
        final Button submit = (Button)view.findViewById(R.id.submit_button_status1_3);
        mHelper = new COWHelper(getActivity());
        final Cow c = mHelper.getCowByID(id);
        breeddate = (EditText) view.findViewById(R.id.breeddate);
        breeddate.setOnFocusChangeListener(new View.OnFocusChangeListener(){

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    Calendar now = Calendar.getInstance();
                    com.layernet.thaidatetimepicker.date.DatePickerDialog dpd = com.layernet.thaidatetimepicker.date.DatePickerDialog.newInstance(
                            Activity_1_3.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)

                    );
                    dpd.setAccentColor("#6388ad");
                    dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
                }
            }
        });
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/THSarabun.ttf");
        submit.setTypeface(tf);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(submit.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setMessage("ยืนยันการผสม?");
                builder.setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idd) {
                        if(breeddate.getText().toString().equals("")){
                            new AlertDialog.Builder(getActivity())
                                    .setMessage("กรุณากรอกวันผสม")
                                    .setPositiveButton("ตกลง", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                            dialogInterface.dismiss();
                                        }
                                    }).create().show();
                            return;
                        }
                        c.setStatus("2");
                        c.setState("0");
                        c.setDaystart(breeddate.getText().toString());
                        mHelper.updateCow(mHelper.StateUpdate(c));
//                        Breed breed = new Breed();
//                        breed.setUsername(username);
//                        breed.setCowname(c.getCowname());
//
//                        breed.setBreeddate(breeddate.getText().toString());
//                        breed.setPapa(papa.getText().toString());
//                        BREEDHelper bHelper = new BREEDHelper(getActivity());
//                        bHelper.addBreed(breed);

//                    ##############################################################################################
                        CBreed cBreed = new CBreed();
                        cBreed.setCowname(c.getCowname());
                        cBreed.setUsername(username);
                        cBreed.setTimes("1");
                        cBreed.setPapa(papa.getText().toString());
                        cBreed.setBreeddate(breeddate.getText().toString());
                        try{ cBreed.setParity(String.valueOf((Integer.parseInt(c.getParity())+1))); }
                        catch (Exception e){cBreed.setParity("1");}
                        cBreed = new CBREEDHelper(getActivity()).plusCBreed(cBreed);
                        new Sync_CBreed(cBreed).execute();
//                    ###############################################################################################
                        Activity_2_1 fragment = new Activity_2_1();
                        Bundle args = new Bundle();
                        args.putString("ID",id);
                        fragment.setArguments(args);

//                        new Sync_Breed(breed).execute();
                        android.support.v4.app.FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.replace(R.id.root_frame,fragment);
                        trans.commit();
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();

            }
        });
        return view;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
        breeddate.setText(date);
    }
}
